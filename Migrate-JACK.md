[[_TOC_]]

# Overview

Not all features of the client JACK library are supported yet, see this matrix:

| Feature  | Supported |
|:--|:--|
| jack_metadata | Yes
| jack_server | Dummy Only
| jack_intclient | No 
| Latency Reporting | Yes (since 0.3.29)
| jack_metadata | Yes
| jack_midi | Yes
| jack_net | Dummy Only
| jack_session | Dummy, Deprecated
| jack_transport  | Yes
| jack_uuid | Yes
| Freewheel | Yes (Since 0.3.28)

# Configuration

Check [here](Config-JACK) for configuration options.

# JACK Tools

Some of the JACK tools now have native equivalents.

## `jack_lsp`

Use pw-link to list the ports:
```
pw-link -iol
```
## `jack_connect`, `jack_disconnect`

Use pw-link to connect to port by name or ID:

```
pw-link paplay:output_FL my-sink:playback_FL
```

Destroy a link by ID (`pw-link -lI`) or with two port names or ID's
```
pw-link -d paplay:output_FL my-sink:playback_FL
```

## `jack_property`

Use `pw-metadata` to list metadata.

`pw-metadata -d` to delete metadata.

## `jack_bufsize`

You can use this tool to query the current buffersize in the graph.

Since 0.3.60, You can also use this tool to set the graph buffer-size. Use a buffer size of 1 to restore the dynamic buffersize selection again.

Use `pw-metadata -n settings 0 clock.force-quantum <bufsize>` to force a specific buffersize. Restore the normal dynamic buffersize behaviour with `pw-metadata -n settings 0 clock.force-quantum 0`. See also [here](Config-PipeWire#latency-quantum-settings).

# Latency

Latency reporting is implemented since 0.3.29. 

# Freewheel

Freewheeling is implemented since 0.3.28. It works slightly differently from jack in that only the nodes involved in the graph are freewheeled. This means that if you are listening to a song using bluetooth, you can export an ardour project without interruptions to the bluetooth audio.
