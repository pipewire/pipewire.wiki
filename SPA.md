Or Simple Plugin API is a plugin API with following features:

- Unlimited input/output ports that can dynamically be created/removed.
- Per port format enumeration and negotiation. Arbitrary extensible format description.
- Enumeration/configuration of per port parameters. These can be used to control allocation of buffers.
- Application controlled buffer allocation with option to let the plugin allocate memory. Buffers are allocated statically and then given to the plugin.
- Arbitrary buffer metadata.
- Buffers are passed around by id which is very fast and avoids the need for refcounting.
- Aynchronous and asynchronous processing.
- All API is designed to work without any allocations. Plugin memory can be statically allocated and mlocked for hard real-time capable plugins.
- Arbitrary input/output behaviour. There is one process function. Status on port io area decides input/output pattern.
- Support for sparse streams such as subtitles.
- Support for arbitrary buffer queuing and notification of buffer re-use.
- Controllable properties with control streams, either on the ports or with metadata on the buffers.
- Precise timing information with shared IO area.