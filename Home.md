# PipeWire

Is a new low-level multimedia framework designed from scratch that aims to provide:

  - Graph based processing with support for feedback loops and atomic graph updates.
  - Flexible and extensible media format negotiation and buffer allocation.
  - Support for out-of-process processing graphs with minimal overhead.
  - Hard real-time capable plugins.
  - Achieve very low-latency for both audio and video processing.

The framework is used to build a modular daemon that can be configured to:

  - Be a low-latency audio server with features like PulseAudio and/or JACK
  - A video capture server that can manage hardware video capture devices and provide access to them.
  - A central hub where video can be made available for other applications such as the Gnome Shell screencast API.

# Documentation

- Reference documentation: https://docs.pipewire.org/
- Guides: on this Wiki
- Program manual pages: https://docs.pipewire.org/page_programs.html
- Configuration reference: https://docs.pipewire.org/page_config.html
- Pipewire module documentation: https://docs.pipewire.org/page_modules.html
- Pulseaudio module documentation: https://docs.pipewire.org/page_pulse_modules.html