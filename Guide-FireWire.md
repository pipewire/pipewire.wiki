
[[_TOC_]]

# FireWire in PipeWire

FireWire audio devices are supported by the linux kernel with the `snd_dice`
ALSA driver. While this allows PipeWire to use the hardware out of the box,
it is not an optimal sulution because the ALSA driver adds a lot of Latency.

Since 1.0.4, PipeWire supports FireWire with the FFADO driver. This significantly
reduces the latency of the device but requires some extra setup.

## Disable the FireWire kernel ALSA driver

FFADO can not be used when the `snd_dice` kernel module is in use.  Some
programs such as alsactl might keep the module busy when monitoring the
card controls.

To check if the `snd_dice` module is in use you can use:

```
lsmod | grep snd_dice
```
If the usage counter is not 0, it is in use and you can:

 1. Identify the program keeping the device busy and stopping it. You can
   use `aplay -l` to find the FireWire card number and 
   `lsof -a /dev/snd/controlCX` to identify the program.
 2. Blacklist the `snd_dice` driver. Add line to /etc/modprobe.d/blacklist.conf:
    `blacklist snd_dice` and reboot. Only do this is you don't intend
    to use FireWire through ALSA.

## Testing the FFADO module

The easiest way to test FFADO is to load the module with pw-cli:

```
PIPEWIRE_CONFIG_NAME=client-rt.conf pw-cli -m load-module
    libpipewire-module-ffado-driver '{ ffado.period-size=256 ffado.period-num=2 }
```

This starts a PipeWire instance that adds a source and sink node to the
PipeWire graph.

## Automatically starting the FFADO module

To make PipeWire permanently load the FFADO module at startup,
make a new file in `~/.config/pipewire/pipewire.conf.d/30-ffado.conf` with
the following contents:

```
context.modules = [
{   name = libpipewire-module-ffado-driver
    args {
        ffado.period-size=256
        ffado.period-num=2 
    }
}
]
```

Restart the PipeWire process to see the changes.

Check out [here](https://docs.pipewire.org/page_module_ffado_driver.html) for more options.
