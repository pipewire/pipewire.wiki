- Bindings for scripting language, this would make it possible to interactively build pipelines.
- Decoders, demuxers probably based on FFMPEG would allows us to make playback pipelines.
- Think about latency. We would want to do automatic latency correction (unlike JACK) to align related streams.
- Make a Wayland server module that you can connect to with an application and it creates a node with the contents of the application. Probably needs some event input as well to make this useful. Once that works, make a compositor node that takes N inputs.
- an OSC module to query and configure the pipewire graph with OSC messages.

# rust

- Rewrite PipeWire in rust
- Use zbus for IPC, memfd, eventfd for data transport
- Probably use JSON or variant for format description and object serialization instead of POD.
- Remove SPA, we can move this in PipeWire itself and implement streams and filter directly.