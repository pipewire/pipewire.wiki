
[[_TOC_]]

# Limit Volume

Clients and other application are able to change the volume of streams. Volumes
are usually expressed as values between 0.0 (silence), 1.0 (no aplification) and
10.0 (very loud).

You can disable or limit this behaviour by modifying the `channelmix.min-volume` and 
`channelmix.max-volume` stream properties for the various layers.

Setting `channelmix.min-volume` and `channelmix.max-volume` to the same value essentially
locks the volume to the specific value. Other min/max values can be used to limit the
volume.

## PulseAudio 

To limit the volume changes for PulseAudio clients, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/50-volume-limit.conf` with the content:
```
# Limits volume between 0.0 and 1.0
stream.properties = {
    channelmix.min-volume   = 0.0
    channelmix.max-volume   = 1.0
}
```

And restart the pipewire-pulse server.

## Native clients / ALSA clients

To limit the volumes for Native clients or ALSA clients, make a file
`~/.config/pipewire/client-rt.conf.d/50-volume-limit.conf` and
`~/.config/pipewire/client.conf.d/50-volume-limit.conf` with the content:
```
# Limits volume between 0.0 and 1.0
stream.properties = {
    channelmix.min-volume   = 0.0
    channelmix.max-volume   = 1.0
}
```

And restart the pipewire client.

## Bluetooth devices

To enable volume limits for Bluetooth, make a file
`~/.config/wireplumber/wireplumber.conf.d/50-volume-limit.conf` with the content:
```
# Limits volume between 0.0 and 1.0
stream.properties = {
    channelmix.min-volume   = 0.0
    channelmix.max-volume   = 1.0
}
```

And restart Wireplumber.

## ALSA devices

To enable volume limits for ALSA nodes made by wireplumber, make a file
`~/.config/wireplumber/wireplumber.conf.d/60-volume-limit.conf` with the content:

```
monitor.alsa.rules = [
  {
    matches = [
      # This matches the value of the 'node.name' property of the node.
      {
        node.name = "~alsa_.*"
      }
    ]
    actions = {
      update-props = {
        channelmix.min-volume   = 0.0
        channelmix.max-volume   = 1.0
      }
    }
  }
]
```

## PulseAudio null-sink

Add the `channelmix.min-volume=0.0` and `channelmix.max-volume=1.0` properties when loading the
module-null-sink, like so:

```
pactl load-module module-null-sink media.class=Audio/Sink channelmix.min-volume=0.0 channelmix.max-volume=1.0
```

Note that this requires at least version 1.0.6.

# Restrict Volume changes in PulseAudio

The PulseAudio layer has an option to restrict clients from changing their own volume. This
can be useful for badly behaving automatic gain control (in Firefox, Chrome.. ).

The best option is to enable this on a per-application base with a rule and the `block-source-volume`
and `block-sink-volume` [quirks](https://docs.pipewire.org/page_module_protocol_pulse.html).

To block volume changes for PulseAudio clients, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/50-volume-block.conf` with the content:

```
# block some apps from changing the volume
pulse.rules = [
    {
        matches = [ { application.process.binary = "Discord" } ]
        actions = { quirks = [ block-source-volume ] }
    }
]
```

