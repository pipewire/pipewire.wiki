
[[_TOC_]]

# *Canceled* Hackfest in Malaga (April 2024)

The PipeWire community was organizing a hackfest in Malaga near the end of April.

It didn't work out, the room was not available and many key people could not make it.

# Venue

- 24,25,26 April 2024
- H10 Croma Malaga if it were available on those days.
- We would have had a nice hacker room in the H10 Hotel with a rooftop bar and near restaurants and bars in Malaga center.
- We were thinking at hosting a maximum of 35-40 people.
- Coffee and snacks would have been provided.
- We were looking for sponsors for lunch/dinners (we had some dinner suggestions).

# Suggested Hotels

- H10 (we might have gotten a discount for groups)
- Ibis Budget Malaga
- Hotel NH Malaga

# Focus areas

We intended to focus on the following topics:

- Video processing
- Bluetooth
- WirePlumber
- libcamera integration
- Realtime audio

# Confirmed Attendees before cancellation

- Wim Taymans (PipeWire)
- Jorge Ramirez-Ortiz (Foundries.io)
- Philippe Gerum (Xenomai)
- Massimo Pennazio (Arduino)
- Raquel Medina (MBition)