[[_TOC_]]

# Enable IEC958 passthrough formats

If your hardware supports this, you can send compressed AC3, DTS, EAC3 and other formats
directly to the audio receiver. You can always also send uncompressed stereo audio over S/PDIF
although the mixers in the hardware might be partially bypassed and the sound might sound
less full than with the analog output options of the hardware.

You will need an audio card that has a working S/PDIF (optical or coaxial) output. You will
also need a working S/PDIF decoder or AV receiver.

The advantages of passing compressed audio to the receiver are few, these days. Unless
you have slow hardware, it is probably better to use software decoding. This allows you
to apply effects and mixing etc. 

Anyway, it can be cool to offload compression and maybe your receiver/decoder can do a
better job at decoding and rendering. It can also be interesting if your device does not
have 5.1 outputs but only S/PDIF.

IEC958 passthrough does not automatically work out of the box and will need some configuration. The reason is that S/PDIF has no way of detecting the supported codecs in the receiver and so to avoid blowing the speakers with noise, you need to explicitly configure the supported codecs first.

Passthrough with HDMI is partially supported. If the device is using UCM, it likely will not
work.

# Using pavucontrol

The easiest way to configure things is with pavucontrol.

## Enable IEC958 on the card

In pavucontrol go to the `Configuration` tab and locate the card you would like to use for passthrough.

Select the `Digital Stereo (IEC958) Output` Profile. If the card does not have this profile,
passthrough is not supported, sorry, and the remainder of this guide is not for you.

## Enable IEC958 codecs

After the card is placed in the IEC958 profile, a new Output Device was created that ends with
the `(IEC958)` name. Go to the `Output Devices` tab in pavucontrol and locate the new Sink.

Click the `Advanced` button on the IEC958 output device. You should see a bunch of checkboxes
with supported codecs for IEC958 passthrough. `PCM` is always supported and selected. 

Select the other codecs that your receiver/decoder knows how to decode (check the receiver/decoder manual
to know which ones are supported).

The list of supported codecs depend on the supported channels and samplerate of the card. If a
codec you want to use is not in the list, it's not supported, sorry.

# Using pactl

You can also configure passthrough with pactl on the command line.

## Enable IEC958 on the card

Start by listing the cards in your system:

```
> pactl list cards short
55	alsa_card.usb-C-Media_Electronics_Inc._TONOR_TC-777_Audio_Device-00	alsa
56	alsa_card.usb-046d_HD_Pro_Webcam_C920_09D53E1F-02	alsa
57	alsa_card.usb-0d8c_USB_Sound_Device-00	alsa
58	alsa_card.usb-BEHRINGER_UMC404HD_192k-00	alsa
59	alsa_card.usb-046d_0821_9534DE90-00	alsa
60	alsa_card.pci-0000_00_1b.0	alsa
```
In this case, `alsa_card.usb-0d8c_USB_Sound_Device-00` is the card with the IEC958 capabilities.

You can see this by looking at the supported card profiles on the card when doing:

```
> pactl list cards
...
Card #57
	Name: alsa_card.usb-0d8c_USB_Sound_Device-00
...
	Profiles:
		off: Off (sinks: 0, sources: 0, priority: 0, available: yes)
        ...
		output:iec958-stereo: Digital Stereo (IEC958) Output (sinks: 1, sources: 0, priority: 5500, available: yes)
        ...
...
```
Now make this profile the active one on the card with:

```
> pactl set-card-profile alsa_card.usb-0d8c_USB_Sound_Device-00 output:iec958-stereo
```

You can check if the profile is active with:

```
> pactl list cards
...
Card #57
	Name: alsa_card.usb-0d8c_USB_Sound_Device-00
...
	Active Profile: output:iec958-stereo
...
```

## Enable IEC958 codecs

Now that the card is in the IEC958 profile, you can set the supported codecs on the sinks.

First list all the sinks:

```
> pactl list short sinks
32	my-sink	PipeWire	float32le 6ch 44100Hz	IDLE
70	alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0	PipeWire	s32le 4ch 44100Hz	IDLE
73	alsa_output.pci-0000_00_1b.0.analog-stereo	PipeWire	s32le 2ch 44100Hz	IDLE
918	alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-stereo.3	PipeWire	s16le 2ch 44100Hz	IDLE
```

In this case `alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-stereo.3` is the IEC958 enabled sink.

You can check the current state of the sink:

```
> pactl list sinks
...
Sink #918
	State: IDLE
	Name: alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-stereo.3
...
    Ports:
		iec958-stereo-output: Digital Output (S/PDIF) (type: SPDIF, priority: 0, availability unknown)
	Active Port: iec958-stereo-output
	Formats:
		pcm
...
```
Enable more formats with:

```
> pactl set-sink-formats 918 'ac3-iec61937;dts-iec61937'
```

The possible formats for pactl are:
`pcm`, `ac3-iec61937`, `eac3-iec61937`, `mpeg-iec61937`, `dts-iec61937`, `mpeg2-aac-iec61937`,
`truehd-iec61937` and `dtshd-iec61937`.

You can check if the new formats are applied:

```
> pactl list sinks
...
Sink #918
	State: IDLE
	Name: alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-stereo.3
...
    Ports:
		iec958-stereo-output: Digital Output (S/PDIF) (type: SPDIF, priority: 0, availability unknown)
	Active Port: iec958-stereo-output
	Formats:
		pcm
		dts-iec61937, format.rate = "{ \"min\": 44100, \"max\": 96000 }"
		ac3-iec61937, format.rate = "{ \"min\": 44100, \"max\": 96000 }"
```

# Using pw-cli

If you are not running the pulseaudio emulation service or you want to use native PipeWire commands only,
pw-cli can be used to configure passthrough as well but is a little bit more complicated.


## Enable IEC958 on the card

List the supported devices on your system and locate the device with IEC958 capabilities:

```
> pw-cli ls Device
...
id 57, type PipeWire:Interface:Device/3
 		object.serial = "57"
 		factory.id = "14"
 		client.id = "54"
 		device.api = "alsa"
 		device.description = "CM106 Like Sound Device"
 		device.name = "alsa_card.usb-0d8c_USB_Sound_Device-00"
 		device.nick = "ICUSBAUDIO7D"
 		media.class = "Audio/Device"
...
```
`object.serial` should be the same as the card index in `pactl` and `device.name` should
match the Card Name in pactl above.

List the supported Device profiles:

```
> pw-cli e alsa_card.usb-0d8c_USB_Sound_Device-00 EnumProfile
...
Object: size 304, type Spa:Pod:Object:Param:Profile (262151), id Spa:Enum:ParamId:EnumProfile (8)
    Prop: key Spa:Pod:Object:Param:Profile:index (1), flags 00000000
      Int 6
    Prop: key Spa:Pod:Object:Param:Profile:name (2), flags 00000000
      String "output:iec958-stereo"
    Prop: key Spa:Pod:Object:Param:Profile:description (3), flags 00000000
      String "Digital Stereo (IEC958) Output"
    Prop: key Spa:Pod:Object:Param:Profile:priority (4), flags 00000000
      Int 5500
...
```

One of those should be the `output:iec958-stereo` profile that we also see with pactl. Remember
the index of the Profile (6 in this case).

Configure the profile with:

```
> pw-cli s alsa_card.usb-0d8c_USB_Sound_Device-00 Profile '{ index: 6, save: true }'
Object: size 56, type Spa:Pod:Object:Param:Profile (262151), id Spa:Enum:ParamId:Profile (9)
  Prop: key Spa:Pod:Object:Param:Profile:index (1), flags 00000000
    Int 6
  Prop: key Spa:Pod:Object:Param:Profile:save (8), flags 00000000
    Bool true
```

This should configure (and save) the profile. It is the session manager that will remember this profile configuration and restore it when the device is created.

You can check the current profile with:

```
> pw-cli e alsa_card.usb-0d8c_USB_Sound_Device-00 Profile
Object: size 328, type Spa:Pod:Object:Param:Profile (262151), id Spa:Enum:ParamId:Profile (9)
    Prop: key Spa:Pod:Object:Param:Profile:index (1), flags 00000000
      Int 6
    Prop: key Spa:Pod:Object:Param:Profile:name (2), flags 00000000
      String "output:iec958-stereo"
    Prop: key Spa:Pod:Object:Param:Profile:description (3), flags 00000000
      String "Digital Stereo (IEC958) Output"
    Prop: key Spa:Pod:Object:Param:Profile:priority (4), flags 00000000
      Int 5500
    Prop: key Spa:Pod:Object:Param:Profile:available (5), flags 00000000
      Id 0        (Spa:Enum:ParamAvailability:unknown)
    Prop: key Spa:Pod:Object:Param:Profile:classes (7), flags 00000000
      Struct: size 120
        Int 1
        Struct: size 96
          String "Audio/Sink"
          Int 1
          String "card.profile.devices"
          Array: child.size 4, child.type Spa:Int
            Int 11
    Prop: key Spa:Pod:Object:Param:Profile:save (8), flags 00000000
      Bool true
```

## Enable IEC958 codecs

The next step is to enable the supported codecs for the IEC958 profile.

First check the currently enabled Route on the Device:

```
> pw-cli e alsa_card.usb-0d8c_USB_Sound_Device-00 Route
  Object: size 776, type Spa:Pod:Object:Param:Route (262153), id Spa:Enum:ParamId:Route (13)
    Prop: key Spa:Pod:Object:Param:Route:index (1), flags 00000000
      Int 4
    Prop: key Spa:Pod:Object:Param:Route:direction (2), flags 00000000
      Id 1        (Spa:Enum:Direction:Output)
    Prop: key Spa:Pod:Object:Param:Route:name (4), flags 00000000
      String "iec958-stereo-output"
    Prop: key Spa:Pod:Object:Param:Route:description (5), flags 00000000
      String "Digital Output (S/PDIF)"
...
    Prop: key Spa:Pod:Object:Param:Route:device (3), flags 00000000
      Int 11
    Prop: key Spa:Pod:Object:Param:Route:props (10), flags 00000000
      Object: size 232, type Spa:Pod:Object:Param:Props (262146), id Spa:Enum:ParamId:Route (13)
...
        Prop: key Spa:Pod:Object:Param:Props:iec958Codecs (65553), flags 00000000
          Array: child.size 4, child.type Spa:Id
            Id 1        (Spa:Enum:AudioIEC958Codec:PCM)
...
```

There is a lot of output but there should be a props key with a Props object that has the
iec958Codecs property.

Remember the Route `index` (4 in this case) and the `device` (11 in this case).

You can enable more codecs by changing the properties:

```
> pw-cli s <card-id> Route '{ index: 4, device: 11, props: { iec958Codecs: [ PCM, AC3, DTS ] }, save: true }'
Object: size 120, type Spa:Pod:Object:Param:Route (262153), id Spa:Enum:ParamId:Route (13)
  Prop: key Spa:Pod:Object:Param:Route:index (1), flags 00000000
    Int 4
  Prop: key Spa:Pod:Object:Param:Route:device (3), flags 00000000
    Int 11
  Prop: key Spa:Pod:Object:Param:Route:props (10), flags 00000000
    Object: size 48, type Spa:Pod:Object:Param:Props (262146), id Spa:Enum:ParamId:Route (13)
      Prop: key Spa:Pod:Object:Param:Props:iec958Codecs (65553), flags 00000000
        Array: child.size 4, child.type Spa:Id
          Id 1        (Spa:Enum:AudioIEC958Codec:PCM)
          Id 3        (Spa:Enum:AudioIEC958Codec:AC3)
          Id 2        (Spa:Enum:AudioIEC958Codec:DTS)
  Prop: key Spa:Pod:Object:Param:Route:save (13), flags 00000000
    Bool true
```

Valid codecs are: `PCM`, `AC3`, `DTS`, `MPEG`, `MPEG2-AAC`, `EAC3`, `TrueHD` and `DTS-HD`.

Check if the codecs are enabled now:

```
> pw-cli e alsa_card.usb-0d8c_USB_Sound_Device-00 Route
  Object: size 784, type Spa:Pod:Object:Param:Route (262153), id Spa:Enum:ParamId:Route (13)
    Prop: key Spa:Pod:Object:Param:Route:index (1), flags 00000000
      Int 4
    Prop: key Spa:Pod:Object:Param:Route:direction (2), flags 00000000
      Id 1        (Spa:Enum:Direction:Output)
    Prop: key Spa:Pod:Object:Param:Route:name (4), flags 00000000
      String "iec958-stereo-output"
    Prop: key Spa:Pod:Object:Param:Route:description (5), flags 00000000
      String "Digital Output (S/PDIF)"
...
    Prop: key Spa:Pod:Object:Param:Route:device (3), flags 00000000
      Int 11
    Prop: key Spa:Pod:Object:Param:Route:props (10), flags 00000000
      Object: size 240, type Spa:Pod:Object:Param:Props (262146), id Spa:Enum:ParamId:Route (13)
...
        Prop: key Spa:Pod:Object:Param:Props:iec958Codecs (65553), flags 00000000
          Array: child.size 4, child.type Spa:Id
            Id 1        (Spa:Enum:AudioIEC958Codec:PCM)
            Id 3        (Spa:Enum:AudioIEC958Codec:AC3)
            Id 2        (Spa:Enum:AudioIEC958Codec:DTS)
...
    Prop: key Spa:Pod:Object:Param:Route:save (13), flags 00000000
      Bool true
```

# Testing passthrough

You will need an IEC958 capable player such as VLC or mpv. Not all backends support
IEC958 passthrough but the pulseaudio backend usually does. Check the docs of the
player to see what is supported.

For VLC, you can configure the following options:

```
Tools > Preferences > Show settings = All

Tools > Preferences > Audio > Output modules > Audio Output module = Pulseaudio output

Tools > Preferences > Audio > Force S/PDIF support = true
```

Then play a file with some encoded format. I use `Dolby Digital - Broadway` from
[here](https://dvdloc8.com/list_dvd_clip.php).

In pw-top you should see this:

```
> pw-top
S   ID  QUANT   RATE    WAIT    BUSY   W/Q   B/Q  ERR FORMAT           NAME
...
R   69    512  48000  30,5us   5,8us  0,00  0,00    0 IEC958 AC3 48000 alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-stereo.3
R  138   1920  48000  14,4us   8,2us  0,00  0,00    0 IEC958 AC3 48000  + VLC media player (LibVLC 3.0.20)
...
```

See the `IEC958 AC3 48000` in the format column that says it's using passthrough. If you see
another format there, it is not using passthrough but software decoding and there is something
wrong with the configuration, try to do the steps above again.

# Digital Surround 5.1 (AC3)

When the A52 ALSA plugin is installed, a new Profile will become available on the Device
called `Digital Surround 5.1 (IEC958/AC3)`.

This profile will make a 5.1 sink available and it will transparently encode the surround sound
to AC3, which will then be sent over S/PDIF to be decoded by the receiver.

You need to have a working S/PDIF setup (see above) for this profile to work.

The advantage of this profile is that it looks like a regular 5.1 PCM device where each channel
can be routed and mixed into separately.

The disadvantage is that this profile requires realtime A52 encoding, which might consume more
CPU.

In pw-top you should see something like this:

```
> pw-top
S   ID  QUANT   RATE    WAIT    BUSY   W/Q   B/Q  ERR FORMAT           NAME
...
R   68   4096  96000 200,0us 918,3us  0,00  0,02    0     S32P 6 48000 alsa_output.usb-0d8c_USB_Sound_Device-00.iec958-ac3-surround-51
R  173   8192  96000  65,5us 110,4us  0,00  0,00    0    S24LE 6 96000  + paplay
...
```
