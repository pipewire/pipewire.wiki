[[_TOC_]]

# MIDI Support

PipeWire has support for some generic event streams. An event stream carries a collection
of events associated with a timestamp. This makes it possible to synchronize the events
with the other timestamped data in the graph, such as video and audio samples.

One of the possible event types is a MIDI event. MIDI events contain the raw MIDI 1.0
message.

Since 1.3.x, PipeWire natively transports UMP (Universal MIDI Packets) in the control
stream that can contain legacy MIDI 1.0 or the newer MIDI 2.0 standard.

# ALSA sequencer

The MIDI support is implemented using the ALSA sequencer API. This makes it possible to
use PipeWire both with the (legacy) ALSA sequencer tools and PipeWire tools.

Connections between MIDI clients made with the ALSA API (aconnect) will not be managed
by PipeWire but by the ALSA sequencer kernel subsystem. As such, the synchronization between
the MIDI events and the rest of the graph is not guaranteed.

Because each MIDI sequencer client and port is visible in the PipeWire graph with a PipeWire
control port, we suggest to only make connections between the PipeWire ports.


# Midi bridge

This is a node usually created by the session manager and it contains an input or output
port for each (hardware) MIDI client that is detected by the ALSA sequencer API. This also
means that any application that creates a MIDI sequencer client will be added to this
Midi bridge. It is similar in features as the a2jmidid for JACK.


# SysEx

SysEx messages are usually problematic because they can have an unbounded length and are
therefore difficult to processes in a graph with limited buffer size.

PipeWire uses since 1.3.x UMP messages for MIDI events, which can nicely split up the
SysEx messages over multiple packets and buffers.

Older PipeWire can also transport SysEx messages and will use custom segmentation rules:

```
 complete SysEx  f0 .... f7

 start of SysEx  f0 .... f0
 continuation    f7 .... f0
 end             f7 .... f7
 cancel          f7 .... f4
```

Receivers of SysEx must accumulate the segments and process the SysEx after receiving
all the segments.

