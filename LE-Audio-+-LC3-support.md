[LE Audio](https://www.bluetooth.com/learn-about-bluetooth/feature-enhancements/le-audio/) is a completely new implementation of the Bluetooth audio stack, which replaces the "Classic Audio" A2DP and HFP profiles. Its default codec is LC3, but since it is a rewrite of everything, the codec is only a small part of what changes. If you want *all* technical details, read this book: https://www.bluetooth.com/bluetooth-resources/le-audio-book/

The main Linux LE Audio implementation consists of parts in three projects: (i) Linux kernel, (ii) [BlueZ](http://www.bluez.org/), (iii) sound server part, i.e., Pipewire.

The main body of LE Audio implementation is in (i) and (ii). The Pipewire part (iii) is relatively small and simple, and is responsible mainly for codec support and integration with the rest of the sound system.

## Status (complete stack)

**As of 2025-01-26**: (and how I would summarize it)

Experimental and not enabled by default. Known bugs exist and the experience is quite flaky compared to the "classic" audio.

Basic Audio Profile is mostly feature complete across the stack. The Pipewire part is feature complete. 

Broadcast audio is mostly implemented, but requires good knowledge of the specifications to configure.

Hardware volume control (VCP/VCS) is implemented in BlueZ & Pipewire and functional.

Device sets are implemented in BlueZ and Pipewire. E.g. earbuds that appear as separate devices work, and audio playback is synchronized to sub-sample accuracy.

## Known issues

**As of 2025-01-13**: (and how I would summarize it)

Some current major issues:

- Missing support for audio reconfiguration on BlueZ side.

- Device compatibility. If device rejects the initially proposed configuration, audio simply fails instead of reconfiguring to a different configuration. Unfortunately, the BAP/PACS specification does not allow devices to signal exactly what configurations they support (e.g. if duplex is supported, what QoS settings *actually* produce working audio, etc.), apparently the configurations will just have to be probed one by one.

- For example, several devices refuse to work in high-quality duplex mode (48kHz output rate). You may get them to work in output-only mode with config file
  ```
  # ~/.config/wireplumber/wireplumber.conf.d/bap-sink.conf
  monitor.bluez.properties = { bluez5.roles = [ bap_source ] }
  ```

- Stream synchronization for received audio (eg. RX sync between different devices, for example microphones in left/right earbuds): current implementation in Pipewire probable doesn't got sub-sample accurate sync. Kernel work may be needed, as any timestamps or packet numbers are not passed to userspace currently.

- Accurate presentation delays: kernel does not currently report TX/RX delays between getting packet from userspace, and controller sending it over air, or vice versa. Consequently, e.g. playback delay values are accurate only to 1-10ms scale or so. This can be improved a bit, but the Bluetooth Core specification does not appear to specify a clock synchronization mechanism between Host/Controller, so how to relate timestamps from the Controller to a clock on the Host is implementation-defined.

- Devices that support both A2DP and LE Audio will connect in A2DP mode, and connecting in LE mode requires setting ControllerMode=le in bluetooth/main.conf. However, that breaks A2DP and other Classic Bluetooth functionality for all devices.

## How to enable

You should first get:

- Bluetooth adapter that supports LE Audio (eg. Intel AX210/211 at least, some new MediaTek devices)
- Headset device that supports LE Audio, or another computer running BlueZ + Pipewire
- The latest Linux kernel release (at minimum >= 6.4, newer prefereable).
- Latest BlueZ (latest release preferred).
- Latest Pipewire (latest release / master branch preferred).
- liblc3 LC3 library.

Then:

- Set `ControllerMode = le` in `/etc/bluetooth/main.conf`. Also enable `Experimental` and in `KernelExperimental` enable ISO socket. Restart bluetoothd. LE Audio only works if the device connects in LE mode, which it might not do otherwise. The device may need to be removed and paired again if it was already paired before this.

- Make sure your headset device actually supports LE Audio (marketing material can be misleading). `bluetoothctl info` for the device will show `UUID: Published Audio Capabilities (00001850-0000-1000-8000-00805f9b34fb)` if it supports LE Audio. If not, the device does not support it.

- `btmgmt info` (from BlueZ master) will show `supported settings: ... cis-central cis-peripheral` if the BT adapter has the necessary hardware features and kernel and BlueZ are new enough. If `cis-central/peripheral` are not shown, adapter/kernel/BlueZ is too old.

- `bluetoothctl show` will show `UUID: Published Audio Capabilities (00001850-0000-1000-8000-00805f9b34fb)` if BlueZ has LE Audio support enabled for the adapter and Pipewire with LC3 codec is running. If not, the adapter probably doesn't have the necessary features, or Pipewire wasn't compiled have enabled LC3 codec, or is not running, or fails to connect to BlueZ etc. possible issues.

- In `bluetooth.service` (or `bluetoothd`) logs you should see a line saying "Endpoint registered: sender=:X.XX path=/MediaEndpointLE/BAPSource/lc3". If not, either BlueZ has not enabled LE Audio support (experimental settings not enabled or hardware features not available), Pipewire is not compiled with LC3 support etc.

- If Pipewire is compiled with LC3 support, there is a file called `libspa-codec-bluez5-lc3.so`.

- For those TWS earbuds that appear as separate devices:

  - Both earbuds need to be paired, and in TWS configuration they have separate MAC addresses (unlike with A2DP).
  - When you pair one earbud, `bluetoothctl` asks if you want to pair also the other. Similarly it should connect the other when connecting one of them.
  - That the devices form a pair is signaled via Coordinated Set Identification Profile. `bluetoothctl info` for each earbud should show `UUID: Coordinated Set Identif.. (00001846-0000-1000-8000-00805f9b34fb)`, and `Sets Key: /org/bluez/hciX/set_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`
  - `pw-dump` should show nodes with `api.bluez5.set` property
  - When both earbuds are connected, Pipewire emits a node that combines and splits the channels to each sub-device. The separate earbud devices are hidden for Pulseaudio apps, but can be seen in Helvum.

If the above are OK, when connecting the device you should see `Active Profile: bap-duplex` in `pactl list cards`.

Playback should also work, depending on device.

## Reporting issues

Before reporting bugs:

- Recheck the above, try restart the daemons and reconnect the device, in case things are just janky.

- Try with Pipewire master branch + BlueZ master branch + latest kernel version --- in case the issue was already fixed

If you are planning to make bug reports, the following information will be useful/needed:

- [Pipewire debug logs](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Troubleshooting#bluetooth), with `WIREPLUMBER_DEBUG="spa.bluez5*:5"`
- bluetoothd debug logs (change bluetoothd to run with `bluetoothd -d '*'` to generate debug logs)
- potentially [BT traffic dumps](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Troubleshooting#bluetooth-traffic-dump) are useful

Note also that the implementation is spread across several projects.