# JACK and PipeWire

We would like to have PipeWire take care of all audio devices but give an option to run JACK natively on demand.

For this we would use the DBus device reservation API. As soon as JACK claims the device, we release it so that JACK
can open and use it.

There is also a module to become a JACK client using the jackdbus API. See for more information on how to set this up
[here](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Guide-JACK#jackdbus-bridge).

From then on, PipeWire is a secure frontend for JACK. Sandboxed applications can use the JACK emulation API to run in PipeWire while non-sandboxed applications run in the real JACK server, because the PipeWire graph is driven by the JACK server we can guarantee the same latency for sandboxed applications.

From JACK, PipeWire would look like another client with in and out ports.

# PipeWire JACK Implementation

There is a drop-in replacement for `libjack.so`. Existing clients should work unmodified. All JACK API calls are mapped to equivalent PipeWire calls. JACK clients can only see ports marked with the DSP tags. These ports provide and consume float32 audio in mono.

PipeWire control ports (which can carry MIDI events among other things) are exposed as JACK MIDI ports. JACK clients only filter out the MIDI events from the stream.

A PipeWire session manager should provide DSP nodes that interfaces with the audio device and mix audio on the inputs. Through the ports provided by this node, JACK clients (or native clients using compatible ports) can connect to the devices.

Wakeup of JACK clients is very fast with an eventfd. JACK clients mix the inputs on their ports locally and can wake up the next JACK client directly by signaling its evenfd.

Session handling Is not implemented at all and is probably best handled by [NSM](https://github.com/linuxaudio/new-session-manager).
