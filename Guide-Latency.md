[[_TOC_]]

# Set lower Latency (JACK/PipeWire clients)

The latency of the PipeWire processing graph is mostly determined by the combination
of the buffer size and the sample rate of the processing graph, called the quantum.

The quantum is the amount of data (in time) that is processed per processing cycle
of the graph.

By default PipeWire will select the best quantum based on the available clients and
configured limits and defaults.

You can force a quantum, of for example 256, with:

```
pw-metadata -n settings 0 clock.force-quantum 256
```

You can restore dynamic behaviour again with:

```
pw-metadata -n settings 0 clock.force-quantum 0
```

Most PipeWire and JACK clients will directly be influenced by this value.

See also the [configuration](Config-PipeWire#setting-buffer-size-quantum).

# Set PulseAudio clients Latency

Pulseaudio clients use extra buffering in the pipewire-pulse server and so is not directly
influences by the graph buffer size.

Use the environment variable `PULSE_LATENCY_MSEC` to suggest a latency in milliseconds.

```
PULSE_LATENCY_MSEC=20 paplay some.wav
```

# Set ALSA clients Latency

ALSA clients use an extra ringbuffer that can add latency.

Use the `PIPEWIRE_ALSA` [environment variable](Config-ALSA#pipewire_alsa) to control the buffer
size and period size of ALSA clients:

```
PIPEWIRE_ALSA='{ alsa.buffer-bytes=16384 alsa.period-bytes=128 }' aplay ...
```
￼


