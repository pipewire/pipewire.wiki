> :warning: &nbsp;&nbsp; This documentation is moved to: https://docs.pipewire.org/page_config.html and https://docs.pipewire.org/page_man_pipewire-client_conf_5.html

[[_TOC_]]

# General

The ALSA plugin will create a new ALSA `pipewire:` device.

# Configuration Files (`client-rt.conf`)

The ALSA plugin uses the client-rt.conf file.

The client-rt module has a configuration template file located in `/usr/share/pipewire/client-rt`. You can copy and edit the file to `/etc/pipewire/` or `~/.config/pipewire/client-rt.conf`.

Since 0.3.45 you can also copy sections of the config file to a file (with a `.conf` extension) in the directory  `/usr/share/pipewire/client-rt.conf.d/`, `/etc/pipewire/client-rt.conf.d/` or `~/.config/pipewire/client-rt.conf.d/` . Properties will override the previous ones, array entries will be appended.

## Stream properties

All ALSA clients will create a stream and so the [Stream Properties](Config-client#configuration-files-clientconf-client-rtconf) and rules will work as usual.

## ALSA properties

An alsa.properties section can be added to configure ALSA specific client config.

```css
alsa.properties = {
    #alsa.deny = false
    #alsa.format = 0
    #alsa.rate = 0
    #alsa.channels = 0
    #alsa.period-bytes = 0
    #alsa.buffer-bytes = 0
    #alsa.volume-method = cubic         # linear, cubic
}
```

```
alsa.deny
```
Denies ALSA access for the client. Useful in rules or PIPEWIRE_ALSA environment variable.

```
alsa.format
```
The ALSA format to use for the client. This is an ALSA format name. default 0, which is to
allow all formats.


```
alsa.rate
```
The samplerate to use for the client. The default is 0, which is to allow all rates.

```
alsa.channels
```
The number of channels for the client. The default is 0, which is to allow any number of channels.

```
alsa.period-bytes
```
The number of bytes per period. The default is 0 which is to allow any number of period bytes.

```
alsa.buffer-bytes
```
The number of bytes in the alsa buffer. The default is 0, which is to allow any number of bytes.

```
alsa.volume-method = cubic | linear
```

This controls the volume curve used on the ALSA mixer. Possible values are `cubic` and
`linear`. The default is to use `cubic`.


## ALSA Rules

It is possible to set client specific properties by using [Rules](Config-PipeWire#rules).
You can set any of the above ALSA properties or any of the [stream.properties](Config-client#streamproperties).

```
alsa.rules = [
    {   matches = [ { application.process.binary = "resolve" } ]
        actions = {
            update-props = {
                alsa.buffer-bytes = 131072
            }
        }
    }
]
```

# Environment variables

The following environment variables influence ALSA applications:

## `PIPEWIRE_ALSA`

This can be an object with properties from `alsa.properties` or `stream.properties` that will
be used to construct the client and streams.

For example:
```
PIPEWIRE_ALSA='{ alsa.buffer-bytes=16384 node.name=foo }' aplay ...
```
Starts aplay with custom properties.

## `PIPEWIRE_NODE`

Instructs the ALSA client to link to a particular sink or source `object.serial` or `node.name`.

For example:
```
PIPEWIRE_NODE=alsa_output.pci-0000_00_1b.0.analog-stereo aplay ...
```
Makes aplay play on the give audio sink.
