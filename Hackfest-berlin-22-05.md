[[_TOC_]]

# Hackfest in Berlin (May 2022)

The PipeWire community is organizing a hackfest in Berlin from Tue 24th to Thu 26th of May. The venue will be open on the 23rd for those who land early.

As part of the event we will also be organizing a libcamera minisummit, to discuss PipeWire integration and other relevant topics.

# Venues

## 23th**, 24th & 25th
MBition
Dovestraße 1, 10587 Berlin, Germany

** the 23th is an informal gathering for those who are already in Berlin

## 26th

Cultivation Space (previously Onion Space) at Gottschedstraße 4, Bezirk Mitte
13357 Berlin,

# Suggested Hotels

- the niu flash, Franklinstraße 25, 10587 Berlin, Germany
- Select Hotel Style Berlin, Franklinstraße 23, 10587 Berlin, Germany

# Focus areas

We intend to focus on the following topics:

- Bluetooth
- WirePlumber
- libcamera integration
- Video virtualization (virtio-video)

# libcamera miniconf

We will be hosting a libcamera miniconf as well.

The focus here will be PipeWire integration and integration in mainstream Linux distros.

# COVID 19 Safety Measures

Given that to fly to Germany you are required to pass a vaccionation/test filter and that MBition have dropped any hard requirements to get into the building we feel like it is an unnecessary overhead.

# Attendees

- Wim Taymans
- Enric Balletbo (virtio-video)
- Kieran Bingham (libcamera)
- Laurent Pinchart (libcamera)
- George Kiagiadakis (WirePlumber)
- Alberto Ruiz (Red Hat, Fedora/CentOS Automotive SIG) 
- Hans de Goede (libcamera, Fedora)
- Christian Kellner (Fedora)
- Robin Gareus (Ardour)
- TBA (MBition)
- TBA (MBition)
- TBA (MBition)
- TBA (MBition)
- TBA (MBition)

