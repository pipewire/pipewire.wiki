[[_TOC_]]

> :information_source: &nbsp;&nbsp; See also: https://docs.pipewire.org/page_config.html

# General

This topic talks about the behavior and configuration of the PipeWire JACK server and client library.

# JACK Server

PipeWire is an implementation of a full JACK server. There is a custom PipeWire libjack.so library to make JACK applications talk to PipeWire.

Since 0.3.81, PipeWire will use the same scheduling as JACK for Pro-Audio cards and will give equal latency with
slightly better performance compared to JACK. See [CPU](Performance#jack-clients) and
[Latency](Performance#latency) benchmarks.

It is also possible to run JACK together with PipeWire. Since 0.3.81 PipeWire will automatically become a JACK client when jackdbus is started. This is the recommended way to use JACK and PipeWire together and is similar to
the PulseAudio/JACK setup. See [here](Config-JACK#jack-bridge) for more details.

## Samplerate

JACK uses by default the global samplerate of the PipeWire graph, which you can configure as explained [here](Config-PipeWire#setting-global-sample-rate). 

The samplerate can be changed at runtime with:
```
pw-metadata -n settings 0 clock.force-rate <samplerate>
```
This will lock the samplerate to `<samplerate>`.  You can change it back to the default
dynamic behaviour with:
```
pw-metadata -n settings 0 clock.force-rate 0
```

More info here in [Locking sample-rate](Config-PipeWire#samplerate-settings)

## Buffer Size

The buffer-size (or quantum in Pipewire) is dynamically changed between the min and max values, as set in the [config file](Config-PipeWire#setting-buffer-size).

The buffersize can be changed and locked at runtime with:
```
pw-metadata -n settings 0 clock.force-quantum <buffersize>
```
This will lock the buffersize to `<buffersize>`.  You can change it back to the default
dynamic behaviour with:
```
pw-metadata -n settings 0 clock.force-quantum 0
```
Since 0.3.46, when a JACK application is started, the buffersize can not be dynamically changed
anymore except with a JACK application or with metadata.

More info in [Locking buffer-size](Config-PipeWire#latency-quantum-settings).

Since 0.3.60, jack_bufsize can be used to change the buffersize at runtime. Use a buffersize of 1 to revert things back to the dynamic bufersize selection.

Since 0.3.81, setting the buffer size will also reconfigure the period side in Pro-Audio apps to match the
JACK behaviour.

## A Minimal JACK Server

PipeWire is usually started with a session manager and PulseAudio compatible server. This enables support for hot-plug devices, bluetooth, hardware volumes and PulseAudio clients.

Since 0.3.44 It is however possible to start a minimal JACK compatible server without a session manager or PulseAudio compatible server. An example config file can be found [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/minimal.conf.in). You will have to edit the ALSA hardware device in use and the channels.

This minimal server is enough to run most JACK applications such as Ardour and Carla.

To enable PulseAudio (and ALSA) clients in this minimal server, you need to run a pipewire-pulse server and a session manager that can link the PulseAudio clients to the hardware devices.

# Configuration File (`jack.conf`)

> :warning: &nbsp;&nbsp; This documentation is moved to: https://docs.pipewire.org/page_man_pipewire-jack_conf_5.html

The JACK module has a configuration file located in `/usr/share/pipewire/jack.conf` that can be customized. You can copy and edit the file to `/etc/pipewire/` or make a copy to `~/.config/pipewire/jack.conf`.

Since 0.3.45 you can also copy sections of the configuration file to a file (with a `.conf` extension) in the directories `/usr/share/pipewire/jack.conf.d/`, `/etc/pipewire/jack.conf.d/` or `~/.config/pipewire/jack.conf.d/`. Properties will be merged with the previous ones, array entries will be appended.

## Properties

The configuration file can contain an extra JACK specific section called `jack.properties` like this:

```
...
jack.properties = {
    #rt.prio             = 88
    #node.latency        = 1024/48000
    #node.lock-quantum   = true
    #node.force-quantum  = 0
    #jack.show-monitor   = true
    #jack.merge-monitor  = true
    #jack.show-midi      = true
    #jack.short-name     = false
    #jack.filter-name    = false
    #jack.filter-char    = " "
    #
    # allow:           Don't restrict self connect requests
    # fail-external:   Fail self connect requests to external ports only
    # ignore-external: Ignore self connect requests to external ports only
    # fail-all:        Fail all self connect requests
    # ignore-all:      Ignore all self connect requests
    #jack.self-connect-mode  = allow
    #
    # allow:           Allow connect request of other ports
    # fail:            Fail connect requests of other ports
    # ignore:          Ignore connect requests of other ports
    #jack.other-connect-mode = allow
    #jack.locked-process     = true
    #jack.default-as-system  = false
    #jack.fix-midi-events    = true
    #jack.global-buffer-size = false
    #jack.passive-links      = false
    #jack.max-client-ports   = 768
    #jack.fill-aliases       = false
    #jack.writable-input     = false

}
```
We refer the the [Client stream Configuration](Config-client#streamproperties) for an explanation of the generic node properties.

It is also possible to have per-client settings, see [rules](#application-specific-settings-rules).

### `rt.prio`

To limit the realtime priority that jack clients can acquire.

### `node.latency`

To force a specific minimum buffer size for the JACK applications, configure:
```
    node.latency = 1024/48000
```
This configures a buffer-size of 1024 samples at 48KHz. If the graph is running at a different sample rate, the buffer-size will be adjusted accordingly.

### `node.lock-quantum`

To make sure that no automatic quantum is changes while JACK applications are running, configure:
```
    node.lock-quantum = true
```
The quantum can then only be changed by metadata or when an application is started with node.force-quantum. JACK Applications will also be able to use jack_set_buffersize() to override the quantum.

### `node.force-quantum`

To force the quantum to a certain value and avoid changes to it:

```
    node.force-quantum = 1024
```
The quantum can then only be changed by metadata or when an application is started with node.force-quantum (or JACK applications that use jack_set_buffersize() to override the quantum).


### `jack.show-monitor`

Show the Monitor client and its ports. 

### `jack.merge-monitor`

Exposes the capture ports and monitor ports on the same JACK device client. This is how JACK presents monitor ports to the clients. The default is however *not* to merge them together because this results in more user friendly user interfaces, usually. An extra client with a `Monitor` suffix is created that contains the monitor ports.

For example, this is (part of) the output of `jack_lsp` with the default setting (`jack.merge-monitor = false`):

Compare:

| `jack.merge-monitor = true` | `jack.merge-monitor = false` |
|:--|:--|
| Built-in Audio Analog Stereo:playback_FL | Built-in Audio Analog Stereo:playback_FL
| Built-in Audio Analog Stereo:monitor_FL | Built-in Audio Analog Stereo Monitor:monitor_FL
| Built-in Audio Analog Stereo:playback_FR | Built-in Audio Analog Stereo:playback_FR
| Built-in Audio Analog Stereo:monitor_FR |Built-in Audio Analog Stereo Monitor:monitor_FR

### `jack.show-midi`

Show the MIDI clients and their ports.

### `jack.short-name`

To use shorter names for the device client names use `jack.short-name = true`. Compare:

| `jack.short-name = true` | `jack.short-name = false` |
|:--|:--|
| HDA Intel PCH:playback_FL | Built-in Audio Analog Stereo:playback_FL
| HDA Intel PCH Monitor:monitor_FL | Built-in Audio Analog Stereo Monitor:monitor_FL
| HDA Intel PCH:playback_FR | Built-in Audio Analog Stereo:playback_FR
| HDA Intel PCH Monitor:monitor_FR |Built-in Audio Analog Stereo Monitor:monitor_FR

### `jack.filter-name`, `jack.filter-char`

Will replace all special characters with `jack.filter-char`. For clients the special characters are ` ()[].:*$` and for ports they are ` ()[].*$`. Use this option when a client is not able to deal with the special characters. (and older version of PortAudio was known to use the client and port names as a regex, and thus failing when there are regex special characters in the name).

### `jack.self-connect-mode`

Restrict a client from making connections to and from itself. Possible values and their meaning are summarized as:

| Value | Behavior
|:--|:--|
| `allow` | Don't restrict self connect requests.
| `fail-external` | Fail self connect requests to external ports only.
| `ignore-external` | Ignore self connect requests to external ports only.
| `fail-all` | Fail all self connect requests.
| `ignore-all` | Ignore all self connect requests.

### `jack.other-connect-mode`

Restrict a client from making connections on ports it doesn't own (external ports). Possible values and their meaning are summarized as:

| Value | Behavior
|:--|:--|
| `allow` | Don't restrict connect requests to external ports.
| `fail` | Fail connect requests to external ports.
| `ignore` | Ignore all connect requests to external ports.

### `jack.locked-process`

Make sure the process and callbacks can not be called at the same time. This is the
normal operation but it can be disabled in case a specific client can handle this.

### `jack.default-as-system`

Name the default source and sink as `system` and number the ports to maximize
compatibility with JACK programs.

| `jack.default-as-system = false` | `jack.default-as-system = true` |
|:--|:--|
| HDA Intel PCH:playback_FL | system:playback_1
| HDA Intel PCH Monitor:monitor_FL | system:monitor_1
| HDA Intel PCH:playback_FR | system:playback_2
| HDA Intel PCH Monitor:monitor_FR | system:monitor_2

### `jack.fix-midi-events`

Fix NoteOn events with a 0 velocity to NoteOff. This is standard behaviour in JACK and is thus
enabled by default to maximize compatibility. Especially LV2 plugins do not allow NoteOn
with 0 velocity.

### `jack.global-buffer-size`

When a client has this option, buffersize changes will be applied globally and permanently for all PipeWire clients using the metadata.

### `jack.passive-links`

Makes JACK clients make passive links. This option only works when the server link-factory was configured with the `allow.link.passive` option.

### `jack.max-client-ports`

Limit the number of allowed ports per client to this value.

### `jack.fill-aliases`

Automatically set the port alias1 and alias2 on the ports.

### `jack.writable-input`

Makes the input buffers writable. This is the default because some JACK clients write to the
input buffer. This however can cause corruption in other clients when they are also reading
from the buffer.

Set this to true to avoid buffer corruption if you are only dealing with non-buggy clients.

# Environment

Environment variables can be used to control the behavior of the PipeWire JACK client library.

## `PIPEWIRE_NOJACK` / `PIPEWIRE_INTERNAL`

When any of these variables is set, the JACK client library will refuse to open a client. The `PIPEWIRE_INTERNAL` variable is set by the PipeWire main daemon to avoid self connections.

## `PIPEWIRE_PROPS`

Adds/overrides the properties specified in the `jack.conf` file. Check out the output of this:

```
> PIPEWIRE_PROPS='{ jack.short-name=true jack.merge-monitor=true }' jack_lsp
...
HDA Intel PCH:playback_FL
HDA Intel PCH:monitor_FL
HDA Intel PCH:playback_FR
HDA Intel PCH:monitor_FR
...
```

## `PIPEWIRE_LATENCY`

```
PIPEWIRE_LATENCY=<samples>/<rate> <application>
```

A quick way to configure the maximum buffer-size for a client. It will run this client with the specified buffer-size (or smaller).

`PIPEWIRE_LATENCY=256/48000 jack_lsp` is equivalent to `PIPEWIRE_PROPS='{ node.latency=256/48000 }' jack_lsp`

A better way to start a jack session in a specific buffer-size is to force it with:
```
pw-metadata -n settings 0 clock.force-quantum <quantum>
```
This always works immediately and the buffer size will not change until the quantum is changed back to 0.

## `PIPEWIRE_RATE`

```
PIPEWIRE_RATE=1/<rate> <application>
```

A quick way to configure the rate of the graph. It will try to switch the samplerate of the graph. This can usually only be done with the graph is idle and the rate is part of the allowed sample rates.

`PIPEWIRE_RATE=1/48000 jack_lsp` is equivalent to `PIPEWIRE_PROPS='{ node.rate=1/48000 }' jack_lsp`

A better way to start a jack session in a specific rate is to force the rate with:

```
pw-metadata -n settings 0 clock.force-rate <rate>
```
This always works and the samplerate does not need to be in the allowed rates. The rate will also not change until it is set back to 0.

## `PIPEWIRE_QUANTUM`

```
PIPEWIRE_QUANTUM=<buffersize>/<rate> <application>
```

Is similar to using `PIPEWIRE_LATENCY=<buffersize>/<rate>` and `PIPEWIRE_RATE=1/<rate>` (see above), except that it is not just a suggestion but it actively *forces* the graph to change the rate and quantum. It can be used to set both a buffersize and samplerate at the same time.

When 2 applications force a quantum, the last one wins. When the winning app is stopped, the quantum of the previous app is restored.

## `PIPEWIRE_LINK_PASSIVE`

```
PIPEWIRE_LINK_PASSIVE=true qjackctl
```
Make this client create passive links only. All links created by the client will be marked passive and will not keep the sink/source busy.

You can use this to link filters to devices. When there is no client connected to the filter, only passive links remain between the filter and the device and the device will become idle and suspended.

## `PIPEWIRE_NODE`

```
PIPEWIRE_NODE=<id> <application>
```
Will sort the ports so that only the ports of the node with <id> are listed. You can use this to force an application to only deal with the ports of a certain node, for example when auto connecting.

# Default Devices

The PipeWire JACK client library uses the metadata to follow the default devices as configured by the session manager.

Use `pw-metadata` to check the default configured devices.

The default configured sink and source will appear first when enumerating the available ports. This makes it likely that apps will connect to it automatically.

JACK apps will not automatically reconnect to the new default device when it changes.

# Controlling Latency of JACK Applications

## Global Latency Settings

When running a JACK application, it will normally run with a buffer-size as defined by the global default [quantum settings](Config-PipeWire#setting-buffer-size). You can permanently tweak the min and max quantum as well as the default quantum there.

We recommend that you leave the defaults as they are and use the metadata instead to temporarily force a
samplerate and buffersize for the JACK session. See [config samplerate](Config-JACK#samplerate) and
[config buffersize](Config-JACK#buffer-size) for how to do this.

## JACK Application Latency

In the [config file](Config-JACK#configuration-file-jackconf), you can set the `node.latency` property to something like `256/48000` to run all JACK applications at a 5.3ms latency or less.

## Per Application Latency

It is often useful to run a specific application with a specific rate and buffer_size. Use the [PIPEWIRE_QUANTUM](Config-JACK#pipewire_quantum) environment variable like this:

```
PIPEWIRE_QUANTUM=256/48000 reaper
```
to run REAPER with a buffer of 256 samples and a rate of 48000Hz for a total latency of 5.3ms. When reaper exits, the
previous quantum will be restored.

## Temporary Forced Latency.

With pw-metadata you can temporarily change the buffer size or rate to a specific value. All application will run with this latency until the defaults are restored.

See [here](Config-PipeWire#latency-quantum-settings) for more information.

## Device systemic latency settings

In addition to the software controlled latency (by controlling the amount of buffered data in the device), there
is also a hardware latency (caused by analog to digital conversion, bus transport, usb timings, etc...). This
latency is usually a few milliseconds.

This latency can be measured with a loopback cable and a tool like `jack_iodelay`. This tool will send a test
signal on the output port and measures when that signal is captured on the input port; this will be the total roundtrip delay of the signal. The tool will assume an equal latency for the source as the sink node (there is no way to know for sure) and will report this latency in samples.

Note that this latency can change when the device is reconfigured. For (pro-audio) devices that share a clock this latency will then be constant for as long as the device is running with the same configuration.

The latency can be configured on the device with:

```
pw-cli s <device-name> ProcessLatency '{ rate=<latency> }'
```
Fill in the `<device-name>` with the `node.name` (constant accross reboots) or the `object.id` of the
sink and soure to adjust.

This latency will now be added to the software latency on the sink/source ports. You can check the new
increased latency with a tool like `jack_lsp -L`.

You can safely rerun the calibration at runtime whenever the quantum or samplerate of the graph has changed.

# Application Specific Settings (Rules)

In the [config file](Config-JACK#configuration-file-jackconf), you can add [rules](Config-PipeWire#rules) to set properties for certain applications.

`jack.rules` provides an `update-props` action that takes an object with properties that are updated
on the client and node object of the jack client.

Add a `jack.rules` section in the config file like this:

```
jack.rules = [
    {
        matches = [
            {
                # all keys must match the value. ! negates. ~ starts regex.
                application.process.binary = "jack_simple_client"
            }
        ]
        actions = {
            update-props = {
                node.latency = 512/48000
            }
        }
    }
    {
        matches = [
            {
                client.name = "catia"
            }
        ]
        actions = {
            update-props = {
                jack.merge-monitor = true
            }
        }
    }
]
```
Will set the latency of jack_simple_client to 512/48000 and makes Catia see the monitor client merged with the playback client.

# Installation

At this time there are still package dependency issues with JACK but the only package you need is `pipewire-jack-audio-connection-kit`. You also need to to create a file `/etc/ld.so.conf.d/pipewire-jack-x86_64.conf` containing a link to the JACK modules:
````
/usr/lib64/pipewire-0.3/jack/
````
Then `sudo ldconfig`.

This [snippet](https://gitlab.freedesktop.org/pipewire/pipewire/-/snippets/1165) may explain in more detail.  

This configuration issue should be fixed in later versions but as of 0.3.21+ it is still an issue.

# MIDI

PipeWire has the equivalent of a2jmidid built-in. All hardware devices and ALSA-sequencer ports will appear as PipeWire ports on the Midi-Bridge node and can be freely routed in PipeWire.

It is recommended to avoid using ALSA raw device or the sequence API for new applications. The reason is that better latency and synchronisation can be guaranteed when the MIDI data is transported as control data in PipeWire.

## Virtual MIDI devices

If some clients (Like Bitwig) want to access raw devices you can load the virtual MIDI device:
```
$ sudo modprobe snd_virmidi midi_devs=1
```
This device will appear in the PipeWire graph as PipeWire control ports that can be freely routed through PipeWire.


# Filters and Effects

For now it is recommended to use a script to configure extra sink/sources and effect chains. There are also many JACK tools to load and save custom graphs.

In the future we plan to add a session manager module to set up these processing chains more easily.

You can't in general route PulseAudio streams directly to (JACK) effect nodes so you need to create a virtual sink or source first with:

```
pactl load-module module-null-sink media.class=Audio/Sink sink_name=my-effect-sink channel_map=stereo
```
Check in the next section for more information on how to do this.

We recommend to use the JACK effects, like the lsp plugins, calfjackhost, jalv, Carla, etc. 

After creating a node to connect the PulseAudio streams to, you can set up and link the filters to a device. We recommend to use `pw-link` for this. If you use the `-P` option to link between the effects and the sinks, all nodes will be able to suspend when the device goes idle.

## Calfjackhost Example

````
pactl load-module module-null-sink media.class=Audio/Sink sink_name=my-calf-effects channel_map=stereo

calfjackhost
````

This makes a sink and a Calf rack. You can insert effects in Calf rack and then link the sink outputs to the rack inputs. You can set it as the default device with PulseAudio and use pavucontrol to send streams to it.

## Virtual Surround for Headphones

A simple guide to apply reverb and/or virtual surround effects to your audio output, especially if using headphones can be found here: 
[Simple audio post processing](Simple-audio-post-processing).

# Network Transport

Use [zita-njbridge](https://kokkinizita.linuxaudio.org/linuxaudio/) to link multiple JACK/pipewire instances in a network.

> note that zita-njbridge does not support dynamic bufer-size changes so you might need to [lock the quantum](Config-PipeWire#latency-quantum-settings). 

You can also use one of the other supported [network protocols](Network).

# JACK Bridge

Since 0.3.71, The recommended way to start PipeWire as a JACK client is to use jackdbus
and the [module-jackdbus-detect](https://docs.pipewire.org/page_module_jackdbus_detect.html).
You can add this module to the PipeWire main config file for highest performance. It
is also possible to load the module into the pipewire-pulse server with
`pactl load-module module-jackdbus-detect`.

Since 0.3.81, the jackdbus module is already loaded and ready to use.

When starting jackdbus, with for example qjackctl, PipeWire will release the JACK device
and become a client for the JACK server.

PipeWire will act as a single synchronous JACK client. On the PipeWire side there is a Sink
and a Source node. Data send to the pipewire client as input will become available on
the PipeWire Source and data sent to the PipeWire sink will be made available on
the output ports of the pipewire JACK client.

Processing inside the PipeWire JACK client is 0-latency. This means that `jack_iodelay`
between JACK capture and playback ports have exactly the same latency as `jack_iodelay`
as measured between the PipeWire source and sink.

## Old bridge pre 0.3.71

PipeWire can function as a JACK client but this needs to be explicitly enabled in [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/configuration/alsa.html) with `[ "alsa.jack-device" ] = true`.

When the JACK device is enabled, a new device will be available in pw-cli ls device or pavucontrol. You can switch this Device to the on profile to make PipeWire become a JACK client. Turning the card profile back off will disconnect from JACK again.

This method is deprecated.
