# AES67 support

PipeWire builds `pipewire-aes67` binary which has RTP receiving and transmitting modules configured for communicating in professional AES67 networks. It has been tested to work with Dante and RAVENNA networks (the list is being expanded).

## Useful links
- [AES67 support tracker](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/3217) - please report successful/troublesome experience here (without opening new issues). You can also see a TODO list for improvements being worked on there.
- [ptp4l docs](https://linuxptp.nwtime.org/documentation/ptp4l/)

## Version requirements

Best features are available in master branch, as AES67 integration is currently being rapidly developed. When 1.1.0 release is out more compatibility features will be available in released builds, but for now consider building PipeWire from sources to get the best results.

## Recommended setup

- A wired network interface connected to the audio network. Ensure it has proper IP by looking at `ip a` output. Also remember and copy the interface name: you'll need it throughout the configuration procedure.
- PTP Hardware Clock (PHC) is highly recommended to ensure sub-millisecond accuracy required to send data to AES67 devices. This can be confirmed by running a command like `ethtool -T enp3s0` (replace `enp3s0` with your interface name). If it has mentions of `hardware-transmit`, `hardware-receive` and `PTP Hardware Clock`, then your networking card has full feature set and thus you'll be able to acquire precise clocking. In the future it might be possible to get some results even if your network adapter only mentions `software-transmit` and `software-receive`.
- PTP software stack. We typically use `ptp4l` program from `linuxptp` package to manage the PHC. It can work without syncing the whole system time to the network clock, which is generally to be avoided. You can use other PTP implementations as well if they sync the PTP hardware clock. As the most tested scenario, we'll describe using ptp4l for clock sync in this article.
> Version 4.0 and higher is recommended for automatic configuration. You might need to make `/dev/ptp0` (or another PHC device) accessible by your user. Don't use PipeWire with sudo, but adjust permissions, e.g. using [a udev rule](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/78642cc53bd84c2ad529f2175cc50a658d1e52c0/src/daemon/90-pipewire-aes67-ptp.rules). For ptp4l versions lower than 4.0 you will need to make /var/run/ptp4l read-writable by your user and change the path in config.
> For other PTP daemons you also need to adjust the socket path.
- Firewall accepting ports 319/320/5004/9875 into and from your audio network. You might want to configure that network to be a trusted firewall zone (but first make sure it's safe to do so).
- Network working with the SAP announcement format. For Dante networks you need to enable AES67 support on devices you want to communicate with, for RAVENNA hardware you need to run RAV2SAP on a connected computer. Please report compatibility/incompatibility discovered with hardware you use.

## Setting up PTP time sync

To ensure high accuracy and latency stability AES67 mandates all the streams to follow a common clock. We'll be using ptp4l to make the network interface clock synced to the network clock. Please ensure your interface supports the features described in prerequisites. If not, better try a different one or contact us in the tracker issue.

To get started you can run a command similar to `sudo ptp4l -i enp3s0 -s -l 7 -m -q` (but with your interface name filled in). It should start cyclically printing messages like `rms 19 max 16 freq -23320 +/- 23 delay 1036 +/- 1`, but no errors.

If so, you can stop the command execution and add `-s` (or `clientOnly 1` for a text config) option to your ptp4l config file \*. Then use `sudo systemctl daemon-reload` and `sudo systemctl enable --now ptp4l@<interface name>.service` to automatically start ptp4l for that interface at each startup. If the system is not guaranteed to be connected to the AES67 network often, you can proceed running ptp4l in your terminal.

\* ptp4l config locations (might not be accurate, refer to you distribution's docs):
- SUSE: `/etc/sysconfig/ptp4l`
- Ubuntu: `/etc/linuxptp/ptp4l`
- Fedora/RedHat: `/etc/ptp4l/ptp4l.conf`

Ensure you have read access to the `/dev/ptp*` PHC node. If not, use [a udev rule](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/78642cc53bd84c2ad529f2175cc50a658d1e52c0/src/daemon/90-pipewire-aes67-ptp.rules) to obtain access.

> On some systems ptp4l@interface.service might not be brought up correctly during system startup. If it reports errors in the logs, but is fine after restarting the service, then you probably need to start it as you need it.

## Get the Grandmaster Clock identity

If you use ptp4l version 4.0 or higher and use pipewire-aes67 from PipeWire 1.1.0 or higher PipeWire will automatically recognize active grandmaster and when your computer's time is in sync to an external PTPv2 it will add that grandmaster identity to the announcement information.

GM identity format is `ptp=IEEE1588-2008:11-22-33-FF-FE-44-55-66:0` based on the MAC address (`11:22:33:44:55:66`) of the device which is the grandmaster. PTP services should be able to report this identity. However ptp4l will ensure robust transitions in case your GM is changed during the network operation, so it's currently the recommended option.

## Configuring `pipewire-aes67`

Ensure you edit the correct config file. As you are likely running PipeWire from git using `pw-uninstalled.sh`, you should edit configs in the build directory. If you run installed version through systemd services, you should edit configs in your home directory (copied from `/usr/share/pipewire` as for any other config modification).

In the copy of `pipewire-aes67.conf` locate comments marked with `###` or `#` and follow the instructions filling in you interface name (use `ip a` to check which one is connected to the AES67 network). Add the grandmaster ID if you determined it yourself. Also pay attention to `#` comments. They are less important than `###` ones, but might be helpful to improve your experience or troubleshoot.

Consider changing `sess.name`, `node.name` and `destination.ip` if you have multiple PipeWire senders on your network or multiple nodes on your system. You can create more AES67 output streams on your computer by copy-pasting the block starting with `{ name = libpipewire-module-rtp-sink` and ending with the corresponding `}` and modifying the said properties. We marked props you need to adjust with comments.

`destination.ip` is a [multicast IP address](https://en.wikipedia.org/wiki/IP_multicast), and should typically be within `239.69.0.0/16` range (`239.69.0.0`-`239.69.255.255`) at least for Audinate Dante devices. You can change this in receiver preferences. This is not an IP address assigned to a device on your network but rather an IP of a group receivers might opt in to join and receive the packets. The rule is that this IP should be enabled for the receiver and not occupied by other senders (probability is pretty low for that knowing there're 65536 unique multicast IPs in such an allowed range).

`node.channel-names` property will set channel names you can see both from the applications you use (primarily JACK professional audio apps) and devices that receive your streams (checked to work with Dante and RAVENNA).

## Common issues
- `failed to open clock device ... ensure pipewire has sufficient access permissions to read the device` - incorrect access permissions. You might want to try either configuring the permissions manually or use [this udev rule](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/78642cc53bd84c2ad529f2175cc50a658d1e52c0/src/daemon/90-pipewire-aes67-ptp.rules). This is not being done by default since read access to PHC could disrupt other clients working with it. As long as you only use the PHC for pipewire-aes67 you can enable access to it.
- Sound disruptions under heavy load, dropouts, late packets reported by receivers - caused by low RT performance. Please refer to the [Performance tuning guide](Performance-tuning) or distribution-recommended settings. `preempt=full` or realtime kernel often gives sufficient improvements in low-latency audio processing.
- Sinks get confused by applications and default sink selection is broken - ensure they have distinct `sess.name` and `node.name` as said in the previous section.

## Further tuning

Currently (as of commit 78055736a25c162f6cef310442def405094cf973 from 30 Jan 2024 and version 1.1.0) PipeWire will default to quantum of 144 (3 ms) for AES67 sender. This will allow most applications to run fine without special configuration. However as you improve performance using common guidelines you may reduce `sess.latency.msec` to lower the overall latency. Non-integer values are not applicable and will cause packets to be off-sync.

## Verified hardware compatibility

### Revision 1.1.0-1de71d96c6c45d8e247ef37e50d7a0f2036dadd1 tested by @ninbura

Hardware:
- RME Digiface Dante (send/receive)
- Dante AVIO AES3/EBU (send/receive)
- MIPRO MI-58 (send/receive)

NIC: Intel i211AT

OS: Ubuntu 23.10 with RT kernel

![image](uploads/0e4e46568b9e49d1fe946e824680559a/image.png)

> https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/3217#note_2277711

### Version: 1.1.0-a7410fe1bfa5bf74b47b51f60d648236a70be0a5 (built from branch)

Tested by: @dewiweb

> Tested Devices:
> - Merging Technologies HAPI (sender/receiver)
> - Lawo MC<sup>2</sup> 36 console (sender/receiver)
> - Lawo MADI_4 (sender/receiver)
> - SHURE AD4Q (receiver)
> - Holophonix processor with Digigram LX-IP Dante PCIe card (receiver)
> - Q-SYS core 110f (sender)

> Host and configuration:
> - Dell PowerEdge T350 server with Intel® Xeon® E-2314 2.8GHz
> - Ubuntu 22.04 with low-latency Kernel
> - GRUB_CMDLINE_LINUX_DEFAULT="preempt=full nohz=off threadirqs"
> - Allowed non-power-of-two quantum

> NIC:
> - Broadcom 5720
> - Intel i210

> https://gitlab.freedesktop.org/pipewire/pipewire/-/merge_requests/1816#note_2215069

### Version 0.3.79. Result from @Mitolf

Hardware: Dante AVIO 2-channel Analog Output

> https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/3217#note_2180561