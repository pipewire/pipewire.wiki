This is a list of things left TODO:

# PipeWire

- [ ] Latency compensation. If ports of a node (left and right channel, for example) go through different paths in the graph (on through a delay element, for example) they might have different latencies and play unsynchronized. Latency compensation would add delay on some ports to align it with the other ports so that they arrive in sync at the end of the graph. There is discussion about if this needs the be done by default or not.

# PipeWire modules

- [x] Combine source.
- [ ] Figure out how we can load and connect an LV2 UI in a separate process for filter-chain.

# Tools


# Bluetooth


# PulseAudio server

- [ ] Implement device manager extension. This can be used to persistently change properties on nodes such as the name, profiles, channels, format etc. This can probably also be used to enhance streams. The implementation can then be used to implement the PulseAudio device manager extension so that pavucontrol can change the device name with a right click.
- [ ] Equalizer sink. Implement with filter chain. Probably needs some DBUS to control the params 
      so  that paeq works.

# JACK

- [ ] Implement netjack. ~~It looks like the original netjack packet format can be used with a more clever slaving and resampling algorithm to keep sender and receiver in sync. Progress can be followed [here](https://gitlab.freedesktop.org/wtaymans/pipewire/-/tree/netjack).~~ We will not implement this, the RTP Session module or RTP sink/source are better.
- [x] Implement netjack2 driver. Since 0.3.72
- [x] Implement netjack2 manager. Since 0.3.72

# AVB

- [ ] Implement PipeWire as an AVB endpoint. This is now part of the main branch but not yet functional.

# Code Cleanups

- [x] audioconvert needs to be rewritten to only use the converters. It would simplify the code a lot and make it easier to add new things.

# See Also

Issue tracker [TODO's](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues?sort=updated_desc&state=opened&label_name[]=To+Do) and [Enhancements](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues?sort=updated_desc&state=opened&label_name[]=enhancement)
