[[_TOC_]]

# Filter Chain

One can insert an arbitrary graph of LADSPA, LV2 and built in plugins in front of a sink or a source to create a new virtual filtered sink/source. This is similar to the PulseAudio module-ladspa-sink but more powerful.

To create a filter-chain, you need to load the `module-filter-chain` module. This can be done by adding a filter-chain fragment file (with a `.conf` extension) to `pipewire.conf.d/` or by running a standalone PipeWire program.


## Structure of the Configuration File

You can load the filer chain module with its configuration fragment (in a file with a `.conf` extension) by:

* Adding it to an existing configuration file, for example the main `pipewire.conf.d/` directory
  or the `pipewire-pulse.conf.d/` directory. Check [here](Config-PipeWire#configuration-file-pipewireconf) on
  how to do this.

* Make a new configuration file (by copying a default configuration file such as filter-chain.conf) and run it manually
  with `pipewire -c my-filter.conf`.

Please refer to the module-filter-chain [documentation](https://docs.pipewire.org/page_module_filter_chain.html) For more information.

# Examples

## Virtual Surround

Spatializer module is available since v0.3.66. You can use [this config](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/spatializer-7.1.conf) to create a virtual surround sink using SOFA parameters of your choice. Some of the places where you can get SOFA files include [sofacoustics.org](https://sofacoustics.org/data/database_sofa_0.6/ari/) and [sofaconventions.org](https://www.sofaconventions.org/data/database/clubfritz/).

Spatializer is also capable of working with any source position, as [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/spatializer-single.conf). Feel free to create new surround sound configurations with this.

There is a 5.1 virtual surround filter-chain config file using the KEMAR impulse response files [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/sink-virtual-surround-5.1-kemar.conf).

There is also a 7.1 virtual surround config file [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/sink-virtual-surround-7.1-hesuvi.conf).

## Filter Chain Configuration Parameters

The filter-chain has all the plugin properties on the capture stream. You can locate the capture stream with `pw-cli ls Node`. Remember the ID.

with `pw-dump <id>` you can see the this:

```json
...
    "Props": [
        {
            "volume": 1.000000,
            "mute": false,
            "channelVolumes": [ 1.000000 ],
            "channelMap": [ "MONO" ],
            "softMute": false,
            "softVolumes": [ 1.000000 ],
            "monitorMute": false,
            "monitorVolumes": [ 1.000000 ]
        },
        {
            "params": [
              "rnnoise:VAD Threshold (%)",
              50.000000
            ]
        }
    ],
...
```
The `params` property lists the available config options of the filter-chain. You can get the valid ranges from the `PropsInfo` of this property:

```json
...
    "PropInfo": [
        ...
        {
            "id": "id-01000000",
            "name": "rnnoise:VAD Threshold (%)",
            "type": { "default": 50.000000, "min": 0.000000, "max": 99.000000 },
            "params": true
        }
        ...
```
These properties are also visible when inspecting the LADSPA plugin with `analyseplugin`.

You can change the property like this:

```
pw-cli s <id> Props '{ params = [ "rnnoise:VAD Threshold (%)" 20.0 ] }'
```
## Dolby Pro Logic II

A 5.1 surround sound can be encoded into a stereo signal using Dolby Pro Logic II. Most AV receivers can decode the stereo signal and reconstruct a (simplified) 5.1 surround sound signal from it.

Use the [Dolby Pro Logic II](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/sink-dolby-surround.conf) filter-chain to create a sink that encodes such a signal.

## Matrix Spatialiser

To enhance the stereo effect of a signal, use the [Matrix spatialiser](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/sink-matrix-spatialiser.conf) virtual sink.

## Mixing input channels

[Here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/sink-mix-FL-FR.conf)
is an example for how to make a new virtual stereo sink that mixes the channels together and produces
on output channel.

## Duplicating output channels

[Here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/daemon/filter-chain/source-duplicate-FL.conf)
is an example for how to make a new virtual stereo source that duplicates the FL channel.


# Multiple Nodes Example

This example strings 3 nodes together:

```json
    ...
    {   name = libpipewire-module-filter-chain
        flags = [ nofail ]
        args = {
            node.description = "Demonic example"
            media.name = "Demonic example"
            filter.graph = {
                nodes = [
                    {
                        name = rev
                        type = ladspa
                        plugin = revdelay_1605
                        label = revdelay
                        control = {
                            "Delay Time (s)" = 2.0
                        }
                    }
                    {
                        name = pitch
                        type = ladspa
                        plugin = am_pitchshift_1433
                        label = amPitchshift
                        control = {
                            "Pitch shift" = 0.6
                        }
                    }
                    {
                        name = rev2
                        type = ladspa
                        plugin = g2reverb
                        label = G2reverb
                        control = {
                            "Reverb tail" = 0.5
                            "Damping" = 0.9
                        }
                    }
                ]
                links = [
                    { output = "rev:Output" input = "pitch:Input" }
                    { output = "pitch:Output" input = "rev2:In L" }
                ]
                inputs = [ "rev:Input" ]
                outputs = [ "rev2:Out L" ]
            }
            capture.props = {
                node.name = "effect_input.filter-chain-demonic"
                #media.class = Audio/Sink
            }
            playback.props = {
                node.name = "effect_output.filter-chain-demonic"
                #media.class = Audio/Source
            }
        }
    }
    ...
```
Note the `links = []` array that has an array of links between ports of plugins. You can link output ports to multiple input ports but you need a mixer to link multiple streams to an input port.

The `inputs = []` and `outputs = []` arrays contains the ports that are linked to the stream inputs and outputs. You don't have to link all ports of the graph.

# Delay

The folling examples adds 1.5 seconds delay between capture and playback.

```json
context.modules = [
  {  name = libpipewire-module-filter-chain
     args = {
         node.description = "Delay line loopback"
         media.name       = "Delay line loopback"
         filter.graph = {
             nodes = [
                {
                    type = builtin
                    name = delay
                    label = delay
                    control = { }
                    config = {
                        "max-delay" = 2.0
                    }
                    control = {
                        "Delay (s)" = 1.5
                    }
                }
             ]
         }
         capture.props = {
             node.name      = "effect_input.delay"
             audio.channels = 2
             audio.position = [ FL FR ]
         }
         playback.props = {
             node.name      = "effect_output.delay"
             audio.channels = 2
             audio.position = [ FL FR ]
         }
      }
   }
]
```

# Draining

When a filter chain becomes unused, it will pause. This will stop playback of all audio abruptly. In some cases like with long reverberation, you would want to let the filter continue until it is drained.

You can stop the filter from pausing by setting the `node.pause-on-idle = false` property in the playback and capture props like so:

```json
            ...
            capture.props = {
                ...
                node.pause-on-idle = false
                ...
            }
            playback.props = {
                ...
                node.pause-on-idle = false
                ...
            }
            ...
```
The session manager will eventually pause/suspend the filter chain after a timeout. You can also set this timeout with `session.suspend-timeout-seconds` (0 to disable suspend of the filter).

# Other Filters


Check out [here](Config-JACK#filters-and-effects) for more ways of adding filters to the processing graph.

