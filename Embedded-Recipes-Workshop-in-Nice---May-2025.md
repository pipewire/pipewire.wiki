[[_TOC_]]

# Embedded Recipes Workshop in Nice (May 2025)

The [Embedded Recipes](https://embedded-recipes.org/) conference organizers together with [Collabora](https://www.collabora.com) are organizing a PipeWire workshop (you may also call it a single-day hackfest) on **Friday May 16th 2025**, as part of the Embedded Recipes [workshops track](https://embedded-recipes.org/2025/workshops/).

## Venue

- Sheraton Nice Airport Hotel
- There's a room for about 15 people reserved for PipeWire
- Coffee and snacks are provided

## Other events

- Co-located with libcamera & yocto workshops (see the [conference workshops page](https://embedded-recipes.org/2025/workshops/))
- Possibly other events to be announced around the same dates (including GStreamer)
- The [Embedded Recipes](https://embedded-recipes.org/2025/) conference is taking place on the days before: [May 14th & 15th](https://embedded-recipes.org/2025/schedule/) (at a different venue, 10mins walk from the Sheraton)

## Practical information

- See https://embedded-recipes.org/2025/attend/
- PS: While the conference has a registration fee, the PipeWire workshop is free to attend

# Topics

If you would like to bring up topics for discussion, please add them on the list below. Since this is only 1 day, let's try to do our homework and bring up specific topics for discussion instead of general areas of interest.

- Using more Rust in PipeWire development ~ @gkiagia
- Securing the future of PipeWire's Bluetooth integration ~ @gkiagia
- ...

# Attendees

1. George Kiagiadakis (Collabora)
1. Wim Taymans (Red Hat)