```ruby
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = source
          pipe.filename = "/tmp/virtualdevice"
          audio.position = [ FL FR ]
          foo-bar = baz
          stream.props = {
              node.name = VirtualMic
          }
      }
  }
]
```

```yaml
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = source
          pipe.filename = "/tmp/virtualdevice"
          audio.position = [ FL FR ]
          foo-bar = baz
          stream.props = {
              node.name = VirtualMic
          }
      }
  }
]
```

