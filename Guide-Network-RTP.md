[[_TOC_]]

# Native RTP network examples

These are some examples for how to set up RTP network streaming between two machines.

You can also use the PulseAudio [RTP modules](Guide-PulseAudio-Tricks#setup-rtp-network-streaming-between-machines)
to make some things easier.

# Point-to-point RTP

To set up a point-to-point connection between a sender and a receiver you need to dedicate
a port, let's say 46000. You also need the IP address of the receiver, say 192.168.1.39

## Sender

On the sender side (sink), make a file 
`~/.config/pipewire/pipewire.conf.d/20-rtp-sink.conf` with the content:

```
context.modules = [
{   name = libpipewire-module-rtp-sink
    args = {
        destination.ip = "192.168.1.39"
        destination.port = 46000
        stream.props = {
            media.class = "Audio/Sink"
            node.name = "rtp-sink"
        }
    }
}
]
```

Restart the sender PipeWire process to activate the sink.

Check out [here](https://docs.pipewire.org/page_module_rtp_sink.html) for more options.

## Receiver

On the receiver side (source), make a file 
`~/.config/pipewire/pipewire.conf.d/20-rtp-source.conf` with the content:

```
context.modules = [
{   name = libpipewire-module-rtp-source
    args = {
        source.ip = "0.0.0.0"
        source.port = 46000
        sess.ignore-ssrc = true         # so that we can restart the sender
        stream.props = {
            media.class = "Audio/Source"
            node.name = "rtp-source"
        }
    }
}
]
```

Check out [here](https://docs.pipewire.org/page_module_rtp_source.html) for more options.

Restart the receiver PipeWire process to activate the source.

# Multicast RTP/SAP

We can also do the same with multicast and with SAP. This makes it possible to not have to hardcode the
destination.ip into the sink. Instead we can send the packets to a multicast address and use SAP to
announce the stream.

Receivers can then receive the SAP messages and start receiving the RTP streams without further
configuration.

## Sender

On the sender side (sink), make a file
`~/.config/pipewire/pipewire.conf.d/20-rtp-mcast-sink.conf` with the content:

```
context.modules = [
{   name = libpipewire-module-rtp-sap
    args = {
        sap.ip = "224.0.0.56"
        sap.port = 9875
    }
}
{   name = libpipewire-module-rtp-sink
    args = {
        destination.ip = "224.0.0.56"
        stream.props = {
            media.class = "Audio/Sink"
            node.name = "rtp-mcast-sink"
            sess.sap.announce = true
        }
    }
}
]
```
Restart the sender PipeWire process. The sender will create a random port and will announce this in SAP.

## Receiver

On the receiver side (source), make a file
`~/.config/pipewire/pipewire.conf.d/20-rtp-mcast-source.conf` with the content:

```
context.modules = [
{   name = libpipewire-module-rtp-sap
    args = {
        sap.ip = "224.0.0.56"
        sap.port = 9875
    }
}
]
```

Restart the PipeWire process. You only need a SAP module, it will automatically create streams for all SAP messages
on the sap.ip and sap.port.

Check out [here](https://docs.pipewire.org/page_module_rtp_sap.html) for more info.

