
[[_TOC_]]

# Multiple rates

PipeWire enables by default 1 samplerate (48000 Hz). Everything will be resampled
to this rate with a high quality resampler. This is enough to give a good default
experience.

It might be interesting to enable more samplerates to avoid resampling when playing
media that is not sampled at 48000Hz.

To do this, make a new file in `~/.config/pipewire/pipewire.conf.d/10-rates.conf` with
the following contents:

```
# Adds more common rates
context.properties = {
    default.clock.allowed-rates = [ 44100 48000 88200 96000 ]
}
```
You can add more common rates to the list.

Find more detailed information [here](Config-PipeWire).

# Force samplerate

By default PipeWire will select the best samplerate for the audio processing based on the
allowed sample rates and available streams.

You can also force a samplerate of, for example 96000Hz, for the data processing with:

```
pw-metadata -n settings 0 clock.force-rate 96000
```

You can go back to the dynamic samplerate selection:

```
pw-metadata -n settings 0 clock.force-rate 0
```

See also the [configuration](Config-PipeWire#setting-sample-rates).
