[[_TOC_]]

# General

If submitting a bug/issue please provide PipeWire **version**, **distribution** and the **desktop environment**. It may also be a good idea to try a nightly or master first as your problem may have been fixed. 

Journalctl with: 
````
journalctl -xe | grep pipewire
journalctl -xe | grep wireplumber
or
journalctl --user-unit=pipewire --user-unit=wireplumber --user-unit=pipewire-pulse -f
or
journalctl --user -u pipewire --user -u wireplumber --user -u pipewire-pulse -f
````

`pw-cli ls`, `pactl list` and `pactl show`to list objects.

`pactl subscribe` to list events as they happen.

`pw-cli info <id>` to get more info about an object. 

`pw-dump`can dump the objects in JSON formats, ready for processing with other JSON query tools like `jq`. If filing an issue please include this as an attachment.

`pw-metadata -n route-settings` will also list similar things to `pw-dump`.

Tools like `pw-cat`, `pw-record`, and `pw-play` are also available. See the man pages. 

`pw-top` will tell you the node, period, rate, and latency.

## PipeWire Debugging Options

```` 
PIPEWIRE_DEBUG=5 <app> 2>log
```` 
Usually debug 4 or 5 should be verbose enough but 6 is also available.

Setting the debugging level too high can cause problems for the systemd journal which can now be disabled with:
````
PIPEWIRE_LOG_SYSTEMD=false
````
For easy copying and pasting:
````
PIPEWIRE_LOG_SYSTEMD=false PIPEWIRE_DEBUG=5 <app> 2>log
````
Wireplumber has its own debug options please see [here](https://pipewire.pages.freedesktop.org/wireplumber/daemon-logging.html).

For debugging the Pipewire daemon, the log level can changed while the daemon is running:
```
pw-metadata -n settings 0 log.level 4
```
Note that some parts such as Bluetooth run inside Wireplumber and their logging is controlled by the Wireplumber options.

## PipeWire Not Loading

PipeWire and Wireplumber have three main directories where its files are stored `/usr/share/pipewire` and `/usr/share/wireplumber`. It is recommended if you would like to make configuration changes that you copy them from `/usr` to the corresponding directories in `/etc` or your `~/.config` folder. 

The third directory is `~/.local/state/wireplumber` which contains four files `default-nodes`, `default-profile`, `default-routes`, and `restore-stream`. These files store your default profile and volume levels so if you're having profile or volume issues it may be that these files need to be deleted and refreshed.

Make sure your systemd services and sockets are enabled and started (as --user) this includes `pipewire.service`, `pipewire.socket`, `pipewire-pulse.service`, `pipewire-pulse.socket` and `wireplumber.service`.        

## Running development version of Pipewire

You may want to compile and run a development version of Pipewire, without replacing any system Pipewire installation. It can be done as follows:
```
# On Fedora: install git and build dependencies
sudo dnf install git make systemd-devel ncurses-devel readline-devel alsa-lib-devel dbus-devel libsndfile-devel libcanberra-devel libunwind-devel gcc gcc-c++ gettext libusb1-devel bluez-libs-devel libldac-devel sbc-devel glib2-devel meson webrtc-audio-processing-devel pkgconf-pkg-config pulseaudio-libs-devel fdk-aac-devel
# Debian/Ubuntu:
sudo apt build-dep pipewire
sudo apt install git make

# Get source code
git clone https://gitlab.freedesktop.org/pipewire/pipewire.git
# git clone https://gitlab.freedesktop.org/pvir/pipewire.git  # or from a developer's repository
cd pipewire
# Optionally, go to specific version:
git checkout 0.3.56
# git checkout origin/a2dp-less-skip  # or checkout a specific branch
# Configure
./autogen.sh -Dlibcamera=disabled -Dsession-managers=wireplumber -Dbluez5=enabled

# Stop any running Pipewire services
systemctl --user stop pipewire.service pipewire.socket pipewire-pulse.service pipewire-pulse.socket wireplumber.service

# Build and run without installing, Ctrl+C terminates
make run
```
You can also apply patches or make changes to the source code, or `git checkout` to a different version, and do `make run` again.
Logout/login to go back to the system Pipewire.

You can also start a second terminal, go to the pipewire checkout directory and type:

```
make shell
```
This enters a shell prompt with the environment set to the build version of pipewire. You can
then run the tools etc from the build directory:

```
pw-cli i 0
pw-top
pw-cat -pv /my/filw.wav
jack_simple_client
....
```

# Bluetooth

## Missing Bluetooth Profiles / Volume Control / etc.

If a Bluetooth device is missing some profiles (A2DP or HFP/HSP ones are missing, or device does not connect at all), hardware volume doesn't work properly, or there are other problems, the problem may also be in the Linux Bluetooth stack below pipewire (BlueZ, kernel drivers, firmware, hardware), or on the headset device side. So consider, in order:

1. Unpair device, pair device again.
2. Clear BlueZ cache: Delete all files and directories under `/var/lib/bluetooth/` and `systemctl restart bluetooth`. This forgets all devices and you need to pair devices again.
3. Check BlueZ version. Consider upgrading if possible with your distro. You can see what is the latest version [here](https://github.com/bluez/bluez/tags).

## Debugging

 1. `systemctl --user stop wireplumber.service` if using systemctl, otherwise something like `pkill -f wireplumber`. Disconnect Bluetooth device.
 2. Run from command line with:
```
WIREPLUMBER_DEBUG="spa.bluez5*:4,pw.context:3,s-monitors:4,m-lua-scripting:4,s-device:4" wireplumber > pipewire-bluez.log 2>&1
```
 4. Connect Bluetooth device, and cause the problem behavior. 
 5. Stop Wireplumber with `ctrl-C` and attach the produced `pipewire-bluez.log` to the issue. It contains information of what PipeWire tried to do during the connection.

If you are using pipewire-media-session instead of wireplumber, do the corresponding steps, and replace `WIREPLUMBER_DEBUG="spa.bluez5*:4"` by `PIPEWIRE_DEBUG="spa.bluez5*:4"`.

Also check if `dmesg` contains messages about missing Bluetooth firmware. There have been issues where the proper firmware was not getting installed for devices like for example the [Asus BT400](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/702) that uses a Broadcom chipset.

## Bluetooth Traffic Dump

In some cases, a Bluetooth traffic dump may be necessary. It can be taken as follows:

1. Power off the Bluetooth device (important!)
2. Run `sudo btmon -p debug -A -S -w dump.btsnoop`
3. Connect device
4. Do the problematic operation
5. Ctrl-C btmon.

Advanced users may inspect the traffic dump by opening it e.g. with Wireshark.

Note that the traffic dump contains all Bluetooth traffic during the time when the dump was taken, including MAC addresses etc. identifying information.

# Crashes

If PipeWire is crashing we require a backtrace to fix it this can be obtained either of two ways:

Attach to the currently running PID:
```
gdb -p `pidof <program>`
# At the prompt
continue
# Let the program crash
thread apply all backtrace
```
Or run the program **through** gdb. In this example Wireplumber is crashing you would first have to stop Wireplumber with systemctl, then start it 
with gdb: 
```
gdb wireplumber
# At the prompt
continue
# Let the program crash
thread apply all backtrace
```
Or if you are a Fedora user you can use this [tutorial](https://fedoramagazine.org/file-better-bugs-coredumpctl/) and use coredumpctl and ABRT to generate coredumps to add to reports.

# Memory Leaks

If PipeWire is taking up too much memory you can run it through valgrind, for example. If it's `pipewire-pulse` that's having the problem either stop it using systemctl or kill it and run it with:
```
valgrind --trace-children=yes --leak-check=full pipewire-pulse
```  
Then add the output to the report.

# My Audio Card Does Not Have XYZ

This could be a problem with the mixer detection code.

# Client too slow! rate:64/48000 pos:532930560 status:triggered

Too slow means that it could not provide data in the current cycle. Status:triggered means that the server woke it up but the client has not completed the work and changed the status to finished, it's actually not even awake so the process has not even been scheduled. It goes from not-triggered -> triggered -> awake -> finished.

# No Node Found

Is when a stream was playing and then the target node disappeared. Happens when a monitor stream in pavucontrol is destroyed.

# Underrun/Underflow and Broken Pipe Errors

Applications not providing data fast enough and not enough headroom in the device to recover. Increase headroom.

# Stuttering Audio (in Virtual Machine)

Normally this should not happen but is usually caused by jittery drivers. In a VM
this is most common because the device is emulated.

you can usually fix this problem by giving more headroom in the alsa device
ringbuffer.

You need to edit the WirePlumber configuration as follows (since 0.5, the older 0.4 version
uses lua scripts for configuration):

```
mkdir -p ~/.config/wireplumber/wireplumber.conf.d/
cd ~/.config/wireplumber/wireplumber.conf.d
```

Then make `~/.config/wireplumber/wireplumber.conf.d/50-alsa-config.conf` in an editor and
add:

```
monitor.alsa.rules = [
  {
    matches = [
      # This matches the value of the 'node.name' property of the node.
      {
        node.name = "~alsa_output.*"
      }
    ]
    actions = {
      # Apply all the desired node specific settings here.
      update-props = {
        api.alsa.period-size   = 1024
        api.alsa.headroom      = 8192
      }
    }
  }
]
```
Afterwards, restart everything via `systemctl --user restart wireplumber pipewire pipewire-pulse`


When running inside a VM, also disable the Firefox speech dispatch as
explained [here](Performance-tuning#firefox).

# Loud Pops When Starting a Sound

This is likely because of power saving settings. You can try:

```
echo 0 > /sys/module/snd_hda_intel/parameters/power_save
```

This setting will last until you reboot. You can make it permanent by adding this text to `/etc/modprobe.d/audio_disable_powersave.conf`:
```
options snd_hda_intel power_save=0
```

You can also try to disable suspend in wireplumber:


Make `~/.config/wireplumber/wireplumber.conf.d/50-alsa-suspend.conf` in an editor and
add:

```
monitor.alsa.rules = [
  {
    matches = [
      # This matches the value of the 'node.name' property of the node.
      {
        node.name = "~alsa_output.*"
      }
    ]
    actions = {
      update-props = {
        session.suspend-timeout-seconds = 0
      }
    }
  }
]
```

# Xruns

There are 2 causes of xruns:

 1. Application's can't complete the cycle in time. It can be because the kernel didn't wake them up in time or that they didn't get enough time allocated.
 2. Driver timing are to tight. The driver is not woken up fast enough to keep the buffer filled.

1 can cause 2. 2 can be improved by adding more buffering (increasing headroom). Adding headroom does not improve 1.

With pw-top you can see what causes the xruns. If the application xruns are increasing, it's 1. If it's only the driver increasing xruns, it's 2. If both are increasing it's 1 causing 2.

# Audio data dump

Since 0.3.68, PipeWire can save a wav file with the raw audio captured from and played to hardware (or streams).

To do this, locate the node you would like to dump with `pw-cli ls Node`. You can refer to the node by id
or by node.name in the following lines.

To start capture to `/tmp/out.wav` from a node (with node.name = alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0):

```
pw-cli s alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0 Props '{ params = [ debug.wav-path "/tmp/out.wav" ] }'
```

To stop capture set an empty debug.wav-path value:
```
pw-cli s alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0 Props '{ params = [ debug.wav-path "" ] }'
```

The captured file in the exact format and channels as sent or received from the hardware is then in `/tmp/out.wav` and can be opened with tools like audacity for inspection.

