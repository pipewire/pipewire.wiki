> :warning: &nbsp;&nbsp; This documentation is moved to: https://docs.pipewire.org/page_config.html and https://docs.pipewire.org/page_man_pipewire-props_7.html

[[_TOC_]]

# Devices

Devices such as audio sinks and sources, cameras and Bluetooth endpoints are usually created and managed by the session manager.

For the various device settings, see the documentation of [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/daemon/configuration.html).

# Runtime Settings

Most ALSA and virtual devices can be configured at runtime as well with the params properties.

First you need to locate the source or sink you want to change with `pw-cli ls Node`. Nodes with the `media.class = "Audio/Sink"` property are audio sinks. 

Use `pw-cli e <id> Props` to list the available properties of the sink. Look for a `params` property, it has a list of key/value pairs that can be changed at runtime.

It looks like this in `pw-dump <id>` for an ALSA device:

```json
{
...
          "params": [
              "audio.channels",
              2,
              "audio.rate",
              0,
              "audio.format",
              "UNKNOWN",
              "audio.position",
              "[ FL, FR ]",
              "audio.allowed-rates",
              "[  ]",
              "api.alsa.period-size",
              0,
              "api.alsa.period-num",
              0,
              "api.alsa.headroom",
              0,
              "api.alsa.start-delay",
              0,
              "api.alsa.disable-mmap",
              false,
              "api.alsa.disable-batch",
              false,
              "api.alsa.use-chmap",
              false,
              "api.alsa.multi-rate",
              true,
              "latency.internal.rate",
              0,
              "latency.internal.ns",
              0,
              "clock.name",
              "api.alsa.c-1"
            ]
          }
...
```

One or more params can be changed like this:

```
pw-cli s <id> Props '{ params = [ "api.alsa.headroom" 1024 ] }'
```
Note that these new settings are not saved (yet) by any session manager. You should reapply them for each session manager restart.

We refer the the [Client stream Configuration](Config-client#streamproperties) for an explanation of the properties.

## ALSA Properties

These are properties for how the device is configured.

### `audio.channels`

The number of audio channels to open the device with. Defaults depends on the profile of the device.

### `audio.rate`

The audio rate to open the device with. Default is 0, which means to open the device with a rate as close to the graph rate as possible.

### `audio.format`

The audio format to open the device in. By default this is "UNKNOWN", which will open the device in the best possible bits (32/24/16/8..). You can force a format like S16_LE or S32_LE.

### `audio.position`

The audio position of the channels in the device. This is auto detected based on the profile. You can configure an array of channel positions, like "[ FL, FR ]".

### `audio.allowed-rates`

The allowed audio rates to open the device with. Default is "[ ]", which means the device can be opened in any supported rate.

Only rates from the array will be used to open the device. When the graph is running with a rate not listed in the allowed-rates, the resampler will be used to resample to the nearest allowed rate.

### `api.alsa.period-size`

The period size to open the device in. By default this is 0, which will open the device in the default period size to minimize latency. 

### `api.alsa.period-num`

The amount of periods to use in the device. By default this is 0, which means to use as many as possible.

### `api.alsa.headroom`

The amount of extra space to keep in the ringbuffer. The default is 0. Higher values can be configured when the device read and write pointers are not accurately reported.

### `api.alsa.start-delay`

Some devices require a startup period. The default is 0. Higher values can be set to send silence samples to the device first.

### `api.alsa.disable-mmap`

Disable mmap operation of the device and use the ALSA read/write API instead. Default is false, mmap is preferred.

### `api.alsa.disable-batch`

Ignore the ALSA batch flag. If the batch flag is set, ALSA will need an extra period to update the read/write pointers. Ignore this flag from ALSA can reduce the latency. Default is false.

### `api.alsa.use-chmap`

Use the driver provided channel map. Default is false because many drivers don't report this correctly.
        
### `api.alsa.multi-rate`

Allow the card to be opened with multiple rates as the same time. Default is true. Some cards have only one clock and will us the wrong sample rates when a kernel < 5.16 is used.
        
## Systemic latency calibration

In addition to the software controlled latency (by controlling the amount of buffered data in the device), there
is also a hardware latency (caused by analog to digital conversion, bus transport, usb timings, etc...). This
latency is usually a few milliseconds.

This latency can be measured with a loopback cable and a tool like `jack_iodelay`. This tool will send a test
signal on the output port and measures when that signal is captured on the input port; this will be the total roundtrip delay of the signal. The tool will assume an equal latency for the source as the sink node (there is no way to know for sure) and will report this latency in samples.

Note that this latency can change when the device is reconfigured. For (pro-audio) devices that share a clock this latency will then be constant for as long as the device is running with the same configuration.

### `latency.internal.rate` and `latency.internal.ns`

You can statically set the systemic latency with these properties. You can express this latency as a combination of samples and nanoseconds.

### Runtime calibration

The latency can be configured at runtime on the device with:

```
pw-cli s <device-name> ProcessLatency '{ rate=<latency> }'
```
Fill in the `<device-name>` with the `node.name` (constant accross reboots) or the `object.id` of the
sink and soure to adjust.

This latency will now be added to the software latency on the sink/source ports. You can check the new
increased latency with a tool like `jack_lsp -L`.

You can safely rerun the calibration whenever the quantum or samplerate of the graph has changed.

## Driver Properties

These control how the device behaves as a driver of the graph and follower.

### `clock.name`

The name of the clock. This name is auto generated from the card index and stream direction. Devices with the same clock name will not use a resampler to align the clocks. This can be used to link devices together with a shared word clock.

In Pro Audio mode, nodes from the same device are assumed to have the same clock and no resampling will happen when linked togther. So, linking a capture port to a playback port will not use any adaptive resampling in Pro Audio mode.

In Non Pro Audio profile, no such assumption is made and adaptive resampling is done in all cases by default. This can also be disabled by setting the same clock.name on the nodes.

### `priority.driver`

The priority of choosing this device as the driver in the graph. The driver is selected from all linked devices by selecting the device with the highest priority.

Normally, the session manager assigns higher priority to sources so that they become the driver in the graph. The reason for this is that adaptive resampling should be done on the sinks rather than the source to avoid signal distortion when capturing audio.

## Session Properties

These properties control how the device behaves in a session, controlled by a session manager.

### `priority.session`

The priority for selecting this device as the default device.

# ALSA Card profiles

The sound card profiles ("Analog stereo", "Analog stereo duplex", ...) except "Pro Audio" come from two sources: 

- [UCM: ALSA Use Case Manager](https://github.com/alsa-project/alsa-ucm-conf/): the profile configuration system from ALSA.
- ACP ("Alsa Card Profiles"): [Pulseaudio's profile system](https://www.freedesktop.org/wiki/Software/PulseAudio/Backends/ALSA/Profiles/) ported to Pipewire.

See the above links on how to configure these systems.

For ACP, Pipewire looks for the profile configuration files under `~/.config/alsa-card-profile`, `/etc/alsa-card-profile`, and `/usr/share/alsa-card-profile/mixer`. The path / profile-set files are in subdirectories `paths` / `profile-sets` of these directories. It is possible to override individual files locally by putting a modified copy into the ACP directories under `~/.config` or `/etc`.

You should however consider [making a new profile set](https://www.freedesktop.org/wiki/Software/PulseAudio/Backends/ALSA/Profiles/) for your device and adding UDEV detection rules for it in `90-pipewire-alsa.rules`, and contributing it to Pipewire & Pulseaudio, so that it benefits also other users.