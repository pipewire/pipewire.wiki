# Some Ideas For The Implementation Of PulseAudio Features:

 - Rewind, we don't want to do rewinds, we would keep the maximum latency at the render side reasonably
   low (21.33ms, or 1024 samples at 48KHz) and simply allow this worst-case latency between volume changes
   or stream switches.
 - Mixer API: ~~We would like to have each physical output path have a separate node, selecting on path would
   make other paths inactive, possibly using UCM.~~ We're using the exact same PulseAudio code and config files to set up devices and mixers, ports and jack detection. There should not be any difference between PipeWire and PulseAudio on how devices are detected and controlled.
 - Work on implementing the relevant modules, module-null-sink is implemented.
 - ~~Maybe we can use the PulseAudio config files to generate a UCM profile for the card.~~ We use the PulseAudio code to deal with UCM.

 ## Design:
 - ~~We would want a client replacement library that speaks directly to PipeWire to make existing clients work without changes.~~ We use a replacement daemon so that we don't have to change the PulseAudio client library in Flatpaks. A replacement daemon is also easier to implement and maintain.
 - ~~PulseAudio clients use the pw_stream API and this run format conversions locally. This currently wakes up the client for each period but is more than fast enough because we do buffering in the adapter and don't need to go to the application more often than the requested latency.~~ pipewire-pulse uses the stream API but only wakes up the PulseAudio client when more data is needed.
 - ~~Volume/mute control on the stream is done on the local adapter. Also rate changes are handled locally.~~ pipewire-pulse does volume and mute in the pw-stream it manages.
 - ~~We get precise timing from the server and calculate local buffering in the adapter.~~ 
 - Config info like default devices, volumes, stream routing is managed with metadata. This makes it possible to control and introspect the state with simple tools like pw-metadata without the need to design complicated APIs. The session manager can easily save metadata to a database as well.
