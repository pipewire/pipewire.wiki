
[[_TOC_]]

# Upmixing

PipeWire does by default not upmix stereo audio to multichannel 5.1 or 7.1
audio because the default behaviour should be to route the audio as is and not
apply filters to the audio.

You need to manually enable umixing in PulseAudio clients, Native PipeWire clients
and bluetooth devices.

## PulseAudio 

To enable upmixing for PulseAudio clients, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/40-upmix.conf` with the content:
```
# Enables upmixing
stream.properties = {
    channelmix.upmix      = true
    channelmix.upmix-method = psd
    channelmix.lfe-cutoff = 150
    channelmix.fc-cutoff  = 12000
    channelmix.rear-delay = 12.0
}
```

And restart the pipewire-pulse server.

## Native clients 

To enable upmixing for Native clients, make a file
`~/.config/pipewire/client-rt.conf.d/40-upmix.conf` and
`~/.config/pipewire/client.conf.d/40-upmix.conf` with the content:
```
# Enables upmixing
stream.properties = {
    channelmix.upmix      = true
    channelmix.upmix-method = psd
    channelmix.lfe-cutoff = 150
    channelmix.fc-cutoff  = 12000
    channelmix.rear-delay = 12.0
}
```

And restart the pipewire client.

## Bluetooth devices

When pipewire is used as a Bluetooth receiver (Speaker) the incomming stereo
signal can be upmixed to multichannel.

To enable upmixing for Bluetooth input, make a file
`~/.config/wireplumber/wireplumber.conf.d/40-upmix.conf with the content:
```
# Enables upmixing
stream.properties = {
    channelmix.upmix      = true
    channelmix.upmix-method = psd
    channelmix.lfe-cutoff = 150
    channelmix.fc-cutoff  = 12000
    channelmix.rear-delay = 12.0
}
```

And restart Wireplumber.

# Virtual upmix sink (simple)

This example makes a new stereo sink that uses the standard stream upmixing
logic to upmix to 5.1. You can link the output to a 5.1 sink.

This can be an alternative to globally enabling the upmixing for all clients.
Simply redirect the client to this sink to make it upmix or directly to the
5.1 sink to get stereo.

To create the virtual upmix sink, make a file
`~/.config/pipewire/pipewire.conf.d/25-sink-upmix-5.1.conf` with the content:

```
# An example of a stereo sink that will use the stream upmixing
# logic to create a 5.1 output.
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            node.description = "Stereo to 5.1 upmix"
            # global audio position makes sure capture and playback streams
            # are converted to this intermediate format
            audio.position = [ FL FR ]
            capture.props = {
                node.name = "sink.upmix_5_1"
                media.class = "Audio/Sink"
            }
            playback.props = {
                node.name = "playback.upmix-5.1"
                # from the internal stereo to this will trigger upmix
                audio.position = [ FL FR FC LFE RL RR]
                # where to link this to
                target.object = "alsa_output.usb-0d8c_USB_Sound_Device-00.analog-surround-51"
                # Makes sure the output channels stay as configured rather than matching
                # target.object
                stream.dont-remix = true
                # Prevents this loopback module from being processed (and possibly keeping
                # CPU at a higher power state) when its not linked to clients
                node.passive = true

                # enable channel upmix
                channelmix.upmix = true
                # use fancy filters and delay in the rear channels
                channelmix.upmix-method = psd  # none, simple
                # filter for the LFE channel
                channelmix.lfe-cutoff = 150
                # filter for the FC channel
                channelmix.fc-cutoff  = 12000
                # delay in rear channels
                channelmix.rear-delay = 12.0
                # optionally widen the stereo field
                channelmix.stereo-widen = 0.0
                # optionally apply a hilbert transform (90 degree phase shift) on
                # the rear channels to improve spacialization
                channelmix.hilbert-taps = 0
            }
        }
    }
]
```

