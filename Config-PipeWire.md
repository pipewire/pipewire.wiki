> :warning: &nbsp;&nbsp; This documentation is moved to: https://docs.pipewire.org/page_config.html and https://docs.pipewire.org/page_man_pipewire_conf_5.html

[[_TOC_]]

# General

One of the design goals of PipeWire is to be able to closely control and configure all aspects of the processing graph.
If a configuration option is not yet available, it's a bug that should be fixed.

A fully configured PipeWire setup runs various pieces, each with their configuration options and files:

* pipewire: The PipeWire main daemon that runs and coordinates the processing. The configuration can be found in this page.
* pipewire-pulse: The PipeWire PulseAudio replacement server. It converts the pulseaudio client protocol to native PipeWire protocol. The configuration can be found [here](Config-PulseAudio). This also configures the properties of the PulseAudio clients connecting to it.
* wireplumber: A large part of the initial configuration of the devices is performed by the session manager. It typically loads the alsa devices and configures the profiles, port volumes and more. Its configuration can be found [here](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/home). The session manager also configured new clients and connects them to the right nodes as configured in the session manager policy.
* PipeWire clients: Each native PipeWire client also loads a configuration file. Depending on the client type the configuration is documented [here](Config-JACK) for JACK client and [here](Config-client) for other clients.

See also [PipeWire Docs](https://docs.pipewire.org/index.html)

# Configuration File (`pipewire.conf`)

[PipeWire ArchWiki](https://wiki.archlinux.org/index.php/PipeWire)

[PipeWire Gentoo Wiki](https://wiki.gentoo.org/wiki/Pipewire)

The PipeWire configuration template file is located in `/usr/share/pipewire/pipewire.conf`. You can copy and edit the file to `/etc/pipewire/` or `~/.config/pipewire/pipewire.conf`.

Since 0.3.45 you can also copy fragments of the config file to a file (with a `.conf` extension) in the directories `/usr/share/pipewire/pipewire.conf.d/`, `/etc/pipewire/pipewire.conf.d/` or `~/.config/pipewire/pipewire.conf.d/`.

*Note!! Properties will override the previous ones, array entries will be appended. It is not possible yet to change
or remove existing array entries. This only applied to the first level objects, arrays in properties will be overwritten as usual.*

See [here](#split-file-configuration) for some examples.

The `pipewire.conf` has some basic configuration for the graph scheduling settings and what modules to load by default. It will also launch the session manager, though that is no longer the default and has been depreciated after 0.3.22 in favor of a systemd service file.

To configure the session manager, please go to:

 * [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/configuration.html)
 * [Media Session](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/home)

## Properties

A config file can contain a set of properties to set up the context.

```python
context.properties = {
    ## Configure properties in the system.
    #library.name.system                   = support/libspa-support
    #context.data-loop.library.name.system = support/libspa-support
    #support.dbus                          = true
    #link.max-buffers                      = 64
    link.max-buffers                       = 16                       # version < 3 clients can't handle more
    #mem.warn-mlock                        = false
    #mem.allow-mlock                       = true
    #mem.mlock-all                         = false
    #clock.power-of-two-quantum            = true
    #log.level                             = 2
    #cpu.zero.denormals                    = false

    core.daemon = true              # listening for socket connections
    core.name   = pipewire-0        # core name and socket name

    ## Properties for the DSP configuration.
    #default.clock.rate          = 48000
    #default.clock.allowed-rates = [ 48000 ]
    #default.clock.quantum       = 1024
    default.clock.min-quantum   = 16
    #default.clock.max-quantum   = 2048
    #default.clock.quantum-limit = 8192
    #default.clock.quantum-floor = 4
    #default.video.width         = 640
    #default.video.height        = 480
    #default.video.rate.num      = 25
    #default.video.rate.denom    = 1
    #
    #settings.check-quantum      = false
    #settings.check-rate         = false
    #
    # These overrides are only applied when running in a vm.
    vm.overrides = {
        default.clock.min-quantum = 1024
    }
}
```

### General Options

```json
library.name.system = support/libspa-support
```
The name of the shared library to use for the system functions for the main thread.

```json
context.data-loop.library.name.system = support/libspa-support
```
The name of the shared library to use for the system functions for the data processing
thread. This can typically be changed if the data thread is running on a realtime
kernel such as EVL.

```json
support.dbus = true
```
Enable DBus support. This will enable DBus support in the various modules that require
it. Disable this if you want to globally disable DBus support in the process.

```json
link.max-buffers = 64
```
The maximum number of buffers to negotiate between nodes. Note that version < 3 clients
can only support 16 buffers. More buffers is almost always worse than less, latency
and memory wise.

```json
mem.allow-mlock = true
```
Try to mlock the memory for the realtime processes. Locked memory will not be swapped out by the kernel
and avoid hickups in the processing threads. The amount of locked memory is however limited per
user but can be [tuned](Performance-tuning#rlimits).

```json
mem.warn-mlock = false
```
Warn about failures to lock memory. 

```json
mem.mlock-all = false
```
Try to mlock all current and future memory by the process.


```json
log.level = 2
```
The default log level used by the process.

```json
cpu.zero.denormals = false
```
Configures the CPU to zero denormals automatically. This will be enabled for the data processing thread
only, when enabled.


### Daemon Options

```json
core.daemon = true
```
Makes the PipeWire process, started with this config, a deamon process. This means that it
will manage and schedule a graph for clients. You would also want to configure a
core.name to give it a well known name.

```json
core.name = pipewire-0
```
The name of the PipeWire context. This will also be the name of the PipeWire socket clients
can connect to.


### Setting Sample Rates

PipeWire uses a global sample rate in the processing pipeline. All signals are converted to this sample rate and then converted to the sample rate of the device.

You can change the sample rate in `pipewire.conf`. Find, uncomment and change this line:
```json
default.clock.rate  =    48000
```
It is also possible to dynamically force a samplerate at runtime see [here](#runtime-settings).

Note that the default clock rate determines the duration of the min/max/default quantums below. You might want to change the quantums when you change the default clock rate to maintain the same duration for the quantums.

Since 0.3.33, it is possible to specify up to 16 (and 32 since 0.3.61) alternative sample rates. The graph sample rate will be switched when devices are idle. Use the following config option to specify the sample rates:

```json
default.clock.allowed-rates = [ 48000 44100 96000 ]
```

Note that this is not enabled by default for now because of kernel driver [bugs](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1547) that need to be fixed/worked around first. There are also potentially some bugs with [bluetooth devices](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1540) and [synchronization](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1633).

Note that when switching rates, the quantum sizes are scaled relative to the default clock rate.

### Setting Buffer Size (Quantum)

PipeWire will automatically select the lowest requested buffer size for the graph. The minimum of the requested buffer size is used. Limits can be configured in `/etc/pipewire/pipewire.conf` with the lines:

```json
default.clock.min-quantum =    32
default.clock.max-quantum =    8192
```
By setting these min and max values to the same value, you can force a buffer size. You can also dynamically change the ranges [here](#runtime-settings).

If no client specifies a latency, a default is used:
```json
default.clock.quantum  =       1024
```
The buffer sizes are relative to the `default.clock.rate` in the config file and will be scaled when an alternative samplerate is used.

Applications can specify a desired buffer size with the `node.latency` property.


Some applications (Ardour, guitarix, etc) have an option to change the buffer size with a menu option.

The `PIPEWIRE_LATENCY` environment variable can be used to configure the latency of an application:

```json
PIPEWIRE_LATENCY=256/48000 jack_simple_client
```
Will run this JACK client with a 256 samples buffer size.

Please check [PipeWire Buffering Explained](FAQ#pipewire-buffering-explained) for further explanation.

```json
default.clock.quantum-limit = 8192
default.clock.quantum-floor = 4
```
The min/max/default quantum values are scaled with the samplerate. The quantum-limit and
quantum-fllor values will give an unscaled upper bound and lower bound for the quantum.

This is interesting when working with high samplerates. For example, with the following setings:

```json
default.clock.rate = 48000
default.clock.allowed-rates = [ 24000 48000 96000 ]
default.clock.min-quantum = 32
default.clock.max-quantum = 8192
default.clock.quantum = 1024
default.clock.quantum-limit = 8192
default.clock.quantum-floor = 32
```

The min/max/default quantum are respectively 0.6ms (32/48000), 170.6ms (8192/48000)
and 21.3ms (1024/48000).

When the graph is using the 96000 samplerate, the quantum min/max/default values are scaled to
64, 16384 and 2048 to obtain the same quantum durations. The max quantum will however be clamped
to `default.clock.quantum-limit` (8192), limiting the max quantum to just 85ms. For even higher
frequencies, it might be good to increase the quantum-limit in order to have larger buffers
and avoid dropouts.

*Note that many JACK application assume that 8192 is the maximum quantum for any samplerate.*

Similarly, when the graph is using the 24000 samplerate, the quantum min/max/default values are
scaled to 16, 4096 and 512 to obtain the same quantum durations. The min quantum will however be
clamped to `default.clock.quantum-floor` (32), to avoid a too small buffer size.

```json
clock.power-of-two-quantum = true
```
The quantum requests from the clients and the final graph quantum are rounded down to a
power of two. A power of two quantum can be more efficient for many processing tasks such as
[FFT](https://en.wikipedia.org/wiki/Fast_Fourier_transform) or many
[SIMD](https://en.wikipedia.org/wiki/Single_instruction,_multiple_data) optimized functions.


### Settings Options

A PipeWire daemon will also expose a settings metadata object that can be used to
change some settings at runtime.

Normally these settings can bypass any of the restrictions listed in the config
options above, such as quantum and samplerate values.

With the following options, the settings metadata changes will be checked against
the allowed configured values:

```json
settings.check-quantum = false
settings.check-rate = false
```
To check if the quantum and rate values in the settings metadata update are compatible
with the configured limits.

### VM Overrides

When running in a VM, some quantum and samplerate settings might need to be changed.
Especially the quantum values might be too low to run without hiccups when running on
an emulated audio card.

```json
    vm.overrides = {
        default.clock.min-quantum = 1024
    }
```
Any property you place in the vm.overrides property object will override the property
in the context.properties when PipeWire detects it is running in a VM.

## SPA Libraries

Plugins are loaded based on their factory-name. This is a well known name that uniquely
describes the features that the plugin should have. The `context.spa-libs` section
provides a mapping between the factory-name and the plugin where the factory can
be found.

This way, it is possible to replace the implementation of these plugins with an
alternative implementation by changing the plugin name for the specific factory-name.

Factory names can contain a wildcard to group several related factories into one
plugin. The plugin is loaded from the first matching factory-name.

```json
context.spa-libs = {
    #<factory-name regex> = <library-name>
    #
    # Used to find spa factory names. It maps an spa factory name
    # regular expression to a library name that should contain
    # that factory.
    #
    audio.convert.* = audioconvert/libspa-audioconvert
    ...
}
```
By removing some factory names, it is also possible to block loading specific
factories.

## Loading Modules

As part of the configuration of a PipeWire server or client, a set of modules can be loaded.

Modules implement functionality or provide a service to other parts of the system.

Refer to [PipeWire Modules](https://docs.pipewire.org/page_pipewire_modules.html) for
more information.

## Making Objects

The `context.objects` section allows you to make some objects from factories (usually created
by loading modules in `context.modules`).

```json
context.objects = [
    #{ factory = <factory-name>
    #    ( args  = { <key> = <value> ... } )
    #    ( flags = [ ( nofail ) ] )
    #    ( condition = [ { <key> = <value> ... } ... ] )
    #}
```
This section can be used to make nodes or links between nodes. For example, this fragment
creates a new dummy driver node:

```json
context.objects = [
    { factory = spa-node-factory
        args = {
            factory.name    = support.node.driver
            node.name       = Dummy-Driver
            node.group      = pipewire.dummy
            priority.driver = 20000
        }
    }
]
```

## Executing Commands

The `context.exec` section can be used to start arbitrary commands as part of the initialization of
the PipeWire program.

```json
context.exec = [
    #{   path = <program-name>
    #    ( args = "<arguments>" )
    #    ( condition = [ { <key> = <value> ... } ... ] )
    #}
]
```

For example: the following fragment executes a pactl command with the given arguments:

```json
context.exec = [
    { path = "pactl" args = "load-module module-always-sink" }
]
```

## Split-File Configuration

As the configuration files also explain configuration options can be split into separate files. The numbers denote the order in which they are loaded, below are examples:

Changing Default Clock Rate:

Place a `10-clock-rate.conf` file in `~/.config/pipewire/pipewire.conf.d/` with the contents:

```json
context.properties = {
   default.clock.rate  =    44100
}
```
Because the `context.properties` is an object (properties), it will *override* the values of the
previous configuration.

Turning Default Upmixing Off for pulseaudio clients:

Place a `11-channelmix-upmix.conf` file in `~/.config/pipewire/pipewire-pulse.conf.d/` with the contents:

```json
stream.properties = {
    channelmix.upmix   =    false
}
```

Load an extra [Pipe Source](Virtual-Devices#pipe-devices) module in the PipeWire daemon:

Place a `20-virtual-pipe-source.conf` file in `~/.config/pipewire/pipewire.conf.d/` with the contents:

```json
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = source
          pipe.filename = "/tmp/virtualdevice"
          audio.format = S16LE
          audio.rate = 44100
          audio.channels = 2
          audio.position = [ FL FR ]
          stream.props = {
	      node.name = VirtualMic
          }
      }
  }
]
```
Because the `context.modules` is an array, it will be *appended* to the existing modules
from the previous configuration.

It is not yet possible to remove existing modules or to insert a moudule in the
array of modules. It is however possible to conditionally load modules with a condition
rule.

# Rules

Some config files can contain match rules. This makes it possible to perform some action when an
object (usually a node or stream) is created/updated that matches certain properties.

The general rules object follows the following pattern:
```json
<rules> = [
    {
        matches = [
            # any of the following sets of properties are matched, if
            # any matches, the actions are executed
            {
                # <key> = <value>
                # all keys must match the value. ! negates. ~ starts regex.
                #application.process.binary = "teams"
                #application.name = "~speech-dispatcher.*"
            }
            {
                # more matches here...
	    }
	    ...
        ]
        actions = {
            <action-name> = <action value>
	    ...
        }
    }
]
```
The rules is an array of things to match and what actions to perform when a match is found. 

The available actions and their values depend on the specific rule that is used. Usually it
is possible to update some properties or set some quirks on the object.

The PipeWire daemon config does not process any match rules (yet) but the various client config
files do. See [JACK](Config-JACK#application-specific-settings)
 matches for some examples.

# Setting Resample Quality

Resampling is performed in 2 places in PipeWire.

 - To convert the client sample rate to the DSP sample rate of the graph (See previous topic). This will do nothing when the client has the same sample rate as the graph. When the graph has multiple rates configured, PipeWire can switch the samplerate of the graph to match that of the client and avoid resampling.
 - To convert the DSP sample rate to a supported device sample rate. This does nothing when the device supports the same sample rate of the DSP graph.
 - When drivers with different clocks are joined in a graph, the adptive resampler will keep that two audio clocks in sync to avoid drifting.

PipeWire uses a custom highly optimized and reasonably accurate adaptive resampler. See [Infinite Wave](https://src.infinitewave.ca/) for a comparison. Changing the quality uses more CPU power with (arguably) little measurable advantages.

The resampler quality of device nodes can be changed in the WirePlumber config files for the devices,
see [here](https://pipewire.pages.freedesktop.org/wireplumber/configuration/alsa.html)

Set the resampler quality of a stream by changing the `resample.quality` property.
`pw-cat` has a `-q` option for this. The client config files (`client.conf`, `client-rt.conf`,
[`pipewire-pulse.conf`](Config-PulseAudio#stream-configuration)
) also have a `stream.properties` section where the resampler and other properties can be configured.

# Runtime Settings

Some runtime settings can be changed with the settings metadata. You can list the current settings with:

```
pw-metadata -n settings
```

## Log Settings

To temporarily increase the log level of the PipeWire daemon, use:

```
pw-metadata -n settings 0 log.level <level>
```


## `samplerate` Settings

The default samplerate can be set with:
```
pw-metadata -n settings 0 clock.rate <value>
```
The allowed samplerates can be set with:
```
pw-metadata -n settings 0 clock.allowed-rates [ <value1> <value2> ... ]
```

To temporarily force the graph to operate in a fixed sample-rate use:

```
pw-metadata -n settings 0 clock.force-rate <samplerate>
```

Both DSP processing and devices will switch to the new rate immediately. Running streams (PulseAudio, native and ALSA applications) will automatically resample to match the new rate.

Switch back to the default behaviour with:

```
pw-metadata -n settings 0 clock.force-rate 0
```

## Quantum Ranges

The default quantum can be set with:
```
pw-metadata -n settings 0 clock.quantum <value>
```
Change the quantum ranges with:
```
pw-metadata -n settings 0 clock.min-quantum <value>
pw-metadata -n settings 0 clock.max-quantum <value>
```
These values are expressed against the `clock.rate` value. If the graph runs at an alternative rate, `clock.quantum` and `clock.min-quantum` will be scaled.

To temporarily force the graph to operate in a fixed buffer-size use:

```
pw-metadata -n settings 0 clock.force-quantum <buffer-size>
```

Switch back to the default behaviour with:
```
pw-metadata -n settings 0 clock.force-quantum 0
```

# Environment variables

The following variables control the behaviour of the PipeWire daemon process:

## Socket directories

`PIPEWIRE_RUNTIME_DIR`, `XDG_RUNTIME_DIR` and `USERPROFILE` are used to find the PipeWire
socket on the server (and native clients).

`PIPEWIRE_CORE` is the name of the socket to make.

`PIPEWIRE_REMOTE` is the name of the socket to connect to.

`PIPEWIRE_DAEMON` is set to true then the process becomes a new PipeWire server.

## Config directories, config file name and prefix

`PIPEWIRE_CONFIG_DIR`, `XDG_CONFIG_HOME` and `HOME` are used to find the config file directories.

`PIPEWIRE_CONFIG_PREFIX` and `PIPEWIRE_CONFIG_NAME` are used to override the application provided
config prefix and config name.

`PIPEWIRE_NO_CONFIG` enables (false) or disables (true) overriding on the default configuration.

## Context information

As part of a client context, the following information is collected from environment variables
and placed in the context properties:

`LANG` the current language in `application.language`.

`XDG_SESSION_ID` is set as the `application.process.session-id` property.

`DISPLAY` is set as the `window.x11.display` property.

## Modules

`PIPEWIRE_MODULE_DIR` sets the directory where to find PipeWire modules.

`SPA_SUPPORT_LIB` the name of the SPA support lib to load. This can be used to switch to
an alternative support library, for example, to run on the EVL realtime kernel.

## Logging options

`JOURNAL_STREAM` is used to parse the stream used for the journal. This is usually configured by
systemd.

`PIPEWIRE_LOG_LINE` enables the logging of line numbers. Default true.

`PIPEWIRE_LOG` specifies a log file to use instead of the default logger.

`PIPEWIRE_LOG_SYSTEMD` enables the use of systemd for the logger, Default true.

## Other settings

`PIPEWIRE_CPU` selects the CPU and flags. This is a bitmask of any of the [CPU](https://docs.pipewire.org/cpu_8h_source.html) flags

`PIPEWIRE_VM` selects the Virtual Machine PipeWire is running on.  This can be any of the [VM](https://docs.pipewire.org/cpu_8h_source.html) types.

`DISABLE_RTKIT` disables the use of RTKit or the Realtime Portal for realtime scheduling.

`NO_COLOR` disables the use of colors in the console output.

## Debugging options

`PIPEWIRE_DLCLOSE` will enable (true) or disable (false) the use of dlclose when a shared library
is no longer in use. When debugging, it might make sense to disable dlclose to be able to get
debugging symbols from the object.

## Stream options

`PIPEWIRE_NODE` makes a stream connect to a specific `object.serial` or `node.name`.

`PIPEWIRE_PROPS` adds extra properties to a stream or filter.

`PIPEWIRE_QUANTUM` forces a specific rate and buffer-size for the stream or filter.

`PIPEWIRE_LATENCY` sets a specific latency for a stream or filter. This is only a suggestion but
                   the configured latency will not be larger.

`PIPEWIRE_RATE` sets a rate for a stream or filter. This is only a suggestion. The rate will be
                switched when the graph is idle.

`PIPEWIRE_AUTOCONNECT` overrides the default stream autoconnect settings.

## Plugin options

`SPA_PLUGIN_DIR` is used to locate SPA plugins.

`SPA_DATA_DIR` is used to locate plugin specific config files. This is used by the
bluetooth plugin currently to locate the quirks database.

`SPA_DEBUG` set the log level for SPA plugins. This is usually controlled by the `PIPEWIRE_DEBUG` variable
when the plugins are managed by PipeWire but some standalone tools (like spa-inspect) uses this
variable.

`ACP_BUILDDIR` if set, the ACP profiles are loaded from the builddir.

`ACP_PATHS_DIR` and `ACP_PROFILES_DIR` to locate the ACP paths and profile directories respectively.

`LADSPA_PATH` comma separated list of directories where the ladspa plugins can be found.

`LIBJACK_PATH` directory where the jack1 or jack2 libjack.so can be found.

