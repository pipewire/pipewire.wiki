[[_TOC_]]

# Tuning tips

See [here](Performance-tuning) for some performance tuning tips.

# Performance Measurements

Samples taken from client and server with:

    sudo perf stat -p <pid> --timeout 5000

 Performance counter stats for process id `24004`:

|   |   |   |   | 
|:--|:--|:--|--:|
| 27.17 msec | task-clock | # | 0.005 CPUs utilized |
| 468| context-switches  | # | 0.017 M/sec |
| 0 | cpu-migrations   | # | 0.000 K/sec |
| 0 | page-faults | # | 0.000 K/sec | 
| 20,664,509 | cycles | # | 0.760 GHz |
| 7,904,656 | instructions | # | 0.38  insn per cycle |
| 1,495,859 | branches | # | 55.051 M/sec |
| 182,369 | branch-misses | # | 12.19% of all branches

 Time elapsed: 5.000486557 seconds

CPU utilized, context-switches and instruction rows are listed below.

Hardware: Intel(R) Core(TM) i7-4770 CPU @ 3.40GHz

Test files:

- 44100_s16_2chan_file.wav: 16 bits uncompressed 44.1KHz stereo wav file
- 48000_s24_2chan_file.wav: 24 bits uncompressed 48KHz stereo wav file

# Playback 44.1KHz, Audio Device 44.1KHz

## PipeWire (0.3.81):

The following script gives the raw output:

```bash
for i in 8192 1024 512 128 64 32;
do
  pw-cat -p --latency=$i 44100_s16_2chan_file.wav &
  PID=$!
  sleep 1
  echo "$i/44100 server:"
  sudo perf stat -p `pidof pipewire-uninstalled` --timeout 5000
  echo "$i/44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  sleep 1
done
```
| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
| 8192 | 54/54 | 1.5M/1.5M | 0.000/0.000 |
| 1024 | 432/433 | 6.5M/6.7M | 0.003/0.003 |
| 512 | 862/862 | 11.7M/10.6M | 0.008/0.005 |
| 128 | 3450/3448 | 42.6M/40.3M | 0.028/0.025 |
| 64 | 6896/6898 | 83.0M/78.4M | 0.055/0.048 |

## PulseAudio (16):

```bash
for i in 2000 200 120 50 20 10 2;
do
  paplay --latency-ms=$i 44100_s16_2chan_file.wav &
  PID=$!
  sleep 1
  echo "Latency $i 44100 server:"
  sudo perf stat -p `pidof pulseaudio` --timeout 5000
  echo "latency $i 44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  sleep 1
done
```

The first column is an approximate latency comparable to the PipeWire period.
The last column is the requested end-to-end latency.

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU | Latency-ms |
|:--|--:|--:|--:|--:|
| 85000 | 44/11 | 8.5M/1.3M | 0.001/0.000 | 1000 |
| 8192 | 265/65 | 18.6M/3.1M | 0.004/0.001 | 200 |
| 4096 | 555/139 | 33.8M/6.2M | 0.008/0.002 | 120 |
| 2048 | 1472/301 | 78.7M/13.1M | 0.022/0.004 | 50 |
| 1024 | 4777/887 | 240.5M/38.5M | 0.064/0.011 | 20 |
| 512 | 9442/1776 | 469.4M/76.3M | 0.121/0.022 | 10 |
| 64 | 20030/5074 | 1031M/216.0M | 0.130/0.034 | 2 |

- Even with massive buffers, PulseAudio needs almost 10 times more instructions.
- With default comparable latencies (20ms/1024 period) PipeWire can to the work
  21 times more efficient.

## PulseAudio (PipeWire 0.3.81, in server):

For this test the protocol-pulse is loaded in the server context. This is not the
default setup where it is loaded in a separate pipewire-pulse process.

```bash
for i in 1000 200 120 50 20 10 2;
do
  paplay --latency-ms=$i 44100_s16_2chan_file.wav &
  PID=$!
  sleep 1
  echo "Latency $i 44100 server:"
  sudo perf stat -p `pidof pipewire-uninstalled` --timeout 5000
  echo "latency $i 44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  sleep 1
done
```

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU | Latency-ms |
|:--|--:|--:|--:|--:|
| 85000 | 81/27 | 4.4M/1.3M | 0.001/0.000 | 1000 |
| 8192 | 324/109 | 8.8M/5.0M | 0.003/0.002 | 200 |
| 4096 | 645/217 | 16.4M/9.9M | 0.006/0.003 | 120 |
| 2048 | 1077/216 | 24.0M/9.9M | 0.010/0.003 | 50 |
| 1024 | 4311/864 | 85.5M/38.9M | 0.040/0.011 | 20 |
| 512 | 8630/1728 | 168.5M/76.9M | 0.077/0.023 | 10 |
| 64 | 8624/1724 | 167.1M/76.9M | 0.073/0.028 | 2 |

The pipewire server implementation is about 5 times more efficient for the same latency.

# Playback 48KHz, Audio Device 44.1KHz

## PipeWire:

    pw-cat -p --latency=<period*2> 48000_s24_2chan_file.wav

Quality 4 (64 resample taps):

| Period  | C-Switch Server/Client | Instructions Server/Client | Quality | CPU |
|:--|--:|--:|--:|--:|
| 8192 | 54/42 | 1.8M/38.8M | 4 | 0,001/0,004 |
| 1024 | 430/231 | 7.0M/41.2M | 4 | 0.003/0.005 |
| 512 | 860/445 | 12.4M/43.3M | 4 | 0.006/0.006 |
| 128 | 3445/1747 | 45.0M/60.7M | 4 | 0.021/0.019 |
| 64 | 6888/3462 | 88.0M/82.3M | 4 | 0.035/0.033 |
| 32 | 13784/6907 | 171.5M/125.6M | 4 | 0.070/0.050 |

Quality 1 (32 resample taps):

| Period  | C-Switch Server/Client | Instructions Server/Client | Quality | CPU |
|:--|--:|--:|--:|--:|
| 8192 | 54/42 | 1.8M/30.7M | 1 | 0.001/0.003 |

Quality 0 (24 resample taps):

| Period  | C-Switch Server/Client | Instructions Server/Client | Quality | CPU |
|:--|--:|--:|--:|--:|
| 32 | 13784/6907 | 171.5M/117.4M | 0 | 0.070/0.047 |

 - PipeWire server times are similar to the 44100Hz case above. All extra work is
done in the client resampler.

## PulseAudio:

*No measurements yet.*

PulseAudio reconfigures the device to 48KHz and avoids resampling. This can be
disabled. Tests will follow.

# Mixing 44.1KHz and 48KHz Streams

## PipeWire:

    (c1) pw-cat -p --latency=<period> 44100_s16_2chan_file.wav
    (c2) pw-cat -p --latency=<period*2> 48000_s24_2chan_file.wav

| Period  | C-Switch Server/c1/c2 | Instructions Server/c1/c2 | Quality | CPU |
|:--|--:|--:|--:|--:|
| 8192 | 54/42/41 | 2.4M/1.6M/38.8M | 4 | 0,001/0,001/0,003 |
| 1024 | 430/229/244 | 8.4M/3.5M/41.6M | 4 | 0.004/0.002/0.005 |
| 512 | 860/444/458 | 15.1M/5.7M/44.2M | 4 | 0.007/0.003/0.008 |
| 128 | 3446/1736/1754 | 53.7M/21.7M/60.5M | 4 | 0.027/0.013/0.018 |
| 32 | 13785/6909/6906 | 203.0M/82.8M/120.3M | 4 | 0.069/0.040/0.048 |

## PulseAudio:

    paplay --latency-msec=<latency-ms> 48000_s24_2chan_file.wav
    
    paplay 44100_s16_2chan_file.wav

We only list the server numbers, the client numbers are like for a single client.

| Period  | C-Switch Server | Instructions Server | CPU | Latency-ms |
|:--|--:|--:|--:|--:|
| 85000 | 143 | 208.5M | 0.015 | def |
| 8192 | 434 | 198.1M | 0.025 | 200 |
| 4096 | 1142 | 225.2M | 0.044 | 120 |
| 2048 | 2383 | 271.2M | 0.078 | 50 |
| 1024 | 5238 | 382.9M | 0.151 | 20 |
| 512 | 9892 | 575.2M | 0.204 | 10 |
| 64 | 17299 | 865.6M | 0.271 | 2 |

# JACK Clients

## PipeWire (0.3.81)

This runs on the USB card (Behringer UMC404HD) in Pro-Audio mode with both capture
and playback using IRQ mode scheduling.

```bash
for i in 8192 1024 512 128 64 32
do
  PIPEWIRE_QUANTUM=$i/44100 jack_simple_client &
  PID=$!
  sleep 1
  echo "$i/44100 server:"
  sudo perf stat -p `pidof pipewire-uninstalled` --timeout 5000
  echo "$i/44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  sleep 1
done
```

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
|8192 | 54/32 | 2.6M/4.4M | 0.001/0.000 |
|1024 | 432/221 | 7.3M/6.5M | 0.003/0.001 |
|512 | 862/436 | 13.3M/8.0M | 0.006/0.002 |
|128 | 3448/1729 | 45.3M/19.6M | 0.022/0.009 |
|64 | 6896/3454 | 88.1M/35.0M | 0.018/0.009 |
|32 | 13792/6901 | 174.8M/66.2M | 0.032/0.018 |

The script for multiple JACK clients:

```bash
for c in 2 4 8 16 32;
do
  for i in 8192 4096 1024 512 128 32;
  do
    pids=()
    for j in $(seq 1 $c); do
      PIPEWIRE_QUANTUM=$i/44100 jack_simple_client &
      pids+=($!)
    done
    sleep 1
    echo "$i/44100 server:"
    sudo perf stat -p `pidof pipewire-uninstalled` --timeout 5000

    for j in $(seq 1 $c); do
      kill -9 ${pids[$j-1]}
    done
    sleep 1
  done
done
```

2 JACK clients (server numbers):

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 54 | 3.1M | 0.001 |
| 4096 | 108 | 3.9M | 0.001 |
| 1024 | 432 | 8.4M | 0.004 |
| 512 | 862 | 15.0M | 0.007 |
| 128 | 3448 | 49.3M | 0.023 |
| 32 | 13790 | 195.3M | 0.051 |

4 JACK clients:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 54 | 3.4M | 0.001 |
| 4096 | 106 | 4.2M | 0.001 |
| 1024 | 432 | 9.9M | 0.005 |
| 512 | 862 | 17.6M | 0.009 |
| 128 | 3448 | 60.1M | 0.021 |
| 32 | 13791 | 228.6M | 0.056 |

8 JACK clients:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 54 | 4.2M | 0.001 |
| 4096 | 108 | 5.3M | 0.002 |
| 1024 | 430 | 12.0M | 0.006 |
| 512 | 862 | 22.3M | 0.010 |
| 128 | 3448 | 75.6M | 0.025 | 
| 32 | 13792 | 276.4M | 0.064 |

16 JACK clients:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 54 | 6.4M | 0.001 |
| 4096 | 108 | 8.1M | 0.002 |
| 1024 | 432 | 21.3M | 0.007 |
| 512 | 862 | 37.3M | 0.014 |
| 128 | 3450 | 122.2M | 0.037 |
| 32 | 13792 | 460.4M | 0.111 |

32 JACK clients:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 54 | 10.9M | 0.002 |
| 4096 | 108 | 16.0M | 0.003 |
| 1024 | 432 | 41.1M | 0.010 |
| 512 | 862 | 75.2M | 0.020 |
| 128 | 3448 | 261.9M | 0.073 |
| 32 | 13791 | 862.4M | 0.172 |

## JACK (1.9.22)

```bash
for i in 8192 1024 512 128 64 32
do
  jackd -R -d alsa -d hw:4 -C -P -r 44100 -p $i &
  JACK_PID=$!
  sleep 2
  jack_simple_client &
  PID=$!
  sleep 1
  echo "$i/44100 server:"
  sudo perf stat -p $JACK_PID --timeout 5000
  echo "$i/44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  kill -9 $JACK_PID
  sleep 1
done
```

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
| 8192 | 66/32 | 16.5M/4.4M | 0.001/0.000 |
| 1024 | 255/221 | 20.7M/6.0M | 0.003/0.001 |
| 512 | 471/436 | 25.3M/7.8M | 0.005/0.002 |
| 128 | 1764/1730 | 52.7M/19.5M | 0.017/0.007 |
| 64 | 3489/3454 | 89.1M/32.9M | 0.019/0.011 |
| 32 | 6937/6900 | 162.3M/61.7M | 0.027/0.014 |

JACK in async mode only collects the client samples in the next iteration.
This results in an extra period of latency but also avoid a context switch
for each period (and associated CPU instructions).

PipeWire is much faster for larger buffer sizes and comparable for smaller
sizes (although it needs to do twice as many context switches).

```bash
for i in 8192 1024 512 128 64 32
do
  jackd -S -R -d alsa -d hw:4 -C -P -r 44100 -p $i &
  JACK_PID=$!
  sleep 2
  jack_simple_client &
  PID=$!
  sleep 1
  echo "$i/44100 server:"
  sudo perf stat -p $JACK_PID --timeout 5000
  echo "$i/44100 client:"
  sudo perf stat -p $PID --timeout 5000
  kill -9 $PID
  kill -9 $JACK_PID
  sleep 1
done
```

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
| 8192 | 94/32 | 17.1M/4.5M | 0.001/0.000 |
| 1024 | 470/220 | 21.7M/6.6M | 0.003/0.001 |
| 512 | 902/436 | 28.3M/8.9M | 0.006/0.002 |
| 128 | 3488/1729 | 61.0M/23.3M | 0.021/0.008 |
| 64 | 6936/3453 | 105.7M/42.3M | 0.039/0.015 |
| 32 | 13847/6902 | 197.7M/80.2M | 0.026/0.011 |

PipeWire use less instructions. For bigger buffer sizes, PipeWire is 
up to 6 times as efficient. 

One theory is that for larger buffer size, most time is spent in
sample conversion from/to floating point and the device sample format, which is
considerably less optimized in JACK.

For smaller buffer sizes, the buffer management overhead starts to dominate,
which is slower and heavier in PipeWire (but still fast enough to outperform
JACK).


### Multiple clients

For multiple clients we use this script:

```bash
for c in 2 4 8 16 32;
do
  for i in 8192 1024 512 128 32;
  do
    pids=()
    jackd -S -R -d alsa -d hw:4 -C -P -r 44100 -p $i &
    JACK_PID=$!
    sleep 2
    for j in $(seq 1 $c); do
      jack_simple_client &
      pids+=($!)
    done
    sleep 4
    echo "$i/44100 server:"
    sudo perf stat -p $JACK_PID --timeout 5000

    for j in $(seq 1 $c); do
      kill -9 ${pids[$j-1]}
    done
    kill -9 $JACK_PID
    sleep 1
  done
done
```

2 `jack_simple_client`:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 94 | 18.3M | 0.001 |
| 1024 | 472 | 23.7M | 0.004 |
| 512 | 902 | 29.7M | 0.007 |
| 128 | 3489 | 66.8M | 0.011 |
| 32 | 13834 | 217.2M | 0.051 |


4 `jack_simple_client`:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 94 | 20.0M | 0.001 |
| 1024 | 471 | 26.1M | 0.005 |
| 512 | 903 | 33.1M | 0.009 |
| 128 | 3489 | 76.7M | 0.030 |
| 32 | 13836 | 245.4M | 0.055 |

8 `jack_simple_client`:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 95 | 23.6M | 0.002 |
| 1024 | 472 | 33.2M | 0.006 |
| 512 | 903 | 42.2M | 0.011 |
| 128 | 3491 | 105.6M | 0.039 |
| 32 | 13832 | 322.4M | 0.052 |

16 `jack_simple_client`:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 94 | 30.5M | 0.002 |
| 1024 | 472 | 46.6M | 0.007 |
| 512 | 903 | 61.0M | 0.012 |
| 128 | 3489 | 157.1M | 0.047 |
| 32 | 13836 | 502.2M | 0.110 |

32 `jack_simple_client`:

| Period  | C-Switch | Instructions | CPU |
|:--|--:|--:|--:|
| 8192 | 94 | 30.5M | 0.002 |
| 1024 | 474 | 73.6M | 0.010 |
| 512 | 904 | 104.3M | 0.018 |
| 128 | 3494 | 264.5M | 0.044 |
| 32 | 13843 | 893.2M | 0.156 |

PipeWire requires less instructions to do the same work. For the larger buffers,
it requires almost half the instructions.

# MIDI

Using `jack_midi_latency_test` with a loopback cable on a U-PHORIA UMC404HD (USB):

## PipeWire (1.0.1)

````
>> jack_midi_latency_test
Waiting for connections ...
Waiting for test completion ...

Reported out-port latency: 21.33-21.33 ms (1024-1024 frames)
Reported in-port latency: 21.33-21.33 ms (1024-1024 frames)
Average latency: 22.51 ms (1080.63 frames)
Lowest latency: 22.42 ms (1076 frames)
Highest latency: 22.60 ms (1085 frames)
Peak MIDI jitter: 0.19 ms (9 frames)
Average MIDI jitter: 0.04 ms (1.47 frames)

Jitter Plot:
0.0 - 0.1 ms: 1024

Latency Plot:
22.4 - 22.5 ms: 363
22.5 - 22.6 ms: 645
22.6 - 22.7 ms: 16

Messages sent: 1024
Messages received: 1024
````

## JACK2 (1.9.17) seq backend

    jackd -S -d alsa -P -X seq -d hw:0 -r 48000 -p 1024

````
jack_midi_latency_test
Waiting for connections ...
Waiting for test completion ...

Reported out-port latency: 0.00-0.00 ms (0-0 frames)
Reported in-port latency: 0.00-0.00 ms (0-0 frames)
Average latency: 43.88 ms (2105.94 frames)
Lowest latency: 43.78 ms (2101 frames)
Highest latency: 44.01 ms (2112 frames)
Peak MIDI jitter: 0.23 ms (11 frames)
Average MIDI jitter: 0.04 ms (1.51 frames)

Jitter Plot:
0.0 - 0.1 ms: 1005
0.1 - 0.2 ms: 19

Latency Plot:
43.7 - 43.8 ms: 60
43.8 - 43.9 ms: 542
43.9 - 44.0 ms: 421
44.0 - 44.1 ms: 1

Messages sent: 1024
Messages received: 1024
````

## JACK2 (1.9.17) raw backend

    jackd -S -d alsa -P -X raw -d hw:0 -r 48000 -p 1024

````
Waiting for connections ...
Waiting for test completion ...

Reported out-port latency: 0.00-0.00 ms (0-0 frames)
Reported in-port latency: 0.00-0.00 ms (0-0 frames)
Average latency: 43.90 ms (2107.01 frames)
Lowest latency: 43.78 ms (2101 frames)
Highest latency: 45.09 ms (2164 frames)
Peak MIDI jitter: 1.31 ms (63 frames)
Average MIDI jitter: 0.04 ms (1.40 frames)

Jitter Plot:
0.0 - 0.1 ms: 978
0.1 - 0.2 ms: 45
1.1 - 1.2 ms: 1

Latency Plot:
43.7 - 43.8 ms: 23
43.8 - 43.9 ms: 370
43.9 - 44.0 ms: 610
44.0 - 44.1 ms: 20
45.0 - 45.1 ms: 1

Messages sent: 1024
Messages received: 1024
````

The RAW midi backend is not really better than the seq backend in JACK2 and consumes more than twice as much CPU due to busy waiting (7.7M vs 19.5M instructions).

PipeWire consumes 7.4M instructions for this latency test.

# Latency

Latencies on PCI devices are slightly better than JACK2 latency measurements.

Latency on USB devices is more difficult to control. The reason is that USB devices are usually labeled as batch devices that update the read/write pointers only on each IRQ update. This adds one period-size of latency for USB devices. PipeWire will automatically lower the period-size to half the quantum. This only works when the device is opened when the graph has a low quantum, at runtime, it can not lower the period-size and decrease latency.

Since 0.3.81, devices in Pro-Audio mode (with 1 capture and 1 playback pcm) are linked and scheduled using IRQ based wakeups. This
gives the same latency as JACK.

All latencies are measured with jack_iodelay and a loopback cable. Listed are total roundtrip latencies as reported by jack_iodelay. Samplerate of the device is set to 48KHz.

## U-Phoria UMC404HD USB card 

An external USB device. Has a loopback cable on port 3.

Slight variations in end-to-end latency can occur depending on how the packets are scheduled in the kernel.

### PipeWire 0.3.81 (defaults)

```bash
for i in 8192 1024 512 128 32 16
do
  PIPEWIRE_QUANTUM=$i/48000 jack_iodelay &
  PID=$!
  sleep 1
  jack_connect jack_delay:out "UMC404HD 192k Pro:playback_AUX2"
  jack_connect "UMC404HD 192k Pro:capture_AUX2" jack_delay:in
  sleep 5
  kill -9 $PID
  sleep 1
done
```

|   |   |   |
|--:|--:|--:|
| 8192 | 355.014ms | |
| 1024 | 56.702ms | |
| 512 | 35.514ms | |
| 128 | 19.639ms | |
| 32 | 6.514ms | |
| 16 | 4.473ms |  |

### JACK 1.9.22

```bash
for i in 8192 1024 512 128 32 16
do
  jackd -S -R -d alsa -d hw:4 -C -P -r 48000 -p $i &
  JACK_PID=$!
  sleep 2
  jack_iodelay &
  PID=$!
  sleep 1
  jack_connect jack_delay:out system:playback_3
  jack_connect system:capture_3 jack_delay:in
  sleep 5
  kill -9 $PID
  kill -9 $JACK_PID
  sleep 1
done
```

|   |   |   |
|--:|--:|--:|
| 8192 | 355.514ms | |
| 1024 | 56.848ms | |
| 512 | 35.493ms | |
| 128 | 19.389ms | |
| 32 | 6.639ms | |
| 16 | 4.473ms |  |

## Lenovo Yoga Laptop

This laptop has an Intel(R) Core(TM) i7-4500U CPU @ 1.80GHz and a PCI HDA Intel card.

### PipeWire 0.3.31

|   |   |   |
|--:|--:|--:|
| 4096 | 174.976ms | |
| 1024 | 45.205ms | |
| 512 | 33.872ms | |
| 128 | 8.184ms | |
| 64 | 5.205ms | |
| 32 | 4.101ms | |
| 16 | --- | xrun |

### JACK 1.9.17

|   |   |   |
|--:|--:|--:|
| 4096 | 171.872 | |
| 1024 | 43.871 | |
| 512 | 22.538 | |
| 128 | 6.517 | |
| 64 | -- | xrun |
| 32 | -- | xrun |

# Device Combine

Test is to play `jack_simple_client` on two devices (hw:0 and hw:1).

## PipeWire

Catia is used to also link `jack_simple_client` to the second device.

    sudo perf stat -p `pidof pipewire` --timeout 5000

Performance counter stats for process id `22617`:

|   |   |   |   | 
|:--|:--|:--|--:|
| 52,34 msec | task-clock | # | 0,010 CPUs utilized |          
| 468 | context-switches | # | 0,009 M/sec |                  
| 0 | cpu-migrations | # | 0,000 K/sec |                 
| 0 | page-faults | # | 0,000 K/sec |                  
| 57.086.173 | cycles | # | 1,091 GHz |                   
| 57.083.754 | instructions | # | 1,00 insn per cycle |         
| 6.271.377 | branches | # | 119,827 M/sec |                 
| 231.016 | branch-misses | # | 3,68% of all branches |

Time elapsed: 5,000616640 seconds

## JACK2

    jackd -S -d alsa -d hw:0 -r 48000 -p 1024

    zita-j2a -d hw:1

Catia to link `jack_simple_client` to zita-j2a

    sudo perf stat -p `pidof zita-j2a`,`pidof jackd` --timeout 5000

Performance counter stats for process id `23824, 23813`:

|   |   |   |   | 
|:--|:--|:--|--:|
|174,35 msec | task-clock | # | 0,035 CPUs utilized |          
| 1.897 | context-switches | # | 0,011 M/sec |                  
| 1 | cpu-migrations | # | 0,006 K/sec |                 
| 0 | page-faults | # | 0,000 K/sec |                  
| 158.909.277 | cycles | # | 0,911 GHz |                    
| 389.039.212 | instructions | # | 2,45 insn per cycle |         
| 35.765.404  | branches | # | 205,142 M/sec |                 
| 399.113 | branch-misses | # | 1,12% of all branches | 

Time elapsed: 5,001364381 seconds

# Raspberry Pi 3

Playback of 44100Hz wav file, server sample rate 44100.

## PipeWire

    pw-cat -p --latency=<period> 44100.wav

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
| 8192 | 54/41 | 2.3M/2.7M | 0.002/0.003 |
| 1024 | 430/232 | 10.1M/6.9M | 0.008/0.006 |
| 512 | 861/447 | 19.0M/9.8M | 0.014/0.008 |
| 256 | 1716/893 | 40.3M/17.8M | 0.030/0.013 |

## PulseAudio

    paplay --latency-msec=<msec> 44100.wav

| Period  | C-Switch Server/Client | Instructions Server/Client | CPU |
|:--|--:|--:|--:|
| max | 8/2 | 1.0M/0.45M | 0.001/0.001 |
| 8192 | 404/104 | 17.6M/4.1M | 0.015/0.004 |
| 1024 | 3571/842 | 143.0M/27.9M | 0.107/0.025 |
| 512 | not stable gets increased to 30ms | not stable gets increased to 30ms| not stable gets increased to 30ms |

PulseAudio does really well here with large buffers. The client only gets woken up twice in the five seconds that the test runs, as it should with a greater than (>) two second buffer.

# Object Create

The following python script is executed:

```python
#!/usr/bin/python

from jacktools.jackgainctl import *
from time import *

NCLIENT = 16
NPORT = 32

t1 = time ()
JGC = [ JackGainctl (NPORT, 'JGC%d' % (i,)) for i in range (NCLIENT) ]
t2 = time ()
for i in range (NCLIENT-1):
    srce = JGC [i]
    dest = 'JGC%d' % (i + 1,)
    for j in range (NPORT):
        srce.connect_output (j, dest + ':in_%d' % (j,))
t3 = time ()

print ("%10.3f" % (t2 - t1))
print ("%10.3f" % (t3 - t2))
sleep (1000)
```

This requires the [zita-jacktools](http://kokkinizita.linuxaudio.org/linuxaudio/downloads/index.html)

It creates 16 clients with 64 ports each (for a total of 1024 ports) and 480 connections.

## jackd 1.9.17

```
     0.682
     0.041
```

## PipeWire 0.3.32

```
     0.379
     0.167
```
