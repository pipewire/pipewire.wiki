[[_TOC_]]

# Network Support

PipeWire has support for some Network protocols.

The best protocol to use depends on the latency requirements, interoperability requirements and ease of use/setup.

module-rtp-session is probably the easiest to setup and run if you want multiple (PipeWire) machines to communicate audio and midi. There are also pipewire-pulse modules available compatible with the PulseAudio modules for the audio communication.

module-netjack2 is probably the best choice for low-latency synchronized audio between machines without resampling.

module-pulse-tunnel is probably the best over unreliable WiFi networks.

module-snapcast-discover is also fairly easy if you already have a snapcast server setup.

| Module | Description | Protocol | Audio | Midi | Video | Latency | resample |
|--------|-------------|----------|-------|------|-------|---------|----------|
| module-netjack2-manager | Netjack Manager | nj2/UDP | Yes | Yes | No | low | No |
| module-netjack2-driver | Netjack Driver | nj2/UDP | Yes | Yes | No | low | No |
| module-rtp-sink | RTP sender | RTP/UDP/SAP | Yes | Yes | No | low/medium | No |
| module-rtp-source | RTP receiver | RTP/UDP/SAP | Yes | Yes | No | low/medium | Yes (when not using common clock) |
| module-rtp-session | RTP duplex | RTP/UDP/SAP | Yes | Yes | No | low/medium | Yes (receiver, when not using common clock) |
| module-roc-sink | ROC sender | RTP/UDP | Yes | No | No | low/medium | No |
| module-roc-source | ROC receiver | RTP/UDP | Yes | No | No | low/medium | Yes |
| module-raop-sink | Airplay | RTP/UDP | Yes | No | No | low/medium | No |
| module-pulse-tunnel | PulseAudio | pulse/TCP | Yes | No | No | medium/high | Yes |
| module-protocol-simple | raw audio | raw/TCP | Yes | No | No | medium/high | No |
| module-snapcast-discover | snapcast sender | snapcast/TCP | Yes | No | No | media/high | No |
| module-vban-send | vb-audio.com sender | vban/UDP | Yes | Yes | No | low | No |
| module-vban-recv | vb-audio.com receiver | vban/UDP | Yes | Yes | No | low | Yes |

Some of the JACK tools can also be used to connect machines together.

| Application | Description | Protocol | Audio | Video | Latency |
|-------------|-------------|----------|-------|-------|---------|
| zita-njbridge | [zita-njbridge](https://kokkinizita.linuxaudio.org/linuxaudio/) | UDP | Yes | No | low |
| jacktrip | [Jacktrip](https://www.jacktrip.org/) | UDP | Yes | No | low |
| jamulus | [Jamulus](https://jamulus.io/) | UDP | Yes | No | low |

## Netjack2

PipeWire has a native Netjack2 manager and driver. It is compatible with netjack2 as found in JACK2.

A netjack2 manager listens on a multicast address for netjack2 drivers. It Sends and receives audio/midi from the remote netjack2 driver with a fixed latency, that is a multiple of the buffer-size.

A netjack2 driver acts like a driver in the JACK/PipeWire graph and is driven by a remote netjack2 manager. This makes the driver follow the rate of the manager it is scheduled by.

### netjack2 manager

Start the manager with (`[ ... ]` contain optional parameters):

```
pw-cli -m lm libpipewire-module-netjack2-manager [ '{ net.ip=<ip-address> net.port=<port> }' ]
```

Or add the config section to a .conf file, see [module-netjack2-manager](https://docs.pipewire.org/page_module_netjack2_manager.html).

Or on JACK2:

```
jack_load netmanager [ -i"-a <ip-address> -p <port>" ] 
```

### netjack2 driver

Start the driver with (`[ ... ]` contain optional parameters):

```
pw-cli -m lm libpipewire-module-netjack2-driver [ '{ net.ip=<ip-address> net.port=<port> }' ]
```

Or add the config section to a .conf file, see [module-netjack2-driver](https://docs.pipewire.org/page_module_netjack2_driver.html).

Or start JACK2 with the netjack2 driver:

```
jackd -d net [ -a <ip-address> -p <port> ]
```

You should see a new client/node in the PipeWire/JACK instance running the manager and you can connect the ports to stream/receive data to/from the driver/manager.

## RTP

PipeWire has a native RTP source and sink. It uses SAP/SDP to announce RTP streams over multicast.

RTP supports various raw audio, midi and OPUS encoded formats.

You can load a rtp-source and the will automatically receive the data from the machines with a rtp-sink module loaded.

More information can be found in the [module-rtp-sink](https://docs.pipewire.org/page_module_rtp_sink.html) and [module-rtp-source](https://docs.pipewire.org/page_module_rtp_source.html) documentation.

There is also a simple guide to set this up [here](Guide-Network-RTP#point-to-point-rtp).

## RTP session

PipeWire has a native RTP session module. It uses avahi to announce RTP sessions.

It uses the [Apple MIDI](https://developer.apple.com/library/archive/documentation/Audio/Conceptual/MIDINetworkDriverProtocol/MIDI/MIDI.html) specification to establish a bidirectional session between two machines. The PipeWire implementation does not only support Apple MIDI but also uncompressed and compressed (OPUS) audio.

More information can be found in the [module-rtp-session](https://docs.pipewire.org/page_module_rtp_session.html).

## ROC

[ROC](https://roc-streaming.org/) is a streaming protocol for audio.

PipeWire has a native source and sink. More information can be found in the [module-roc-sink](https://docs.pipewire.org/page_module_roc_sink.html) and [module-roc-source](https://docs.pipewire.org/page_module_roc_source.html) documentation.

## RAOP

RAOP is the Apple Airplay protocol that implements Audio streaming over RTP/UDP and RTSP.

PipeWire has a native sink. More information can be found in the [module-raop-sink](https://docs.pipewire.org/page_module_raop_sink.html) documentation.

There is also a discover module that automatically creates the roap-sink modules with the right settings. See [module-raop-discover](https://docs.pipewire.org/page_module_raop_discover.html) for more information.

## Pulse Tunnel

The pulse tunnel module can create a sink or source to tunnel audio to a remote PulseAudio server. More information can be found in the [module-pulse-tunnel](https://docs.pipewire.org/page_module_pulse_tunnel.html) documentation.

The tunnel can also be automatically configured with the zeroconf module. See the [module-zeroconf-discover](https://docs.pipewire.org/page_module_zeroconf_discover.html) documentation.

There is also a simple guide to set this up [here](Guide-PulseAudio-Tricks#setup-tcp-network-streaming-between-machines).

## Protocol Simple

The simple protocol sends bidirectional audio over a TCP socket. It is meant to be used with the simple_protocol_player app on Android but you can also use it if you want to handle raw audio over the network.

More information can be found in the [module-protocol-simple](https://docs.pipewire.org/page_module_protocol_simple.html) documentation.

## Snapcast discover

This module detects snapcast servers and makes a sink for each server and sets up a new stream on the snapcast server.

It uses the simple protocol under the hood for this.

More information can be found in the [module-snapcast-discover](https://docs.pipewire.org/page_module_snapcast_discover.html) documentation.

## VBAN

The [VBAN](vb-audio.com) protocol is a simple UDP based protocol for sending audio and midi. It can be used to interoperate with Windows and MAC applications.

More information can be found in the [module-vban-send](https://docs.pipewire.org/page_module_vban_send.html) and [module-vban-recv](https://docs.pipewire.org/page_module_vban_recv.html) documentation.

## AES67

[AES67](https://en.wikipedia.org/wiki/AES67) support is possible with:

- Ensuring the sender and receiver have synchronized clocks using PTP. This can be done with a `/dev/ptp0` device or by synchronizing the system time (CLOCK_TAI) to ptp time.
- Adding a driver to the PipeWire graph that uses the synchronized time (`/dev/ptp0` or CLOCK_TAI).
- Configuring module-rtp-sink and module-rtp-source to send and receive AES67 packets and use the synchronized driver. You can use pre-built `pipewire-aes67` binary for this, but most likely need to adjust its preferences.

### Synchronizing with PTP

Check out the documentation for [ptp4l](https://linuxptp.nwtime.org/documentation/) to configure this.

### Adding a PTP driver

Add this object to the context:

```
context.modules = [
    {  factory = spa-node-factory
        args = {
            factory.name    = support.node.driver
            node.name       = PTP-Driver
            node.group      = pipewire.ptp0
            priority.driver = 30000
            clock.name      = "clock.system.tai"
            clock.id        = tai # monotonic | realtime | tai | monotonic-raw | boottime
            #clock.device   = "/dev/ptp0"
        }
    }
]
```

This will add a new driver named `pipewire.ptp0` that follows the TAI clock. You need to set up ptp4l or alternative (statime, ptpd) to synchronize TAI with PTP for this to work.

Alternatively, you can set the clock.device property to directly read the `/dev/ptp0` clock time (and remove the clock.id property). This method is preferred when you have a network card with hardware PTP since hardware clocks appear to be the most precise.

### Making a sender

Add this object to the context:

```
context.modules = [
{   name = libpipewire-module-rtp-sink
    args = {
        sap.ip = 239.255.255.255
        sap.port = 9875
        #source.ip = "0.0.0.0"
        destination.ip = "233.254.0.1"
        destination.port = 46000
        local.ifname = "eth0"
        #net.mtu = 1280
        #net.ttl = 1
        #net.loop = false
        #sess.min-ptime = 2
        #sess.max-ptime = 20
        sess.max-ptime = 2
        sess.name = "PipeWire AES67 Sender"
        sess.ts-refclk = "ptp=IEEE1588-2008:39-A7-94-FF-FE-07-CB-D0:0"
        #audio.format = "S16BE"
        #audio.rate = 48000
        #audio.channels = 2
        #audio.position = [ FL FR ]
        stream.props = {
            media.class = "Audio/Sink"
            node.name = "rtp-sink"
            node.group = pipewire.ptp0
            node.virtual = false
            device.api = aes67
        }
    }
}
]
```

Pass a valid ts-refclk identifier, see [here](https://www.rfc-editor.org/rfc/rfc7273.html#section-4.8).

### Making a receiver

```
{   name = libpipewire-module-rtp-source
    args = {
        sap.ip = 239.255.255.255
        sap.port = 9875
        local.ifname = eth0
        node.always-process = true
        stream.props = {
            media.class = "Audio/Source"
            node.name = "rtp-source"
            node.group = pipewire.ptp0
            node.virtual = false
            device.api = aes67
        }
        stream.rules = [
            {   matches = [
                    # any of the items in matches needs to match, if one does,
                    # actions are emited.
                    {   # all keys must match the value. ! negates. ~ in value starts regex.
                        #rtp.origin = "wim 3883629975 0 IN IP4 0.0.0.0"
                        #rtp.payload = "127"
                        #rtp.fmt = "L16/48000/2"
                        #rtp.session = "PipeWire RTP Stream on fedora"
                        rtp.ts-refclk = "ptp=IEEE1588-2008:39-A7-94-FF-FE-07-CB-D0:0"
                    }
                ]
                actions = {
                    create-stream = {
                        sess.latency.msec = 5
                        sess.ts-direct = true
                    }
                }
            }
        ]
    }
}
```

You can make a new stream when it receives a SAP message that matches the refclk.

`sess.ts-direct` will directly use the RTP timestamps to sync against the local PTP clock to achieve the lowest possible latency and avoid resampling at the receiver.