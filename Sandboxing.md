# Some Ideas For Implementing Sandboxing

1.) The PipeWire client load the portal module which hooks into the pw_remote_connect() method

2.) When a client does a pw_remote_connect() the portal module intercepts the call and does a DBus call to the portal. The client needs to give hints about what it wants to do in the properties.

3.) The portal gets the client request and opens a PipeWire connection.

4.) The portal configures the session, it can list the devices and present those in a user interface. The user grants access to selected devices.

5.) The portal can choose to hide objects from the session as well.

6.) The portal returns the PipeWire connection socket to the client.

7.) The client continues the PipeWire session on the socket.

**Some Advantages:**

- We don't expose the PipeWire socket to the sandbox, minimizing the attack surface.
- The portal has all information and can present a rich GUI to the client.
- The portal can hold on to the socket and revoke access to objects or the session.

**Some Questions:**

- What do we do with new objects that appear? do we configure a default access mode for them (view/hide) or do we let the Portal configure access?