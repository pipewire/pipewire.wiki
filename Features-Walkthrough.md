# How does PipeWire implement some features:

> - dynamic buffer sizes (latency adapts to application needs)
> - dynamic sample rates (avoids resampling when possible)

AKA the quantum is a combination of buffer-size and sample-rate that defines how frequently the
graph will wake up to process more data.
These get decided based on what the client requires (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/context.c#L980)
There are a lot of variables that go into this.

How the dynamic buffer-size is implemented is best shown with a simple example (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/alsa/test-timer.c). It basically uses a timer and a DLL to balance the selected
quantum and amount of space in the hardware device. This is different from how JACK does things (with interrupts, which
are not dynamic), in some drivers one or the other gives lower latency.

PipeWire can also use IRQ based scheduling. dynamic buffer size switches are disabled then and only performed when explicitly asked by the user.

> - no stable ABI between client and server (can't be shipped in flatpaks,... )
> - no extensible protocol

THe API is versioned and extensible. Old and newer version of the API can coexist.
Look at the profiler API (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/extensions/profiler.h). It's implemented as an extension that is implemented in a module (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/modules/module-profiler.c).

> - limited buffer size (8192 samples, can be too low when working with big samplerates)

The buffer sizes are not hardcoded like JACK but dynamically allocated when negotiating buffers between ports.
The largest buffer size can be set in a config file (https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Config-PipeWire#setting-buffer-size-quantum).
The buffer negotiation code based on parameters from the ports starts here: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/buffers.c#L51

> - only supports float32, no support for other formats to avoid format conversions
> - no support for DSD passthrough
> - no support for AC3/DTS passthrough over SPDIF or HDMI

Basically ports can negotiate all kinds of formats (https://gitlab.freedesktop.org/pipewire/pipewire/-/tree/master/spa/include/spa/param/audio).
The session manager decides if passthrough, DSP or conversion mode is active (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/audioconvert/audioadapter.c#L634). In DSP mode, ports are float32, in convert mode, any of the
raw audio formats is possible, in passthrough, any of the other formats is possible (including video).

DSD and AC3/DTS support in ALSA:  https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/alsa/alsa-pcm.c#L1311

> - no (integrated) bluetooth support

Bluetooth is handled just like any other device. It's just using the Bluez DBus API instead of ALSA.
You can see how that works here: https://gitlab.freedesktop.org/pipewire/pipewire/-/tree/master/spa/plugins/bluez5
 
> - no device suspend, no attempt to save battery..

The suspend is triggered by the session manager based on policy and timeouts. It basically sends a Suspend
command to IDLE nodes and they will stop and close themselves: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/impl-node.c#L419

> - no dynamic hotplug of devices

This is implemented with the device specific hotplug API. For ALSA that is udev: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/alsa/alsa-udev.c

> with automatic rate matching between devices when needed

This is actually the same code as the DLL and timer. Instead of tweaking the timeouts, an adaptive resampler is
configured with the rate difference. Here is where the DLL is done to get a rate difference between the driver
and another device: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/alsa/alsa-pcm.c#L1866

This is where the adaptive resampler recalculates the rates: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/audioconvert/resample-native.c#L154

This is not unlike what zita-a2j does but with lower latency because of the timers and the removal of a
thread.

> - no security model (limited support for sandboxing)

All object in the PipeWire graph have permissions (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/permission.h).
They are enforced in various places (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/pipewire/impl-core.c#L49)
Permissions are assigned by the session manager and the portal. All clients can have a different view of the graph with
certain things disabled. A flatpak application can then, for example, not mess with the system volume or capture from
a microphone/app it was not allowed to.

> - limited support for looping and transport lookahead

The PipeWire equivalent of transport has an array of future tempo and rate changes: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/node/io.h#L203
It adds an extra timeline that can be used to implement looping at a sample level and backwards playback 
and more.

> - no support for control streams (no fun things like automated volume ramps, ...)

PipeWire only has control streams. MIDI, property updates etc are all special contents of the control stream (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/control/control.h).
The control stream is modeled like the lv2 sequence: (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/pod/pod.h#L232). This gives timed events of things that happen. Here is how this is used to implement volume ramps (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/examples/adapter-control.c#L293) and here is how
the ramp is applied (https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/audioconvert/audioconvert.c#L2160).

> - limited support for port metadata such as channel mapping

Ports have properties. One of the properties is the channel assignment or mapping: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/param/audio/raw.h#L171
This is used to link the right ports together but also to adapt a signal to an alternative channel mapping.

> - no support for automatic channel reconfiguration based on client needs.

This is decided by the session manager. It decides if passthrough or DSP mode is used between a client stream and a device.
It then adapts the client channel setup to match the device. This makes it possible to have a stereo client link to
a 5.1 sink and have it upmix the audio. Or to have a stereo capture client (firefox) connect to a mono microphone and
have things work. The channel remixing code is here: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/audioconvert/channelmix-ops.c#L140

> - no video support, no DMA-BUF for zero copy, ...

Video is just another format. You can find the supported video formats here: https://gitlab.freedesktop.org/pipewire/pipewire/-/tree/master/spa/include/spa/param/video
In PipeWire, all buffer memory is passed using file descriptors. This way, you can pass around shared memory but also
DMA-BUF fds. PipeWire marks the fd type in the buffers: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/include/spa/buffer/buffer.h#L45
You can just place the fd in the buffer. Here is how DMA-BUF from v4l2 source is used: https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/v4l2/v4l2-utils.c#L1498

Additional info can be added to the buffers such as the timeline sync objects for explicit sync.

I hope this helps you to get a better idea of what a modern audio stack includes and how it's done.
It's not rocket science but it also depends on years of experience with JACK and PulseAudio and also
various multimedia frameworks and APIs.

