[[_TOC_]]

### Is PipeWire Ready Yet?

PipeWire reached version 1.0.0 on november 26 2023 and has been declared ready and stable.

It has been used since Fedora 27 (November 2017) for Wayland screen sharing and has been the default audio server since Fedora 34 (April 2021).

The API/ABI has been declared stable since version 0.3.

The protocol can support older 0.2 version clients transparently. This means that flatpaks with older PipeWire libraries can connect to a newer daemon.

### Where Is PipeWire In The Stack?

PipeWire sits right on top of the kernel drivers (or as close as possible). You can think of it as a multimedia routing layer on top of the drivers that applications and libraries can use.

### Does PipeWire run on top of a PulseAudio or JACK server?

No, PipeWire is a complete reimplementation of a PulseAudio server and a JACK server. It only requires ALSA to make sound (and bluez5 for bluetooth).

PipeWire still uses the original PulseAudio client library but has a reimplementation of the client JACK library for compatibility with existing applications. 

PipeWire also reuses the ALSA card profile code from pulseaudio. This code is responsible for setting up devices and managing the profiles and mixer settings.


### Don't Pro-Audio and Consumer Audio Have Conflicting Requirements?

They do...

Pro-audio needs low and reliable latency with minimal audio over/underruns. Power usage is of little concern. Pro-audio requires flexible user-configurable routing of the signals.

Consumer Audio focuses on low power usage, latency (in the case of playback) is of no concern. Consumer audio wants things to just work with minimal configuration.

Where JACK and PulseAudio where explicitly (exclusively) tuned for their respective use cases, PipeWire takes a hybrid approach. PipeWire uses the scheduling and graph model of JACK but mainly uses timer-based wakeups like PulseAudio. This makes it possible to dynamically switch between small buffers with low-latency/high power usage and large buffers with high-latency/low power usage. It adapts based on the latency requirements of the application in a glitch-free way. There are limits to this, PipeWire can only increase buffer sizes to 8192 samples (+-180ms) but coupled with much more simple code-paths this should be good enough for consumer use.

PipeWire also supports dynamic add/remove of devices with automatic clock slaving. It handles bluetooth devices or any other node that can be written as a plugin.

PipeWire mainly uses pro-audio formats (floating point samples) as the canonical data-format between nodes in the graph. It is also possible to negotiate other formats to support compressed formats.

The part of PulseAudio that manages the policy is implemented in a separate session manager that can be adapted and configured according to the consumer use case.


### Is PipeWire Just Another GStreamer?

PipeWire is architecturally significantly different from GStreamer and is designed more like JACK. Differences include:

 - The processing graphs are processed in a much more controlled fashion. This allows us to achieve much lower and more predictable latencies. All nodes in the graph are woken up from source to sink when the device wakes up for input/output. Data is processed in fixed sized chunks.
 - lockfree processing.
 - More localized and lighter format negotiation. The Negotiation and format description is borrowed from GStreamer.
 - No dynamic buffer allocation while processing. All buffers and metadata are allocated before processing begins.

GStreamer is intended to be a swiss army knife of multimedia, PipeWire is meant to be much lower level, more like what alsa-lib, JACK or libv4l2 provides.

### Is PipeWire Another JACK Implementation?

Kindof.. PipeWire has a very similar processing model as JACK but adds the following features compared to JACK:

 - Extensible communication protocol that allows new interfaces on objects to be added in the future.
 - Arbitrary formats can be negotiated between nodes. This allows us to handle video as well as compressed formats. This is important for sending compressed formats to the device (AC3 over HDMI or AAC over bluetooth, for example).
 - Negotiation of buffers. A pool of buffers can be negotiated between instances and the memory is exchanged with fd passing. This makes it possible to share hardware surfaces and make video possible.
 - Dynamic sinks and sources. Devices can be hotplugged. There is automatic slaving between devices similar to what a2j does when graphs are joined.
 - Dynamic latency, it adapts the buffer period to the lowest requested latency. Smaller buffer sizes use more CPU but larger buffer sizes have more latency.
 - Synchronous clients are providing data for the current processing cycle of the device(s). There is no extra period of latency.
 - Dynamic device suspend and resume. Unused devices are closed to save CPU.
 - Implemented with sandboxing in mind.
 - Some of the limitations of JACK are fixed. PipeWire has something similar to the JACK transport that also supports looping, trick modes and lookahead of the scheduled timeline.
 - PipeWire has a more generic control type that can be used to implement Midi and OSC natively. Midi similar to a2jmidid is built in.

### Can I use the PipeWire graph processing in my own programs?

Yes. All programs that are linked to libpipewire can use the PipeWire graph without the need to connect to a PipeWire server. See [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/examples/internal.c) for an example audio processing pipeline.

### Does PipeWire Replace ALSA?

No, ALSA is an essential part of the Linux audio stack, it provides the interface to the kernel audio drivers.

That said, the ALSA user space library has a lot of stuff in it that is probably not desirable anymore these days, like effects plugins, mixing, routing, slaving, etc.

PipeWire uses a small subset of the core alsa functionality to access the hardware (It should run with tinyalsa, for example). All of the other features should be handled by PipeWire.


### Why another audio standard, linux already has 13 of them

You might be referring to [this](https://www.gnuyen.org/images/blog/linuxaudio.png) and [xkcd](https://xkcd.com/927/) This is a myth and somewhat of an old joke.

Linux has 1 kernel API named ALSA. PipeWire unifies the 2 user-space servers in practical use: PulseAudio and JACK. With 1 Kernel and 1 user-space audio subsystem, Linux has reached the ideal situation.

The other APIs in the famous picture are either:

1. Frameworks with various audio backends: Allegro, SDL, GStreamer, ffmpeg, Clanlib, OpenAL, Phonon, VLC, ...
2. Dead projects: aRts, ESD, NAS, MAS, ... 
3. Wrappers around audio backends: PortAudio, libao, ...

Neither of these wrappers and frameworks are counted as audio subsystems in MacOS or Windows (although they all implement backends for them) and it is disingenuous to do so in Linux.
 

### Will PipeWire Ever Be As Good As JACK?
 
Possibly... there are some things that JACK can optimize for, like:

- JACK is simpler because it can simply configure ALSA in IRQ modes with a fixed period size. PipeWire needs to manage both timer based scheduling and IRQ based scheduling with dynamic switching when allowed.
- It does not need to care about security and can simply allocate all objects in one fixed piece of shared memory, this makes it much faster to get to the data you need and to introspect objects.
- It does not need to care about negotiation of data formats or buffers, which makes it faster to build the graph and start processing.
- It has a lot of support and history.
- We might not want to support some JACK features, like session management.

Since 0.3.81, PipeWire uses the same scheduling as JACK in the Pro-Audio profile and achieves the same
latency as JACK with slightly fewer CPU cycles. See [performance](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance#latency).

### Why is PipeWire So much worse than JACK? Why do I get so many xruns?

Since 0.3.81 PipeWire uses the same scheduling as JACK and achieves the same latency and
XRuns as JACK. This however only works in Pro-Audio mode, when not in Pro-Audio mode,
read on.

It depends on the hardware and drivers. Some people can get lower latencies with JACK, others
with PipeWire.

PipeWire (when not in pro-audio mode) uses the ALSA API in a completely different way than JACK.

The ALSA API was traditionally designed to be used using the interrupts. This means that the
user space program is woken up at fixed intervals using the hardware interrupt when the device
is ready for reading or writing. This fixed interval is called the ALSA period and directly
defines the latency and can't be changed at runtime.

The kernel drivers are optimized for this use case. Timing reporting, for example, only works
reliably on some devices after an interrupt.

PipeWire does *not* use the interrupts (unless pro-audio profile is selected) and relies on
timers to read the status of the device and read/write more data. This is so that the period
is not fixed and the latency can change depending on the requirements of the applications.
This exposes a couple of problems:

- read/write positions are not always accurate between interrupts.
- User space is not woken up with an IRQ anymore but with a timer. The kernel is not
  optimized for this. -rt kernels might even do worse in this case and cause more xruns.

There are some workarounds, mostly increasing [headroom](Config-Devices#apialsaheadroom).

### Are You Using A Push Or Pull Model For Scheduling?

PipeWire uses pull model. This means that the device wakes up at the last possible moment to pull in more data from all the nodes in the graph. This allows for the lowest possible latency between producing the data and consuming it.

This is in contrast to GStreamer that mostly uses the push model. In this model, data is produced independently of the device and is then queued in the device or queues in front of the device (in case of video playback).

### Isn't Format Negotiation Bad For Pro Audio?

Yes. Format conversions are not cheap and must be avoided. For audio processing in PipeWire we have the following rules:

 - Filters and real-time clients must use float 32 mono audio. The audio processing graph is only operating in this format.
 - Format conversions are done at the input/output nodes. This means that conversions are done to and from devices and also to and from clients that use the stream API.
 - This also means that the conversion code for clients runs in the context of the client and not the server. This also avoids issues with having complicated code such as decoders running in the server context.

### What about bit-perfect playback

PipeWire has a passthrough mode that lets the client and device negotiate a format and transfer
data directly without any conversions. This is used for:

* Encoded formats passthrough over S/PDIF or HDMI. (see VLC, mpv, ...)
* Native DSD audio playback. (see pw-dsdplay)
* Passthrough raw audio playback (not enabled by default, but see below)

By default, PipeWire requires all clients to convert the source audio format to
floating point, which is then converted back to the audio card format. When no additional
processing is performed in the graph and no volume or channelmixing or resampling is
needed, this conversion is entirely lossless up to 24 bits formats (see unit tests).

In the future, we expect to add more options to automatically disable the potential sources of lossyness when
bit-perfect playback is requested.

### What is the Pro Audio Profile?

In addition to the PulseAudio profiles, all audio cards now have an extra "Pro Audio" profile. Selecting this profile does the following things:

* Exposes all the devices available on the card.
* All devices from a card are assumed to share the same clock and so the adaptive resamplers are
  disabled.
* Exposes the maximum number of channels on all devices.
* The channels are labelled as AUX0, AUX1, ...
* Disables the hardware mixers, it only enables software volume/mute.
* A session manager will configure streams to stereo and route to the first two channels (AUX0 and AUX1).
* Since 0.3.81 it will be configured to use IRQ based wakeup and links the related ALSA devices together.

This profile is meant to be used on audio cards with:

* More than 8 channels for input or output. Channel assignment is probably wrong for these cards.
* Cards with many instruments connected to each input. It makes sense to handle each channel as a separate unrelated stream.
* Output Cards with many unrelated outputs, like speakers on the first 2 channels, headphones on the second channel-pair, additional speakers on the other channels.

For consumer cards with stereo or surround configurations, it does not make sense to use this Profile.

When using this profile, it might make sense to configure additional virtual sources to route applications to specific channels: like [this](Virtual-Devices#behringer-umc404hd-speakersheadphones-virtual-sinks).

Note that before 0.3.81 this profile did *not* change any of the latency settings or configurations on the device.

Since 0.3.81 this profile will use IRQ based scheduling with linked devices when there is 1 capture and 1 playback device. This results in the same latency as can be achieved with JACK on the device. Devices with more than 1 capture and playback device will *not* be using IRQ scheduling because these devices typically don't allow running all devices at the same time and will fail to start when linked.


### Pro Audio Profile: A Further Explanation

It is called "Pro" Audio because it is meant to be used by "pro" audio users (i.e. ones who use complex JACK pipelines and want the devices to be exposed in a similar fashion).

Pro Audio exposes the raw ALSA devices without applying any logic on top.

Normally, ALSA exposes devices and controls that reflect hardware features, but they do not always reflect how the hardware should be used. In many cases, you cannot use some devices simultaneously or the same device is used to access different inputs or outputs and the selection is done by triggering ALSA controls. Also, volume controls work in a strange way where you have multiple controls affecting the same output.

To make this easier, we have ACP and UCM, two different mechanisms that try to provide user-friendly profiles for these devices by automatically showing/hiding specific devices and applying controls when needed to provide a pleasant user experience. Pro Audio goes around the profile mechanism, and exposes the devices like they are in ALSA. Without hiding anything and without applying controls, Pro Audio also does not manage hardware volume controls, so you are free to use alsamixer and control the hardware in any way that you like.

### What are those *Analog Stereo* and *Digital Stereo (IEC958)* profile?

TL;DR: You want to use the Analog Profile, it will sound best. Digital Profile is only used in specialized cases as you
can read below.

The [Digital](https://alsa.opensrc.org/DigitalOut) Stereo Profile is meant to send uncompressed stereo and compressed surround formats such as AC3, DTS or Dolby Atmos digitally to a receiver to be decoded by the receiver. It bypasses part of the mixers in the hardware and makes the digital signal available for receiver with either a coaxial S/PDIF cable, an optical S/PDIF cable or an HDMI cable. If you do not have a receiver that can decode these streams, you should use the Analog Stereo profile to get better mixer controls.

If you select this profile, you should probably also select the codecs that the receiver can decode, see the advanced tab in pavucontrol. Uncompressed stereo PCM can always be sent over IEC958 but not all receivers support the other compressed formats.

Some hardware does not have a coaxial, optical or HDMI output but you still have the profile available. This is likely because hardware has the feature but is not made available. With the right model:vendor id (see with lsusb) it is possible to make a custom profile and remove the nonfunctional IEC958 profile.

Most hardware has the Digital output also connected to the DAC so that it will still produce output on the Analog outputs, usually with likely less optimal mixer settings (volume too low, less bass, ...).


### Why are my speaker not mapped correctly for my 5.1 surround setup?

This is currently a chicken-and-egg problem. The problem at the core is a combination of a bad default in PipeWire because of potential bugs in ALSA.

ALSA has an API to query the channel layout (`snd_pcm_query_chmaps()`) but that often results in wrong information.

PipeWire (and also PulseAudio) therefore assume that the card is working in the 'default' ALSA channel layout, which
appears to be true more often.

There is no easy way out of this problem. Ideally PipeWire should use the ALSA channel map by default and when that is wrong, fix the kernel driver or temporary disable the ALSA channel map. This would however create a fair amount of regressions we are not prepared to deal with yet...

For now, you can work around this by:

1. Enable ALSA channel map [here](https://pipewire.pages.freedesktop.org/wireplumber/configuration/alsa.html) using `["api.alsa.use-chmap"] = true` for the device.
2. Force a specific channel layout with the `["audio.position"] = "..."` property on the device

See also [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1707)

### What About Pro Video?

 - Similar to audio we have one common format: RGBA float32 premultiplied linear video. This should be easy to generate and manipulate on GPU/CPU and allow for HDR and simple compositing operations.
 - A splitter/converter for video devices. We need to convert the v4l2 buffers to the common format so that the filters can work on them. Likewise we need converters inside the server side stream API to send/receive video in other formats.

### What Kind Of API Will There Be To Interface With PipeWire?

A lowlevel API that allows you to create a node that you can then add to a local or remote processing graph. This API gives you full control over format and buffer negotiation, supports multiple input and outputs as well as controls, commands, events and parameters. The node will be part of the real-time processing graph and provides data for the current processing cycle of the graph.

There is a filter API that can be used to make audio and video filters. It can have multiple input and output ports as well as parameters. It is like the JACK client API but more powerful.

The most used API will be the stream API. The idea is to create a stream object that allows you to play or record one stream from the server. You then receive callbacks when a buffer needs to be
provided for playback or when a buffer is available for capture. The stream API has a client side component that will do format and buffer size conversions when requested. The stream API has simple controls for audio volume and video colorbalance. The stream API can work synchronous and asynchronous. It is like the pulseaudio API but more powerful.

There is a replacement library for JACK client so that they run on top of PipeWire. PulseAudio application continue to use the pulseaudio client library. There is a replacement pulseaudio daemon implemented on top of PipeWire.

### What Audio API Do You Recommend To Use?

The situation is a bit like GUI toolkits. There are many, each with different use cases. Nobody uses the native display server protocols directly (X11, Wayland) but always through an abstraction layer (GTK, Qt, etc).

We recommend that you continue to use PulseAudio, JACK and ALSA API's for now.

### Should I uninstall everything pulseaudio?

A pulseaudio install contains a *server* and *client* libraries.

We recommend you uninstall the pulseaudio *server* and use the PipeWire pulseaudio replacement
server instead. It has better [performance](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance#pulseaudio-pipewire-0381-in-server), interfaces and integrates directly with the rest of
the PipeWire audio system. In fact, running pulseaudio together with a PipeWire setup configured
for audio is not supported (and will fail as both servers fight for devices).

For applications that are already using the pulseaudio *client* libraries we recommend that you
keep using it. Some applications (firefox) can also be compiled with different backends but this
is not recommended because:

- The PulseAudio backend is usually the best (only?) maintained one.
- The PulseAudio API has more features compared to ALSA:
  - Stream volume management. An ALSA application will have to implement this itself without
    integrating into the system.
  - Buffering management. Latency changes and requirements are dynamically adjusted based on
    stream routing. ALSA does not provide an API for feedback about this.
  - Timing and synchronization.  ALSA does not provide a good API for this (no systemic latency).
  - Format renegotiation. ALSA does not provide an API for this.
  - Metadata information. ALSA does not provide an API for this.
  - The ALSA PipeWire plugin does not support passthrough. Also most applications only implement
    passthrough with the PulseAudio API anyway.
- The PulseAudio API has more features compared to JACK:
  - Stream volume management. A JACK application will have to do this itself with no integration
    into the rest of the system.
  - Buffering management. JACK application will block the PipeWire graph from dynamic latency
    changes.
  - Format renegotiation. JACK only provides 1 sample format to applications.
  - Metadata information. JACK provides no usable metadata API for this purpose.
  - The JACK API will not cause the session manager to automatically link to the default sink.
    You will have to run extra software to make dynamic pro-audio connections.
  - The JACK API will not dynamically adapt the channels and layout to the connected sink.
  - The JACK API has no possibility to handle passthrough formats.
- Possibility for integration into the system, ALSA and JACK provide very little API for
  reporting about stream movements, error cases, suspend, detection of (virtual) devices,
  enumeration of passthrough formats.. etc..

In short, the pulseaudio API is a good and featureful API for clients, there is no reason
to use less featureful APIs.

### Is there a native GUI tool to configure PipeWire?

It depends what kind of configuration you want to do. Most features are configured in the config files, See [Wiki](home).

To configure Card Profiles, default devices, ports and other settings, we recommend to use pavucontrol. Your
desktop might also have settings pannels to configure aspects of PipeWire (GNOME-Control-Center).

To make connections in the graph, there are [qpwgraph](https://gitlab.freedesktop.org/rncbc/qpwgraph) and
[Helvum](https://gitlab.freedesktop.org/pipewire/helvum). You can also use the JACK applications such
as Carla, qjackctl or Catia.

The PipeWire lowlevel API is a loose collection of objects, properties and parameters that are combined into a coherent use case by the Audio Toolkit in use (JACK/PulseAudio). So any GUI without a concrete use case would not make much sense. 

That said, there are cli tools to configure and manipulate every aspect of PipeWire. See the migration from [JACK](Migrate-JACK)/[PulseAudio](Migrate-PulseAudio) to see how to map existing use cases to lowlevel PipeWire operations. 

### What Is Wrong With JACK + PulseAudio?

PulseAudio has a JACK backend that sends all the mixed streams to JACK. It however has some problems:

  - Smaller JACK period sizes wake up pulseaudio a lot, causing it to use massive amounts of CPU.
  - Suspend of the JACK device is not implemented/possible.
  - Passthrough on the JACK device is not possible.
  - Individual streams in PulseAudio are not managed inside JACK.

Since 0.3.71, a jackdbus module was added so that pipewire can implement this exact same functionality,
only *much* more efficient.

### Why Not Just Improve JACK Instead?

 - JACK has no support for negotiating formats or buffers. This makes it hard to implement anything like exclusive access to devices or more complicated buffer memory. PipeWire attempts to keep the same goals as JACK but with adding format and buffer negotiation.
 - The JACK API has no support for fd backed memory. For video it is important to leave the pixels on the GPU instead of touching it with the CPU. It's not clear how this can be added nicely. One option would be to embed more data into the port buffers. With an extension to the protocol we could place a data structure in a local buffer with the video fd.
 - Current JACK implementations do not care about security of sandboxed clients.

It was decided that it was simply not feasible to add these missing features to the JACK architecture and it was better
to start from scratch.

### Why Not Just Improve PulseAudio Instead?

 - The PulseAudio design does not allow for video buffers.
 - PulseAudio design is not suited for the kind of low-latency we target. There is too much logic and context switches between the client and device.

It was decided that it was simply not feasible to add these missing features to the Pulseaudio architecture and it was better
to start from scratch.

### How Is PipeWire Supposed To Be A Better PulseAudio?

 - PipeWire can achieve lower latency with much less CPU usage and dropouts [compared](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance#pulseaudio-pipewire-0381-in-server) to PulseAudio. This would greatly improve video conferencing apps, like WebRTC in the browser.
 - PipeWire's security model can stop applications from snooping on each other's audio.
 - PipeWire allows more control over how applications are linked to devices and filters.
 - PipeWire uses an external policy manager that can provide better integration with the rest of the desktop system and configuration.

### How Is PipeWire Supposed To Be A Better JACK?

 - PipeWire is more dynamic by design. It can expose all devices and does similar things that zita-a2j/j2a can provide. The implementation of merging the devices and doing resampling is also a lot more efficient than what zita-a2j can provide.
 - Multiple devices don't need to be resampled to a common clock when they are not in any way linked to each other.
 - It handles Bluetooth devices or any device for which a plugin can be made.
 - PipeWire can adapt the latency dynamically, which is important for power usage on a laptop. When low latency is required, the system can switch automatically and seamlessly to smaller buffer sizes.
 - PipeWire allows arbitrary formats, which makes it possible to implement exclusive access to devices, passthrough and more. This is important if you want to send raw DTS to your amplifier or AAC to your Bluetooth headphones, potentially improving audio quality and preserving power.
 - ~~PipeWire will implement full latency compensation. This is not available in JACK and it would be hard to implement efficiently.~~
 - PipeWire can achieve the same latency as JACK with, in some simple benchmarks, [fewer](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance#jack-clients) CPU cycles.

### Why Not use sndio

[sndio](https://sndio.org) for audio is conceptually very similar to PulseAudio  and before that
ESD and so it shares the same fundamental shortcomings:

 - Communication happens with pipes. This introduces buffering and an undefined latency.
   There is no pull based mechanism in place.
 - It is also quite heavy because it is not zero copy. It performs well for larger buffers
   but degrades for smaller buffers.
 - It has no support for video.
 - It has limited security features based on file permissions.
 - It has no support for dynamic latency.
 - No support for ALSA card profiles, complicated mixer setups, etc..

That said, it would be possible to implement an sndio frontend to PipeWire, similar to
how pulseaudio works. This would probably fix some of the shortcomings related to
permissions, alsa card profiles and dynamic latency.

### Why Can't we just use OSS and mix everything in the kernel

This is a bad idea for many reasons:

 - It is not necessary to do the conversion, resampling and mixing in the kernel. It does *not*
   improve latency because the applications still need to provide the kernel with the data, only
   now you need to have N well behaved applications instead of one well behaved sound server.
 - mixing and mostly resampling requires floating point operations, which are problematic in the
   kernel. To do mixing and resampling efficiently, you also need to use vector operations for various
   architectures and adapted to various use cases. This complexity and flexibility in implementations
   is not something you would want in the kernel, especially when it would make things worse, see above.
 - Dynamic latency adjustments would be harder to implement, especially without support for
   this in OSS itself.
 - Adaptive resampling and clock slaving between devices is another complex use case that
   can better be done in user space and should therefore not be added to the kernel.
 - You would still need a user space implementation of all this to support bluetooth and
   various network protocols.

### How Is PipeWire Going To Avoid Xruns?

 - All the regular system tuning you might do to avoid or reduce xruns still apply, for now. 
 - PipeWire uses a thread with real-time priority, eventfd and epoll in the data processing path. This does not fix avoid the underrun/overrun problems but using simple primitives allows the processing pipeline to run on a real-time kernel subsystem like EVL (https://evlproject.org). This might be to solution in the long term to avoid xruns.   
  
### How Is PipeWire Going To Handle Latency?

 - ~~The plan is to implement full latency compensation in PipeWire. This means that streams will be sample accurately aligned even when signals go through different paths with different latencies because of how PipeWire allocates memory, this can be done quite efficiently by changing offsets in the sample buffers.a~~
 - For how PipeWire handles latency for USB devices please see some [performance](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Performance#latency) numbers.

### How Are Multiple Devices Handled?

When playing or capturing audio from multiple devices, the audio will slowly drift out of sync because of the different clocks used by the devices. PipeWire handles this by using an [adaptive resampler](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/spa/plugins/audioconvert/resample-native.c) and a DLL to make the signal match the clock of the device.

When 2 devices are joined in the graph in some way, one of the devices is selected as the driver and the other(s) as followers. The followers adapt the sample rate to keep up with the driver. PipeWire uses the same mechanism for timer based wakeups and driving the resampler, which results in an extremely stable rate difference estimation. Only the delay of the resampler (usually around 64 samples) is added as extra latency.

It is also possible to link 2 devices together with a [word clock](Config-Devices#clockname) to avoid the software resampling. Check out more info on how to configure the resampler and device priorities [here](Config-Devices).

### Why Is The API So Complicated?

"Can't an audio API be simply open/read/write"? "Why do I need a mainloop and callbacks"? "Isn't doing audio essentially just copying samples to and from a buffer"?

For anything more than playing a beep, it is more complicated.

At the lowest level, the device decides when more samples need to be written or read to/from the device ring buffer. This is usually implemented with an interrupt of some sort. For optimal performance, the application needs to directly react to this signal and read/write samples to the device ring buffer. This is called the pull module.

This way, the application can wait with generating the audio data until the last possible moment to achieve the lowest possible latency. Volume updates or synthesizers can react to GUI sliders and keyboard events with lower latency this way.

With a simple read/write model this cannot be done, you need an API to wait for the device signal either with a poll or an event. Additional API that provides timing information can also work but then you need to do polling or implement the timeouts or callbacks yourself, likely with less accurate results than what the device interrupt can provide.

That said, you can always write a simple open/read/write API on top of pull based API and PipeWire provides the more low level APIs to make this possible. Look at [pa_simple](https://freedesktop.org/software/pulseaudio/doxygen/simple.html), which also works fine on PipeWire.

There is some more about this here:

 * [Push vs Pull in SDL](https://discourse.libsdl.org/t/sdl-audio-push-vs-pull-model/3923)
 * [Linux Audio Systems](https://blog.linuxplumbersconf.org/2009/slides/Paul-Davis-lpc2009.pdf)

### PipeWire Buffering Explained

PipeWire has two 'buffers':

 1. One it keeps in the hardware device by keeping one period (quantum) of data in the sink. If there is < quantum of data it runs the graph to ask all nodes to provide one new quantum of data. Presumably that can happen before the sink finishes playing the remaining data (if not: xrun). Batch devices (alsa devices that report themselves as batch) enable IRQs at quantum/2 (called the ALSA period-size, by default 1024/2) and keep an extra quantum/2 of data in the device as headroom. So, normal devices introduce a latency of `quantum`, batch devices run at a latency of `quantum + quantum/2`.

 2. Buffers in the application. This can be anything you would want it to be. jack clients use 0 buffering and react to the graph wake-up immediately. stream based clients can do the same thing but usually do some sort of extra buffering.

This does not include extra buffering in the resamplers (\~50 samples). There is a resampler in each stream and sink/source. The resampler latency is 0 when the resampler is not used.

This does also not include latency introduced by the USB subsystem or other hardware latencies. These are generally the same as with JACK and PulseAudio when the IRQ period-size is set to a sufficiently low value for batch devices.

The quantum on the server is controlled by the clients node.latency property. It it always set to the lowest requested latency. If you start a PipeWire app with PIPEWIRE_LATENCY=128/48000 it will (at most) use a 2.6ms quantum and the latency between a client waking up and the sink read pointer will be (at most) 2.6ms.

With `default.clock.max-quantum` you should be able to configure 128 samples for a 2.6ms server latency. If you use `pw-top` you can see the selected latency between app/device. I would suggest to see what it says there after adjusting the max-quantum.

For PulseAudio clients, it is the `pipewire-pulse` server that is woken up every quantum and it has an internal buffer based on what the client negotiated. Clients in general set buffering requirements based on configuration options in the application or you can use `PULSE_LATENCY_MSEC` env variable to configure things. Other than that it should work pretty much the same way as PulseAudio.

Note that `PIPEWIRE_LATENCY=` does nothing for PulseAudio clients. You must use the pulseaudio client environment variables to control the latency, like `PULSE_LATENCY_MSEC`.

So, first use max-quantum to limit server latency, configure [WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/configuration/alsa.html) set batch period size (for batch devices), then use client config or env variable to set client latency.

See [also](Config-PipeWire#setting-buffer-size).

Also note that all PipeWire clients of the same driver will use *the same* buffer size, regardless of the requested buffer size. At no point will PipeWire attempt to run clients with a different buffer size. This means that if a client asks for 1024 samples buffer size and another one asks for 64 samples, *both* clients will run with 64 samples. If a client needs to do additional buffering, it needs to take this into account: See also [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/2845).

### Does PipeWire Support Network Streaming?

PipeWire is optimized for local use cases and does *not* transparently work over a network. It is however possible to implement network streaming with various [modules or applications](Network).

### Does PipeWire require DBus/SystemD/Flatpak

No, all of these are optional dependencies, PipeWire can function without those dependencies.

DBus can be disabled at compile time or at [runtime](Config-PipeWire#general-options). If enabled,
it is used for:

- Bluetooth support. The bluetooth stack (Bluez) on Linux is provided using a DBus API. Without
  DBus, there is no bluetooth support possible.
- Interface with RTKit to acquire realtime priorities. If no DBus is available, it will fall
  back to posix scheduling API, which requires the user to have the right
  [limits](Performance-tuning#rlimits).
- Tag clients that are managed by the portal with the right properties so that the session
  manager can securely handle them.
- Let the session manager perform [Device reservation](http://git.0pointer.net/reserve.git/tree/reserve.txt).
  This can be used to improve interoperation between JACK and PipeWire so that they can
  manage exclusive device access between each other.
- Register the pulseaudio server name on DBus. This might be used by some clients to detect
  when a pulseaudio server is running.

Systemd integration can be disabled at compile time. If enabled, it is used for:

- Logging to the journal. A fallback logger will use stderr instead.
- Management of the PipeWire, PipeWire-pulse and session manager services.
- Socket activation of the PipeWire and PipeWire-pulse services.
- logind to manage logged in users. This is mostly important for the Bluetooth services
  because only one user can use the bluetooth stack at a time.

PipeWire is a lowlevel service and is not shipped as a Flatpak usually. Application shipped
as a Flatpak can use PipeWire but it is not a requirement. PipeWire can enforce extra
permissions on Flatpak applications.

### PCI-E Link Switching

Frequent and brief HDMI (and possibly DisplayPort) audio drop-outs/cuts may be caused by the PCI-E link switching between signalling generations. See #2375.

## User Questions

### Does Desktop Audio Interfere With Pro-Audio Using PipeWire?

No, we're going to make sure they don't interfere. We have some options to do this:

- Tag some devices as DSP devices and make sure when in use by DSP apps, we don't route non-DSP apps to it.
- Make sure the pipewire-pulse server can't see the DSP tagged devices.
- Go into a 'DSP' mode where we simply don't service non-DSP apps.

I think in the end we will need to look at some concrete use cases and then implement the policy correctly.

Taken [from Gitlab](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/425).

As it is currently implemented in 0.3.81, the Pro Audio profile will lock the quantum (latency) and samplerate of the graph
for a constant latency. No special care is taken to route non-pro audio applications.

### Bypass Mixer In Exclusive Mode?

Please see [issue #126](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/126).

### Developers: About a2j In PipeWire Current Midi Catch 22 With JACK

Please see [issue #406](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/406).

### Output To Two Devices?

Please see [issue #508](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/508)

### Could PipeWire Be Used To Work Around Lack of Acceleration in Xinerama?

Please see [issue #522](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/522)

### How to Change rtkit rt.prio? 

Please see [issue #685](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/685)

### Is There a Way to Connect Droidcam Audio?

Please see [issue 713](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/713)

### Ho do I link my filter to a sink monitor?

Use the `node.name` of the sink and then set `stream.capture.sink = true`. This instructs the
session manager to attempt to link to the sink monitor ports.

### See Also

For a list of questions from the [users](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues?sort=updated_desc&state=closed&label_name[]=question).
