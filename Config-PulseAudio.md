[[_TOC_]]

> :information_source: &nbsp;&nbsp; See also: https://docs.pipewire.org/page_config.html

# General

A relatively small server (`pipewire-pulse`) that converts the PulseAudio native protocol to PipeWire protocol. It allows clients linked to the PulseAudio client library to talk directly to `pipewire-pulse`, which then creates streams to PipeWire.

All of the policy and device configuration is still managed by the session manager. Check the session manager configuration guide.

The `pipewire-pulse` server implements a sample cache that is currently not otherwise available in PipeWire

Almost all of the native PulseAudio tools should continue to work. We recommend the following tools:

| Tool | Purpose |
|:--|:--|
| pactl | command line introspection of PulseAudio server and configuration
| pavucontrol | GUI configuration tool
| paplay | play sound files
| parecord | record sound files

We refer to [PulseAudio Migration](Migrate-PulseAudio) for more information for how to use the replacement PulseAudio server.

# Configuration File (`pipewire-pulse.conf`)

> :warning: &nbsp;&nbsp; This documentation is moved to: https://docs.pipewire.org/page_man_pipewire-pulse_conf_5.html

The `pipewire-pulse` module has a configuration file template located in `/usr/share/pipewire/pipewire-pulse.conf`. You can copy and edit the file to `/etc/pipewire/` or `~/.config/pipewire/pipewire-pulse.conf`.

Since 0.3.45 you can also copy sections of the config file to a file (with a `.conf`extension) in the directories `/usr/share/pipewire/pipewire-pulse.conf.d/`, `/etc/pipewire/pipewire-pulse.conf.d/` or `~/.config/pipewire/pipewire-pulse.conf.d/`. Properties will be merged with the previous ones, array entries will be appended.

The PulseAudio compatible server is implemented by the `libpipewire-module-protocol-pulse` module, which should be loaded by the config file in the context.modules section. We refer to the
[module-protocol-pulse](https://docs.pipewire.org/page_module_protocol_pulse.html) Documentation.

## Stream Configuration

The [`pipewire-pulse.conf`](#configuration-file-pipewire-pulseconf) file can
also contain a [stream.properties](Config-client#streamproperties) section with properties for streams created
by the pipewire-pulse server:

```css
...
stream.properties = {
    #node.latency = 1024/48000
    #node.autoconnect = true
    #resample.disable = false
    #resample.quality = 4
    #monitor.channel-volumes = false
    #channelmix.disable = false
    #channelmix.min-volume = 0.0
    #channelmix.max-volume = 10.0
    #channelmix.normalize = false
    #channelmix.mix-lfe = true
    #channelmix.upmix = true
    #channelmix.upmix-method = psd  # none, simple
    #channelmix.lfe-cutoff = 150.0
    #channelmix.fc-cutoff = 12000.0
    #channelmix.rear-delay = 12.0
    #channelmix.stereo-widen = 0.0
    #channelmix.hilbert-taps = 0
    #dither.noise = 0
    #dither.method = none # rectangular, triangular, triangular-hf, wannamaker3, shaped5
    #debug.wav-path = ""
}
```

Some of these properties map to the PulseAudio `/etc/pulse/default.pa` config entries as follows:
| PulseAudio | PipeWire | Notes |
|:--|:--|:--|
| remixing-use-all-sink-channels | channelmix.upmix |
| remixing-produce-lfe | channelmix.lfe-cutoff | Set to > 0 to enable
| remixing-consume-lfe | channelmix.mix-lfe |
| lfe-crossover-freq | channelmix.lfe-cutoff |

It is also possible to add [stream.rules](Config-client#rules) to the pipewire-pulse config file.

## PulseAudio Rules

For each client, a set of rules can be evaluated to configure quirks of the client
or to force some pulse specific stream configuration.

The general look of this section is as follows and follows the layout of [Rules](Config-PipeWire#rules).

```css
pulse.rules = [
    {
        # skype does not want to use devices that don't have an S16 sample format.
        matches = [
             { application.process.binary = "teams" }
             { application.process.binary = "teams-insiders" }
             { application.process.binary = "skypeforlinux" }
        ]
        actions = { quirks = [ force-s16-info ] }
    }
    {
        # speech dispatcher asks for too small latency and then underruns.
        matches = [ { application.name = "~speech-dispatcher*" } ]
        actions = {
            update-props = {
                pulse.min.req          = 1024/48000     # 21ms
                pulse.min.quantum      = 1024/48000     # 21ms
            }
        }
    }
]
```

Please refer to the [Module documentation](https://docs.pipewire.org/page_module_protocol_pulse.html)
for a list of client quirks and properties that can be configured.

## Autostart Configuration

As part of the server startup procedure you can execute some additional commands with the `pulse.cmd` section in `pipewire-pulse.conf`. Currently only the load-module command is implemented.

```css
...
pulse.cmd = [
    { cmd = "load-module" args = "module-always-sink" flags = [ ] }
    { cmd = "load-module" args = "module-switch-on-connect" }
    { cmd = "load-module" args = "module-gsettings" flags = [ "nofail" ] }
]
...
```

You can also run some additional commands with the `context.exec` section in `pipewire-pulse.conf`.

It takes an array of objects with a path (program) to run and the arguments.

You can use it to load extra modules with `pactl (or better use pulse.cmd for this)` or run a script with `/usr/bin/sh`.
Here are some examples:

```css
...
context.exec = [
    { path = "pactl"  args = "load-module module-switch-on-connect" }
    { path = "/usr/bin/sh"  args = "~/.config/pipewire/pipewire-pulse.sh" }
]
...
```

# Modules

Please refer to the [migrating guide](Migrate-PulseAudio#modules-1) for more information about the supported modules and how to migrate.

# Network Support

The PipeWire PulseAudio server has fairly complete network support, include RTP and ROC support.

## `module-native-protocol-tcp`

Use:

```bash
pactl load-module module-native-protocol-tcp [port=<port, default:4713>] [listen=<ip, default:127.0.0.1>]
```

This loads a new server listening on the given port and IP.

You can then use:

```bash
PULSE_SERVER=tcp:[<ip>:]<port> pactl info
```

to connect to it. Fill `<ip>` with the IP address of the host running the server and `<port>` with the port.

You can unload the module by id or name:

```bash
pactl unload-module module-native-protocol-tcp
```

to kill all connections and stop the server.

You can also load a sink that will forward all audio to this remote server with:

```bash
pactl load-module module-tunnel-sink server=tcp:[<ip>:]<port>
```

## `module-simple-protocol-tcp`

Use:

```bash
pactl load-module module-simple-protocol-tcp rate=48000 format=s16le channels=2 record=true port=8001
```

This loads a new server listening on the given port for new connections. Connected clients receive a stream of raw samples. Applications such a `simple protocol player` can play these streams.

This module is also available as a native [module-protocol-simple](https://docs.pipewire.org/page_module_protocol_simple.html).

## `module-tunnel-sink`

Use:

```bash
pactl load-module module-tunnel-sink server=tcp:<ip>
```

This creates a new sink. Playing audio on the sink will play the audio on the remote PulseAudio/PipeWire server at `<ip>`. The remote server needs to be listening on tcp with the `module-native-protocol-tcp`

## `module-tunnel-source`

Use:

```bash
pactl load-module module-tunnel-source server=tcp:<ip>
```

This creates a new source. Capturing audio from the source will capture the audio from the remote PulseAudio/PipeWire server at `<ip>`. The remote server needs to be listening on tcp with the `module-native-protocol-tcp`

## `module-zeroconf-discover`

Available since 0.3.28

Use:

```bash
pactl load-module module-zeroconf-discover
```

Loads the zeroconf discover code that will create tunnel sources and sinks for all published devices.

## `module-zeroconf-publish`

Available since 0.3.31

Use:

```bash
pactl load-module module-zeroconf-publish
```

Loads the zeroconf publish code that will publish tunnel sources and sinks on the network through zeroconf.

## `module-roc-sink`

Available since 0.3.31

Use:

```bash
pactl load-module module-roc-sink
```
This internally uses [module-roc-sink](https://docs.pipewire.org/page_module_roc_sink.html).

## `module-roc-source`

Available since 0.3.31

Use:

```bash
pactl load-module module-roc-source
```
This internally uses [module-roc-source](https://docs.pipewire.org/page_module_roc_source.html).

## `module-raop-sink` and `module-raop-discover`

Available since 0.3.41

Use:

```bash
pactl load-module module-raop-discover
```

## `module-rtp-send`

Available since 0.3.60

Use:

```bash
pactl load-module module-rtp-send
```

It will link to the default source and start streaming on the multicast address.
The stream is announced with SAP/SDP.

This internally uses [module-rtp-sink](https://docs.pipewire.org/page_module_rtp_sink.html).

## `module-rtp-recv`

Available since 0.3.60

```bash
pactl load-module module-rtp-recv
```

Listens on multicast for SAP/SDP messages and creates a new stream for each one.

This internally uses [module-rtp-source](https://docs.pipewire.org/page_module_rtp_source.html).

# Environment variables

In addition to the generic [PipeWire](Config-PipeWire#environment-variables) environment variables pipewire-pulse supports:

`PULSE_RUNTIME_PATH` and `XDG_RUNTIME_DIR` to find the directory where to create the native
protocol pulseaudio socket.

