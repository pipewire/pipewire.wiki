When a new client connects to the daemon, it's PID, UID, GID and security label are usually known in a secure way.
The application is initially blocked from doing any further communication unless something sets permissions.

`module-portal` is executed first. It monitors the well-known dbus name of the portal (`org.freedesktop.portal.Desktop`). All clients with the same pid as the portal are tagged as
managed by the portal.

After that `module-access` is executed. Based on the PID and the files it it's root directory, it can classify the client as a flatpak or other contained application.

The session manager will see the new client and will configure the permissions on the client object based on how the client was tagged by portal/access modules. Permissions are given as a tuple of two integers: `<id,permissions>`. The id identifies the global object id and permissions is a bit mask of allowed operations on the object. This includes Read, Write, Execute access but more permission bits can be added later. The `id -1` sets the default permissions for objects without specific permissions.

In a similar way, the session manager can revoke or grant permissions on existing or new objects as they appear.

It is important that the session manager knows about the allowed objects for a client because it needs to use this information to select possible devices or other nodes.
