[[_TOC_]]

# Setting from Configuration File is Ignored

Please see [#938](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/938) for an example.