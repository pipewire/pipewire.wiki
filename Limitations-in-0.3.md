### General

 - Emulation libraries need to be tested with a lot of applications. Many application (ab)use API in different ways that all need to be supported.
 - The ALSA plugin is probably quite complete and seems to work with a lot of apps.
 - The JACK library is quite complete. The JACK API is not huge but apps still expect certain implementation details that are not documented well (order of callbacks, thread contexts, etc)
 - ~~PulseAudio in general is a big API with many subtle bugs that will need time to perfect. Some things currently work with the pulseaudio API, others don't. Most apps should be able
   to produce/consume sound.~~ PulseAudio support is now implemented with a replacement daemon. The replacement library is deprecated. The daemon has much better compatibility and uses the regular pulse library.
 - ~~Bluetooth is not tested much and might not work very well yet.~~ See below.
 - Portaudio apps tend to do bad things (audacity) like opening all devices multiple times in
   all possible formats and configurations to 'probe' the devices. This is quite expensive.

### ALSA
 
 - ~~No mixer, unclear how it could make sense.~~ since 0.3.8.
 - Tested: aplay, vlc, mplayer, sweep, apulse

### JACK

 - No session management API implemented, possibly not important/used.
 - Internal clients not implemented.
 - ~~Freewheeling not implemented. Since 0.3.7 there is a dummy driver that could be used to implement this later.~~ Freewheeling is implemented since 0.3.28.
 - ~~Latencies not implemented at all yet.~~ Latency reporting is implemented since 0.3.29.
 - No jackdbus yet.
 - Tested: ardour5, qjackctl, carla, catia, jack_simple_client, jack-smf-player, xjadeo, qsynth, calfjackhost, ...

### PulseAudio

 - ~~Move to sink/source is not implemented.~~ since 0.3.7.
 - ~~Default source/sink is not implemented.~~ since 0.3.7.
 - ~~Volumes are done in software currently, hardware volumes are not touched.~~ since 0.3.7 uses UCM or PulseAudio profile config to implement hardware volume and mute.
 - ~~Profiles on devices is not implemented.~~ since 0.3.7. supported with UCM and PulseAudio profile config.
 - ~~Time reporting is inaccurate and broken, apps might skip, stutter or hang. better since 0.3.4, firefox youtube works perfect, chrome youtube has trouble.~~ Better with pulse-server in 0.3.16.
 - ~~Extensions are not implemented.~~ Only the stream-restore extension is implemented for the notification volume. device-restore is implemented for setting formats and codecs. device-manager is not implemented yet.
 - There are problems with switching between ports and profiles when plugs are (dis)connected.
 - Tested: paplay, pavucontrol, vlc, mplayer, firefox, chrome, webrtc, youtube, spotify, ...

### Bluetooth

 - A2DP source and sink works well, ~~LDAC codec causes stuttering~~, AptX/HD should work fine.
 - SCO needs testing

### Flatpak

- Flatpaks using PulseAudio will work with the pulse-server implementation.
- Flatpaks with JACK will not work and need to be converted to use the replacement `libjack.so`.
- Tested: chrome, firefox, spotify, lutris with the libretro runner, steam, sonic robo blast 2, virt manager (own packaging, not in Flathub), audio over SPICE TCP socket.
