# General

This page contains a guide for people migrating from PulseAudio to PipeWire and the pipewire-pulse replacement server.

[[_TOC_]]

# Settings/Config Files

Settings are not migrated from PulseAudio by default. Also the scripts (`.pa`) files are not executed or converted. Not all settings and custom scripts can be migrated yet but here is an overview of how to migrate the files.

## `/etc/pulse/daemon.conf`

Most server settings can be set in [`/etc/pipewire/pipewire.conf`](Config-PipeWire#configuration-file-pipewireconf).

| PulseAudio  | PipeWire |
|:--|:--|
| daemonize  | N/A
| fail | N/A
| allow-module-loading | [pipewire-pulse](Config-PulseAudio) `pulse.allow-module-loading`.
| allow-exit | N/A
| use-pid-file | Is a [pipewire-pulse](Config-PulseAudio) setting.
| system-instance | N/A
| local-server-type | N/A
| enable-shm | N/A
| enable-memfd | N/A
| shm-size-bytes | N/A
| lock-memory | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `mem.mlock-all`.
| cpu-limit | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt configuration`.
| high-priority | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt configuration`.
| nice-level = -11 | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt configuration`.
| realtime-scheduling | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt configuration`.
| realtime-priority | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt configuration`.
| exit-idle-time | N/A
| scache-idle-time | N/A
| dl-search-path | [PipeWire](Config-PipeWire#configuration-file-pipewireconf).
| load-default-script-file | N/A
| default-script-file | N/A
| log-target | N/A
| log-level | [PipeWire](Config-PipeWire#configuration-file-pipewireconf).
| log-meta | N/A
| log-time | N/A
| log-backtrace | N/A
| resample-method | N/A, is always default native resampler.
| avoid-resampling | N/A, is always avoided and can't be disabled.
| enable-remixing | Remixing is always enabled.
| remixing-use-all-sink-channels | [Stream](Config-client#streamproperties)
| remixing-produce-lfe | [Stream](Config-client#streamproperties)
| remixing-consume-lfe | [Stream](Config-client#streamproperties)
| lfe-crossover-freq | [Stream](Config-client#streamproperties)
| flat-volumes | N/A, flat volumes are always false.
| rescue-streams | N/A, always true.
| rlimit-fsize | N/A
| rlimit-data | N/A
| rlimit-stack | N/A
| rlimit-core | N/A
| rlimit-as | N/A
| rlimit-rss | N/A
| rlimit-nproc | N/A
| rlimit-nofile | N/A
| rlimit-memlock | N/A
| rlimit-locks | N/A
| rlimit-sigpending | N/A
| rlimit-msgqueue | N/A
| rlimit-nice | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt`.
| rlimit-rtprio | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt`.
| rlimit-rttime | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) `libpipewire-module-rt`.
| default-sample-format | [pipewire-pulse](Config-PulseAudio).
| default-sample-rate | [PipeWire](Config-PipeWire#configuration-file-pipewireconf) and [pipewire-pulse](Config-PulseAudio).
| alternate-sample-rate | [PipeWire](Config-PipeWire#configuration-file-pipewireconf)
| default-sample-channels | [pipewire-pulse](Config-PulseAudio).
| default-channel-map | [pipewire-pulse](Config-PulseAudio).
| default-fragments | N/A
| default-fragment-size-msec | [ALSA](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/Config-ALSA) `api.alsa.period-size`.
| enable-deferred-volume | N/A, never deferred.
| deferred-volume-safety-margin-usec | N/A
| deferred-volume-extra-delay-usec | N/A

## `/etc/pulse/client.conf`

Because PulseAudio client still use the original `/etc/pulse/client.conf` all of the settings will be transferred. There is an exception for the autospawn option, that is not supported.

To make the `auto-connect-localhost` option work, you need to start a local TCP native server, see [here](Config-PulseAudio#module-native-protocol-tcp).

For non-pulseaudio clients we list here how to convert a native client:

| PulseAudio  | PipeWire |
|:--|:--|
| default-sink | environment `PIPEWIRE_NODE`
| default-source | environment `PIPEWIRE_NODE`
| default-server | environment `PIPEWIRE_REMOTE`
| default-dbus-server | N/A
| autospawn | N/A
| daemon-binary | N/A
| extra-arguments | N/A
| cookie-file | N/A
| enable-shm | N/A
| shm-size-bytes | N/A
| auto-connect-localhost | N/A
| auto-connect-display | N/A

## `/etc/pulse/default.pa`

Most of the `/etc/pulse/default.pa` script consists of loading the required functionality in the PulseAudio server. This is mostly implemented in the session manager, see the [modules](#modules-1) section.

Other custom configuration would involve loading modules to create virtual devices for loopback or remap-sink. We refer to [virtual devices](Virtual-Devices) for how to load these modules and their native equivalents.

Custom modules loaded in `default.pa` can be loaded in the `context.exec` section of
[`pipewire-pulse.conf`](Config-PulseAudio#configuration-file-pipewire-pulseconf) or an override in `pipewire-pulse.d/`.
You can load `module-switch-on-connect`, for example like this:

```
context.exec = [
    { path = "pactl"  args = "load-module module-switch-on-connect" }
]
```
You can also use `/usr/bin/sh` to start custom scripts as part of the setup.

# `pactl`

When using the PipeWire PulseAudio replacement server, all of pactl commands should work as usual. Some of the pactl commands have native PipeWire equivalents that will also work without pipewire-pulse.

Below is a list of pactl commands and the native equivalents:

## Stat

pactl stat lists the allocated memory on pipewire-pulse and does not have an equivalent in native PipeWire.

## Info

The user/host/cookie fields are info from the core object: `pw-cli i 0`

The default sample rate is taken from the default sample rate of the core: `pw-cli i 0`.

The Default Channel Map is taken from
[`pipewire-pulse.conf`](Config-PulseAudio#configuration-file-pipewire-pulseconf)
in the `pulse.default.position` config option. It is mostly used as a default for the format and channel map when loading modules. There is no equivalent default setting for PipeWire.

## List

Most list commands have an equivalent in PipeWire with the `pw-cli ls <type>` or `pw-dump` command.

### Modules

`pw-cli ls Module` lists the modules in PipeWire

`pactl list modules` will list the same modules and also includes the modules loaded in the `pipewire-pulse` daemon that don't have an equivalent native PipeWire module.

### Sinks/Sources

`pw-cli ls Node`. Use the node-id in `pw-cli i <node-id>` to get more info. Use `pw-cli e <node-id> <param-id>` to enumerate parameters.

Sinks are nodes with "media.class" = "Audio/Sink". Sources have "media.class" = "Audio/Source" or "Audio/Source/Virtual". There are also Nodes that are both sink and source with "media.class" = "Audio/Duplex".

To get the supported formats of a sink/source do:
```
pw-cli e <node-id> EnumFormat
```

To get the currently used format of a sink/source do:
```
pw-cli e <node-id> Format
```

The volumes on the sink/source are either from the associated device route params (see card) or from the `Props` parameter on the sink/source.

The ports are from the associated device `EnumRoutes` param and the currently active port from the `Route` param (see card).

### Sink Inputs and Source Outputs

`pw-cli ls Node`. Use the node-id in `pw-cli i <node-id>` to get more info. Use `pw-cli e <node-id> <param-id>` to enumerate parameters.

sink-inputs are nodes with "media.class" = "Stream/Output/Audio". source-outputs are nodes with "media.class" = "Stream/Input/Audio".

To get the supported formats of a sink_input or source_output do:
```
pw-cli e <node-id> EnumFormat
```

To get the currently used format of a sink_input/source_output do:
```
pw-cli e <node-id> Format
```

The volume is taken from the `Props` param on the node:
```
pw-cli e <node-id> Props
```

### Clients

Functionally identical to `pw-cli ls Client`

### Samples

Samples are not natively implemented in PipeWire. They are only implemented in the pipewire-pulse server.

### Cards

`pw-cli ls Device`. Use the device-id in `pw-cli i <device-id>` to get more info. Use `pw-cli e <device-id> <param-id>` to enumerate parameters.

PipeWire devices with the "media.class" = "Audio/Device" are mapped to cards directly.

Profiles are enumerated with the `EnumProfile` param on the device. The `Profile` param contains
the currently active profile on the device.

Port are enumerated with the `EnumRoute` param. The currently active ports, per device, are enumerated with the `Route` param on the device. The volume of the active ports can also be found in the `Route` param.

## Samples

A Sample cache is only available in the pipewire-pulse server. You can upload/play and remove samples with the pactl commands like before.

```
pactl [options] upload-sample FILENAME [NAME]
pactl [options] play-sample  NAME [SINK]
pactl [options] remove-sample  NAME
```
There is no native equivalent of a sample cache in PipeWire.

## Modules

pipewire-pulse has support for some pulseaudio modules. They can be loaded and unloaded in the same way as is done on PulseAudio:
```
pactl [options] load-module  NAME [ARGS ...]
pactl [options] unload-module  NAME|#N
```

This it the status of the modules:

supported: can be used with pactl to load a module
alternative: there is (also) a native alternative, notes contain how

| Name | Supported | Alternative | Notes |
|:--|:-:|:-:|:--|
| module-allow-passthrough | No | No |
| module-alsa-card | No | Yes | Session manager, pw-cli.
| module-alsa-sink | Yes | Yes | Session manager, pw-cli.
| module-alsa-source | Yes | Yes | Session manager, pw-cli.
| module-always-sink | Yes | Yes | Session manager.
| module-always-source | No | No | Session manager.
| module-augment-properties | No | Yes | Session manager.
| module-bluetooth-discover | No | Yes | Session manager.
| module-bluetooth-policy | No | Yes | Session manager
| module-bluez5-device | No | Yes | Session manager.
| module-bluez-discover | No | Yes | Session manager.
| module-card-restore | No | Yes | Session manager.
| module-cli-protocol-tcp | No | No | Obsolete.
| module-cli-protocol-unix | No | No | Obsolete.
| module-cli | No | No | Obsolete (see pacmd).
| module-combine-sink | Yes | Yes | module-combine-stream
| module-console-kit | No | No |
| module-dbus-protocol | No | No |
| module-default-device-restore | No | Yes | Session manager.
| module-detect | No | No | Obsolete.
| module-device-manager | No | No |
| module-device-restore | No | Yes | Session manager.
| module-echo-cancel | Yes | Yes | Since 0.3.29
| module-equalizer-sink | No | Yes | With filter-chains
| module-esound-compat-spawnfd | No | No | Obsolete.
| module-esound-compat-spawnpid | No | No | Obsolete.
| module-esound-protocol-tcp | No | No | Obsolete.
| module-esound-protocol-unix | No | No | Obsolete.
| module-esound-sink | No | No | Obsolete.
| module-filter-apply | No | No |
| module-filter-heuristics | No | No |
| module-gsettings | Yes | No |
| module-hal-detect | No | No | Obsolete.
| module-http-protocol-tcp | No | No | Obsolete.
| module-http-protocol-unix | No | No | Obsolete.
| module-intended-roles | No | No |
| module-jackdbus-detect | Yes | Yes | Since 0.3.71
| module-jack-sink | No | Yes | See [jack.device](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/Config-ALSA#properties)
| module-jack-source | No | Yes | See [jack.device](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/Config-ALSA#properties)
| module-ladspa-sink | Yes | Yes | filter-chains
| module-lirc | No | No |
| module-loopback | Yes | Yes | pw-loopback.
| module-match | No | No |
| module-mmkbd-evdev | No | No |
| module-native-protocol-fd | No | No |
| module-native-protocol-tcp | Yes | Yes | pipewire-pulse.
| module-native-protocol-unix | No | Yes | pipewire-pulse.
| module-null-sink | Yes | Yes | pw-cli.
| module-null-source | No | Yes | pw-cli.
| module-oss | No | No |
| module-pipe-sink | Yes | Yes | Since 0.3.28
| module-pipe-source | Yes | Yes | Since 0.3.31
| module-position-event-sounds | No | No |
| module-raop-discover | Yes | Yes | libpipewire-module-raop-discover
| module-raop-sink | No | Yes | libpipewire-module-raop-sink, loaded by module-raop-discover only
| module-remap-sink | Yes | Yes | pw-loopback.
| module-remap-source |yes | yes | pw-loopback.
| module-rescue-streams | No | No |
| module-role-cork | No | No |
| module-role-ducking | No | No |
| module-rtp-recv | Yes | Yes | libpipewire-module-rtp-source
| module-rtp-send | Yes | Yes | libpipewire-module-rtp-sink
| module-rygel-media-server | No | No |
| module-simple-protocol-tcp | Yes | Yes |
| module-simple-protocol-unix | No | No |
| module-sine | No | Yes | pw-cli.
| module-sine-source | No | Yes | pw-cli.
| module-stream-restore | No | Yes | Session manager, pw-metadata.
| module-suspend-on-idle | No | Yes | Session manager [suspend-node](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/Config-general#suspend-node)
| module-switch-on-connect | Yes | No | 
| module-switch-on-port-available | No | Yes | Session manager.
| module-systemd-login | No | No |
| module-tunnel-sink-new | Yes | Yes | named module-tunnel-sink
| module-tunnel-sink | Yes | Yes | Obsolete.
| module-tunnel-source-new | Yes | Yes | named module-tunnel-source
| module-tunnel-source | Yes | Yes | Obsolete.
| module-udev-detect | No | Yes | Session manager.
| module-virtual-sink | Yes | Yes | Example, pw-loopback also [example-sink](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/modules/module-example-sink.c)
| module-virtual-source | Yes | Yes | Example, pw-loopback also [example-source](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/src/modules/module-example-source.c)
| module-virtual-surround-sink | No | Yes | See [Filter-chain](Filter-Chain#virtual-surround)
| module-volume-restore | No | Yes | Obsoleted by module-stream-restore.
| module-x11-bell | Yes | Yes | module-x11-bell
| module-x11-cork-request | No | No | Obsolete.
| module-x11-publish | No | No | Obsolete.
| module-x11-xsmp | No | No | Obsolete.
| module-zeroconf-discover | Yes | Yes | Since 0.3.28
| module-zeroconf-publish | Yes | No | Since 0.3.31

### `module-alsa-sink`

This module makes a new ALSA sink. Use the device= argument to specify the pcm device.

### `module-alsa-source`

This module makes a new ALSA source. Use the device= argument to specify the pcm device.

### `module-always-sink`

This module ensure that there is always a (dummy) sink.

### `module-card-restore`

This module restores the saved profile of a card. PipeWire has this feature implemented in the session manager.

When setting a profile, you can specify the save flag to save this new profile as a default.

### `module-combine-sink`

This module creates a virtual sink that combines all or selected sinks into one big virtual sink.

This module is implemented with the same parameters that pulseaudio supports. It is implemented with the the native [`module-combine-stream`](Virtual-Devices#combine-streams) module and [module-combine-stream](https://docs.pipewire.org/page_module_combine_stream.html).

### `module-default-device-restore`

This module restores the saved default source and sink devices. PipeWire has this feature implemented in the session manager.

The session manager stores the default devices into the default metadata with the `default.configured.*` keys.

The session manager will use the available devices to populate the current default devices in the metadata with `default.*` keys.

You can query the currently configured nodes with:
```
pw-metadata
```

### `module-device-restore`

Restores the volumes of devices. This functionality is implemented in the session manager.

Whenever a new Route is activated, the volumes are restored. When the Route properties are changed and the save flag is set, the settings are saved.

The session manager also maintains a list of routes that were manually selected per profile. It will try to restore the routes when the profile is activated.

### `module-echo-cancel`

This module is implemented with the same parameters that pulseaudio supports. It is implemented with the the native [`module-echo-cancel`](Virtual-devices#echo-cancellation) module.

### `module-gsettings`

This module reads the same GSettings keys as PulseAudio and loads the same modules. It should work with paprefs.

### `module-jackdbus-detect`

This module Automatically creates a JACK client with a sink and source to exchange data with a running JACK server. It turns PipeWire into a 0-latency effect chain for JACK.

This module is implemented with the same parameters that pulseaudio supports. It is implemented with the the native [module-jackdbus-detect](https://docs.pipewire.org/page_module_jackdbus_detect.html).

### `module-ladspa-sink`

This module is implemented with the same parameters that pulseaudio supports. It is implemented with the [filter-chain](Filter-Chain) module.

### `module-loopback`

`pactl load-module module-loopback <pulseaudio-properties>`

Is equivalent to executing:

```
pw-loopback <options>
```

The following mapping applies to pulseaudio-properties and options

| PulseAudio | PipeWire | Notes |
|:--|:--|:--|
| source | -C |
| sink | -P |
| latency_msec | -l |
| rate | / | Not available rate is always DSP rate to avoid resampling.
| channels | -c |
| channel_map | -m | Expects a json array. Channel names need to be converted.
| sink_input_properties | --playback-props = {} 
| source_output_properties | --capture-props = {} 
| source_dont_move | / | `node.dont-reconnect=true` in --capture-props 
| sink_dont_move | / | `node.dont-reconnect=true` in --playback-props
| remix | / | No equivalent.

### `module-native-protocol-tcp`

PulseAudio can load this module to enable the native protocol on new TCP ports at runtime. PipeWire also supports loading this module at runtime to open up TCP ports on demand. When the module is unloaded, the TCP socket is removed and clients are closed.

You can also permanently configure TCP ports in the
[`pipewire-pulse.conf`](Config-PulseAudio#configuration-file-pipewire-pulseconf)
config file.

In the server.address array, add a new "tcp:[<listen>:]<port>" entry to make the pulse server listen on this port, for example:

```
...
    {   name = libpipewire-module-protocol-pulse
        args = {
            # the addresses this server listens on
            server.address = [
                "unix:native"
                "tcp:4713"
            ]
            #pulse.min.req = 128/48000              # 2.7ms
...
```

For more information, see the
[module-protocol-pulse](https://docs.pipewire.org/page_module_protocol_pulse.html)
documentation.

### `module-null-sink`

`pactl load-module module-null-sink <pulseaudio-properties>`

This is equivalent to executing:

```
pw-cli create-node adapter factory.name=support.null-audio-sink media.class="Audio/Sink" object.linger=1 monitor.channel-volumes=1 monitor.passthrough=1 <pipewire-properties>
```

The following mapping applies to pulseaudio-properties and pipewire-properties:

| PulseAudio | PipeWire | Notes |
|:--|:--|:--|
| sink_name | node.name |
| sink_properties | * | Add the properties directly.
| format | - | Not available, always F32.
| rate | audio.rate |
| channels | audio.channels |
| channel_map | audio.position | Channel names are converted, ex: front-left -> FL.

### `module-null-source`

Null sources can be created with the native API:

```
pw-cli cn adapter factory.name=audiotestsrc media.class="Audio/Source" object.linger=1 node.name=my-null-source node.description="My null source"
```

### `module-remap-sink`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-remap-sink  <properties>
```
This module is also available with `pw-loopback`. See [Virtual Devices](Virtual-Devices#remap-sink).


### `module-remap-source`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-remap-sink <properties>
```
This module is also available with `pw-loopback`. See [Virtual Devices](Virtual-Devices#remap-source).

### `module-simple-protocol-tcp`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-simple-protocol-tcp rate=48000 format=s16le channels=2 record=true port=8001
```
And then use `simple protocol player` to play a stream. You might need to use pavucontrol or the metadata to redirect the stream to a sink monitor.

This module is also available as a native module that you can load and configure by adding this blob to the context.modules config section, see the [module-protocol-simple](https://docs.pipewire.org/page_module_protocol_simple.html) documentation.

### `module-sine`, `module-sine-source`

You can create a sine stream:
```
pw-cli cn adapter factory.name=audiotestsrc media.class="Stream/Output/Audio" object.linger=1 node.name=my-sine-stream node.description="My sine stream" node.autoconnect=1
```

Or a sine source:
```
pw-cli cn adapter factory.name=audiotestsrc media.class="Audio/Source" object.linger=1 node.name=my-sine-source node.description="My sine source"
```

### `module-stream-restore`

This module is usually loaded by default in PulseAudio and manages a database to load and save stream volumes.

As such the session manager maintains a similar table in the restore-stream module. When a stream is created, it is matched against the keys in the table. If there is a match then volume/mute and target.node are restored for the stream.

You can consult the contents of the database with the metadata API:
```
pw-metadata -n route-settings
```
Modifications to this table are saved by the session manager. This makes it possible to, for example, change the volume of the Notification sounds:

```
pw-metadata -n route-settings 0 'restore.stream.Output/Audio.media.role:Notification' '{ "mute": false, "volumes": [ 0.686530 ], "channels": [ "MONO" ] }' 'Spa:String:JSON'
```

### `module-suspend-on-idle`

This module is implemented in the session manager with the `suspend-node` module. The default timeout for this module is 5 seconds.

Individual nodes can request a a different timeout by setting the `session.suspend-timeout-seconds` property. A value of 0 disabled suspend for the node.

Refer to the media-session configuration for how to set the timeouts.

### `module-switch-on-connect`

This module is only implemented for Pulseaudio compatibility, and is useful only when some applications try to manage the default sinks/sources themselves and interfere with PipeWire's builtin default device switching.

### `module-tunnel-sink`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-tunnel-sink server=<ip>
```
And then redirect audio to the new sink.

This module is also available as a native module that you can load and configure by
loading the module in context.modules. See
[module-pulse-tunnel](https://docs.pipewire.org/page_module_pulse_tunnel.html)
for more documentation.

### `module-tunnel-source`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-tunnel-source server=<ip>
```
And then capture audio from the new source.

This module is also available as a native module that you can load and configure by
loading the module in context.modules. See
[module-pulse-tunnel](https://docs.pipewire.org/page_module_pulse_tunnel.html)
for more documentation.

### `module-zeroconf-discover`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-zeroconf-discover
```
And it will create tunnel sources and sink for all published devices.

This module is also available as a native module that you can load and configure
by loading the module in context.modules.  See
[module-zeroconf-discover](https://docs.pipewire.org/page_module_zeroconf_discover.html)
for more documentation.

### `module-zeroconf-publish`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-zeroconf-publish
```
And it will announce all sources and sinks on this server.


### `module-raop-discover`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-raop-discover
```
And it will create sinks for all published devices.

This module is also available as a native module that you can load and configure
by loading the module in context.modules.  See
[module-raop-discover](https://docs.pipewire.org/page_module_raop_discover.html)
for more documentation.

### `module-rtp-send`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-rtp-send
```
And it will create a new sender RTP stream. The stream will be announced with SAP.

This module is also available as a native module that you can load and configure
by loading the module in context.modules.  See
[module-rtp-sink](https://docs.pipewire.org/page_module_rtp_sink.html)
for more documentation.

### `module-rtp-recv`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-rtp-recv
```
It will listen for SAP announcements and create streams for each one.

This module is also available as a native module that you can load and configure
by loading the module in context.modules.  See
[module-rtp-source](https://docs.pipewire.org/page_module_rtp_source.html)
for more documentation.


### `module-x11-bell`

This module is supported in pipewire-pulse so you can do:
```
pactl load-module module-x11-bell
```

This module is also available as a native module that you can load and configure
by loading the module in context.modules.  See
[module-x11-bell](https://docs.pipewire.org/page_module_x11_bell.html)
for more documentation.

## Move

Streams (sink-input/source-output) are linked and moved by the session manager.

You can use pactl or pavucontrol to move streams:
```
pactl [options] move-(sink-input|source-output) #N SINK|SOURCE
```
The PipeWire native equivalent is by using the metadata tool.
```
pw-metadata <stream-id> target.node <node-id>
```
This instructs the session manager to move stream <stream-id> to the sink/source with name/id <node-id>

You can move the stream back to the default with:
```
pw-metadata -d <stream-id> target.node
```

## Suspend

Use pactl to suspend a sink/source:
```
pactl [options] suspend-(sink|source) NAME|#N 1|0
```

The PipeWire native equivalent is by using the pw-cli tool.
The native equivalent requires you to look up the ID of the Sink/Source (Node) with `pw-cli ls Node`. Then you can suspend the node with:

```
pw-cli c <node-id> Suspend '{}'
```

## `set-card-profile`

Use pactl to set the card profile:
```
pactl [options] set-card-profile  CARD PROFILE
```
The native equivalent requires you to look up the ID of the card (Device) with `pw-cli ls Device` and the index of the profile with `pw-cli e <card-id> EnumProfile`. Then you can set the profile with:

```
pw-cli s <card-id> Profile '{ index: <profile-index>, save: true }'
```
Set the `save` flag to `true` if you want to session manager to save this profile as the default for the device.

You can query the current profile with:
```
pw-cli e <card-id> Profile
```

See also [here](Guide-IEC958) for more detailed instructions about IEC958 passthrough.

## Default Sink/Source

Use pactl to set the default sink and source. You can use `pactl info` to list the current defaults.

Run pw-metadata and look for id:0 and the default.* keys.

| Key | Meaning |
|:--|:--|
| default.configured.audio.source | User configured audio source.
| default.configured.audio.sink | User configured audio sink.
| default.audio.source | Currently configured default source.
| default.audio.sink | Currently configured default sink.

The currently configured values are calculated from the priorities of the currently available devices and the default.configured.* values.

You can change the configured defaults with:
```
pw-metadata 0 default.configured.audio.sink '{ "name": "<sink-name>" }'
```
You need to get a sink name with `pw-cli ls Node` and look for the "node.name" property for a valid value.

## Sink/Source Port Volume/Mute/Port-Latency

Change the active port on a sink/source:
```
pactl [options] set-(sink|source)-port NAME|#N PORT
pactl [options] set-(sink|source)-volume NAME|#N VOLUME [VOLUME ...]
pactl [options] set-(sink|source)-mute NAME|#N 1|0|toggle
pactl [options] set-port-latency-offset CARD-NAME|CARD-#N PORT OFFSET
```
The native way to perform this action is to first locate the device associated with the sink/source node. You can use `pw-cli ls Node`, locate the sink/source node, do `pw-cli i <node-id>` and remember the "device.id" and "card.profile.device" property. Some sinks/sources don't have an associated device and then they don't have any ports to change either.

Then do `pw-cli e <device-id> EnumRoute` to list all the Route params on the device. Remember the index you would want to switch to, you can only switch to routes that contain the <card-profile-device> in the devices list. Then do this to switch to the port:
```
pw-cli s <device-id> Route '{ index: <route-index>, device: <card-profile-device> }'
```

You can do `pw-cli e <device-id> Route` to read the current active ports on the device and the volumes associated with them. Some routes have a `latencyOffsetNsec` property with the port latency.

You can set volume, mute and/or port-latency while you switch or on an already active route:
```
pw-cli s <device-id> Route '{ index: <route-index>, device: <card-profile-device>, props: { mute: false, channelVolumes: [ 0.5, 0.5 ] }, save: true }'
```
Route settings are saved by the session manager when the `save` flag is `true` in the Route param.

It's not possible to toggle the mute with pw-cli, you need to manually read the old value and set a new toggled value.


## Sink-Input/Source-Output Volume/Mute

Change the volume/mute of a stream:
```
pactl [options] set-(sink-input|source-output)-volume #N VOLUME [VOLUME ...]
pactl [options] set-(sink-input|source-output)-mute #N 1|0|toggle
```
This is performed on the node of the stream. First find the stream with `pw-cli ls Node`, remember the node id. Then you can introspect the current volume/mute state with:
```
pw-cli e <node-id> Props
```
Then you can change the properties with:
```
pw-cli s <node-id> Props '{ mute: false, channelVolumes: [ 0.3, 0.3 ] }'
```

## Sink Formats

```
pactl [options] set-sink-formats #N FORMATS
```
You can use this as on PulseAudio to configure passthrough formats. You need to
place the device in "Digital Stereo (IEC958) Output" and then you can do,
for example:

```
pactl set-sink-formats <node-id> 'pcm;ac3-iec61937;dts-iec61937'
```
The session manager will save and restore these settings on the Route param of the
device associated with the sink.

You can get the same functionality with pw-cli:

```
pw-cli e <card-id> Route
```
In the properties is a `iec958Codecs` array of codecs. Remember the index of the
route and the device.

You can change the settings with:

```
pw-cli s <card-id> Route '{ index: <route-index>, device: <device-index>, props: { iec958Codecs: [ PCM, AC3, DTS ] }, save: true }'
```

See also [here](Guide-IEC958) for more detailed instructions about IEC958 passthrough.

## Subscribe

Listen to events with pactl:
```
pactl [options] subscribe
```
The native equivalent is to run `pw-mon` or `pw-dump -m`. `pw-link -iolm` can also be used to monitor ports and links between them.

# `pacmd`

pacmd will likely not be supported by PipeWire. fortunately, most of its functionality is available in pactl and other places.

Here is a table with conversion between pacmd, pactl and pw-cli.

| pacmd  | pactl |  pw-cli | Notes  |
|:--|:--|:--|:--|
| help | help | help | Generic help for the tool. |
| list-modules | list modules | ls Module | `pw-cli i <id>` to inspect module details.
| list-cards | list cards | ls Device | `pw-cli i <id>` to inspect Card/Device details.
| list-sinks | list sinks | ls Node | media.class = Audio/Sink, `pw-cli i <id>` for more details.
| list-sources | list sources | ls Node | media.class = Audio/Source, `pw-cli i <id>` for more details.
| list-clients | list clients | ls Client | `pw-cli i <id>` for more details.
| list-sink-inputs | list sink-inputs | ls Node | media.class = Stream/Output/Audio, `pw-cli i <id>` for more details.
| list-source-outputs | list source-outputs | ls Node | media.class = Stream/Input/Audio, `pw-cli i <id>` for more details.
| stat | stat | i 0 | Memory statistics not exposed in PipeWire.
| info | list | ls | Also `pw-dump`.
| load-module | load-module |  | dynamically loading modules not supported in PipeWire, new objects are created with factories. Modules are loaded at startup.
| unload-module | unload-module | | See load-module.
| describe-module | `send-message /core pipewire-pulse:describe-module <module-name>` | | Introspects a module without loading it.
| set-sink-volume | set-sink-volume | |
| set-source-volume | set-source-volume | |
| set-sink-mute | set-sink-mute | |
| set-source-mute | set-source-mute | |
| set-sink-input-volume | set-sink-input-volume | |
| set-source-output-volume | set-source-output-volume | |
| set-sink-input-mute | set-sink-input-mute | |
| set-source-output-mute | set-source-output-mute | |
| set-default-sink | set-default-sink | |
| set-default-source | set-default-source | |
| set-card-profile | set-card-profile | |
| set-sink-port | set-sink-port | |
| set-source-port | set-source-port | |
| set-port-latency-offset | set-prot-latency-offset | |
| suspend-sink | suspend-sink | |
| suspend-source | suspend-source | |
| suspend | *not available* |  | Suspend all sinks and sources.
| move-sink-input | move-sink-input | |
| move-source-output | move-source-output | |
| update-sink-proplist | *not possible* | |
| update-source-proplist | *not possible* | |
| update-sink-input-proplist | *not possible* | |
| update-source-output-proplist | *not possible* | |
| list-samples | list samples | | Sample cache part of pipewire-pulse, no native support.
| play-sample | play-sample | | Sample cache part of pipewire-pulse, no native support.
| remove-sample | remove-sample | | Sample cache part of pipewire-pulse, no native support.
| load-sample | upload-sample | | Sample cache part of pipewire-pulse, no native support.
| load-sample-lazy | *not supported* | |
| load-sample-dir-lazy | *not supported* | |
| kill-client | *not supported* | destroy <id> | Destroy a global Client object.
| kill-sink-input | *not supported* | destroy <id> | Destroy a global Node object.
| kill-source-output | *not supported* | destroy <id> | Destroy a global Node object.
| set-log-target | *not supported* | Use config to set target | 
| set-log-level | `send-message /core pipewire-pulse:log-level <num>` | 
| set-log-meta | *not supported* |  | Always enabled.
| set-log-time | *not supported* |  | Always enabled.
| set-log-backtrace | *not supported* |  |
| send-message | send-message | |
| play-file | *not supported* | | Use pw-cat or paplay.
| dump | list | ls | Also pw-dump.
| dump-volumes | list | ls | Also pw-dump.
| shared | *not supported* | |
| exit | exit | | Does nothing.
