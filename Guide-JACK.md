
[[_TOC_]]

# jackdbus bridge

Since 0.3.81, PipeWire will automatically become a JACK client when jackdbus is started.

To make PipeWire become a JACK client when jackdbus is started on older versions,
make a new file in `~/.config/pipewire/pipewire.conf.d/20-jackdbus.conf` with
the following contents:

```
context.modules = [
{   name = libpipewire-module-jackdbus-detect
    args { }
}
]
```

Restart the PipeWire process and start jackdbus.

# Set the samplerate

By default PipeWire will select the best samplerate for the audio processing based on the
allowed sample rates and available streams.

You can also force a samplerate of, for example 96000Hz, for the data processing by typing into
a terminal window:

```
pw-metadata -n settings 0 clock.force-rate 96000
```

You can go back to the dynamic samplerate selection:

```
pw-metadata -n settings 0 clock.force-rate 0
```

See also the [configuration](Config-PipeWire#setting-sample-rates).

# Set buffersize

By default PipeWire will select the best buffersize based on the available clients and
configured limits and defaults.

You can force a buffer size, of for example 256, by typing into a terminal window:

```
pw-metadata -n settings 0 clock.force-quantum 256
```

You can restore dynamic behaviour again with:

```
pw-metadata -n settings 0 clock.force-quantum 0
```

See also the [configuration](Config-PipeWire#setting-buffer-size-quantum).

# Calibrate systemic latency of devices

See [here](Config-Devices#systemic-latency-calibration) for how to do this statically or at runtime.
