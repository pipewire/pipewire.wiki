[[_TOC_]]

# Virtual Devices

These are devices that do not correspond to real hardware devices but are handled just like real devices by most applications.

There are two types of virtual devices:

1) [Single nodes](#single-nodes) in the graph. They are usually implemented with null-sink and have input/output ports that need to be linked manually (with pw-link or link-factory) after creation. They can be used, for example, to create inputs/outputs for processing chains. 

2) [Coupled streams](#coupled-streams). This is internally implemented as a capture and playback stream that can implement some processing such as channel mixing or remapping. They automatically connect to default/specified source/sinks and can be moved with tools like pavucontrol. 

A special case of a coupled stream is the [Filter-chain](Filter-Chain) that can be used to make virtual devices
that need some more complicated logic.

## Configuration

Virtual devices are configured by adding their definitions in a file (with a `.conf` extension) to `~/.config/pipewire/pipewire.conf.d/` or `/etc/pipewire/pipewire.conf.d/`. For example, add a single file called `10-null-sink.conf` to the config directory with this content:

```
context.objects = [
    {   factory = adapter
        args = {
            factory.name     = support.null-audio-sink
            node.name        = "my-sink"
            media.class      = Audio/Sink
            audio.position   = [ FL FR FC LFE RL RR ]
            monitor.channel-volumes = true
            monitor.passthrough = true
            adapter.auto-port-config = {
                mode = dsp
                monitor = true
                position = preserve
            }
        }
    }
]
```

Restart PipeWire and you will see a new my-sink audio sink. It will be unlinked. You can link it to a
node called `system` by adding a file `20-link-null-sink.conf` to the config directory with this
content:

```
context.objects = [
    {   factory = link-factory
        args = {
            link.output.node = my-sink
            link.output.port = output_FL
            link.input.node  = system
            link.input.port  = playback_1
            link.passive     = true
        }
    }
]
```
Creating links from the config file is difficult because most sinks and ports are dynamically
created and might not be available when the config file is processed. It is really only
possible to link between completely static node definitions.

It is recommended to use [coupled streams](#coupled-streams) to make virtual sinks because they
will automatically be linked by the session manager.


## Single Nodes

Single nodes are the smallest building block and are often used to create the input/output for effect chains.

They are usually created from a `support.null-audio-sink` factory, either in a config file or with pw-cli.

To have the volume changes be applied to the node output (monitor) ports, you will need to pass the
`monitor.channel-volumes = true` option.

To let latency information pass through the monitor ports, add the `monitor.passthrough = true` option.

Merge the following `context.objects` section into a config file or a `conf.d` directory:

```
context.objects = [
    {   factory = adapter
        args = {
           factory.name     = support.null-audio-sink
           node.name        = "my-sink"
           media.class      = Audio/Sink
           object.linger    = true
           audio.position   = [ FL FR FC LFE RL RR ]
           monitor.channel-volumes = true
           monitor.passthrough = true
        }
    }
]
```

You can do the same with `pw-cli`:

```
pw-cli create-node adapter '{ factory.name=support.null-audio-sink node.name=my-sink media.class=Audio/Sink object.linger=true audio.position=[FL FR FC LFE RL RR] monitor.channel-volumes=true monitor.passthrough=true }'
``` 

The `media.class` and `audio.position` keys are the most important. `audio.position` defines the number of input and output ports this node has and their name. For each input port, there is a matching output port. The signal is passed from the input port to the output port after applying volume.

The following `media.class` values can be used:

| `media.class` | Function |
|:--|:--|
| Audio/Sink | An audio sink that can receive data. Has playback_* input ports and monitor_* output ports for each channel. Can be selected as output in PulseAudio applications. Has a monitor source for input. |
| Audio/Source/Virtual | An audio source that can produce data. Has input_* input ports and capture_* output ports for each channel. Can be selected as source in PulseAudio applications. |
| Audio/Duplex | An audio source and sink. Has playback_* input ports and capture_* output ports for each channel. Can be selected as source and sink in PulseAudio applications. |

Once the node is created, you can find the id with `pw-cli ls Node`, look for your node and remember the id.

You can list the volume, for example using:

```
pw-cli e <id> Props
```
Change with:
```
pw-cli s <id> Props '{ channelVolumes = [ 1.0 1.0 ] }'
```

### Create a Sink

Create a new node that can be selected as sink in PulseAudio applications:

```
pactl load-module module-null-sink media.class=Audio/Sink sink_name=my-sink channel_map=surround-51
```

The sink by itself will not play any audio. It will have monitor ports and a monitor source that can be selected in PulseAudio applications to get the output.

### Create a Source

Create a new node that can be selected as source in PulseAudio applications:

```
pactl load-module module-null-sink media.class=Audio/Source/Virtual sink_name=my-source channel_map=front-left,front-right
```
You will need to use a tool like pw-link to link the real source to the input ports of the virtual source.

### Create a Duplex Node

Create a new node that can be selected as source and sink in PulseAudio applications:

```
pactl load-module module-null-sink media.class=Audio/Duplex sink_name=my-tunnel audio.position=FL,FR,RL,RR
```
You can use this to send data from one PulseAudio application to another directly.

### Create a Combined Sink/Source

This is how you can make a sink/source that sends the output to multiple sinks/sources.

Create a new sink with:
````
pactl load-module module-null-sink media.class=Audio/Sink sink_name=my-combined-sink channel_map=stereo
````
Link the output ports (with pw-link or qjackctl to the sinks/sources). We hope to make it easier to link to the sink/sources in the future.

### Create an Aggregate Sink/Source

Here is how you can make a big multichannel audio device from multiple stereo devices.

Create a new sink with:
````
pactl load-module module-null-sink media.class=Audio/Sink sink_name=my-combined-sink channel_map=surround-51
````
Link the output ports (with pw-link or qjackctl) to the sinks/sources. We hope to make it easier to link to the sink/sources in the future.

### Multichannel Cards

When your card has multiple mono channels that you want to use separately, like when you have a microphone plugged in one input and a guitar in the other, you can make a mono source like so:
````
pactl load-module module-null-sink media.class=Audio/Source/Virtual sink_name=my-mic channel_map=mono
````
Then link the input port of the source to the microphone port (with, for example, pw-link). You can then select my-mic in applications as a source.

## Coupled Streams

Coupled streams are implemented with two internally connected streams, a capture and playback stream. In PulseAudio applications they show up as streams connected to sources and sinks. It is also possible to make one or both streams look like a Virtual device (Sink/Source).

Streams are normally automatically linked to devices by the session manager. This means that coupled streams don't need any manual intervention to get linked and can also be moved around with tools like pavucontrol or pw-metadata.

They however have a slightly higher overhead because they have the logic to do channel mapping and need a copy between the capture and playback streams.

Coupled streams are created by loading the [module-loopback](https://docs.pipewire.org/page_module_loopback.html) module or by running the `pw-loopback` tool.

Place the following config section into a separate file in a `.conf.d` directory or merge with an existing `.conf` file:

```
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            audio.position = [ FL FR ]
            capture.props = {
                media.class = Audio/Sink
                node.name = my_sink
                node.description = "my-sink"
            }
            playback.props = {
                node.name = "my_sink.output"
                node.passive = true
                target.object = "my-default-sink"
            }
        }
    }
]
```

You could copy a config file (like `/etc/pipewire/client-rt.conf`) to `/etc/pipewire/virtual-sink.conf` and merge
the above secion in it, then you can run it separately with:

```
pipewire -c virtual-sink.conf
```

Or with pw-loopback:

```
pw-loopback -m '[ FL FR ]' --capture-props='media.class=Audio/Sink node.name=my_sink' --playback-props='target.object="my-default-sink"'
```

This makes a sink that can be selected in applications. The data sent to this Sink is routed to a stream that will output to the sink named "my-default-sink".

This table lists some of the common properties that can be set on playback and capture:

|Key | Purpose|
|:--|:--|
| media.class | The class of the stream, Audio/Sink for a sink, Audio/Source for source.|
| node.name | The name of the node. This is used to link and refer to the stream later and should be unique. |
| node.description | The user visible description of the stream. |
| node.latency | The desired latency of the streams. |
| audio.rate | The desired audio rate of the stream. Leave this unset or else it might trigger resampling. |
| audio.channels | The number of channels of the stream. The channels from the capture stream are send directly to the playback stream. If there are more channels in the playback stream, they are set to 0. |
| audio.position | The audio position of the stream channels. by setting a different position for capture and playback stream, channels can be remapped. |
| target.object | The desired source or sink node.name or object.serial to connect to. |
| node.dont-reconnect | If the target.object disappears, kill the stream and don't reconnect. |
| node.passive | This node should be suspended with the sink when nothing is using it. |
| stream.dont-remix | Don't remix the channels of the stream. |

The following table summarizes the typical use cases:

| Use Case | Properties|
|:--|:--|
| Loopback | *default arguments* |
| Virtual Sink | `capture.props = { media.class=Audio/Sink }` |
| Virtual Source | `playback.props = { media.class=Audio/Source` } |
| Remap Sink | `capture.props = { media.class=Audio/Sink audio.position=[FL FR] } playback.props = { audio.position=[FR FL] }` |
| Remap Source | `capture.props = { audio.position=[FR FL] } playback.props = { media-class=Audio/Source audio.position=[FL FR] }` |

We refer the the [Client stream Configuration](Config-client#streamproperties) for an explanation of the properties.


### Loopback

Use `pw-loopback` with no arguments or load the libpipewire-module-loopback without any arguments to get the default loopback behavior.

```
pw-loopback
```
There is a PulseAudio equivalent:
```
pactl load-module module-loopback
```

### Virtual Sink

Create a sink that can be selected in applications and that forwards the data to another sink.

```
pw-loopback -m '[ FL FR]' --capture-props='media.class=Audio/Sink node.name=my-sink'
```
Or the PulseAudio equivalent:

```
pactl load-module module-remap-sink sink_name=my-sink
```
### Virtual Source

Create a source that can be selected in applications and that forwards the data from another source.

```
pw-loopback -m '[ FL FR]' --playback-props='media.class=Audio/Source node.name=my-source'
```
Or the PulseAudio equivalent:

```
pactl load-module module-remap-source sink_name=my-source
```

### Remap Sink

Create a sink that can be selected in applications and that swaps the left and right channels to another sink.

```
pw-loopback --capture-props='media.class=Audio/Sink node.name=my-sink audio.position=[FL FR]' --playback-props='audio.position=[FR FL]'
```
Or the PulseAudio equivalent:

```
pactl load-module module-remap-sink sink_name=my-sink channel_map=front-left,front-right master_channel_map=front-right,front-left
```

### Remap Source

Create a source that can be selected in applications and that swaps the left and right channels from another source.

```
pw-loopback --capture-props='audio.position=[FR FL]' --playback-props='media.class=Audio/Source node.name=my-source audio.position=[FL FR]'
```

Or the PulseAudio equivalent:
```
pactl load-module module-remap-source source_name=my-source channel_map=front-left,front-right master_channel_map=front-right,front-left
```

### Filter-chain

For more complicated virtual sinks, use the [Filter-chain](Filter-Chain).

# Virtual Surround

Check out the part of filter-chain on how to make [virtual surround sinks](Filter-Chain#virtual-surround).

See [JConvolver](Simple-audio-post-processing) example.

# Echo Cancellation

There is a native echo-cancel module available in PipeWire. it can be loaded as a PulseAudio module
or as a native [module-echo-cancel](https://docs.pipewire.org/page_module_echo_cancel.html) module.

You can then use pavucontrol to redirect the echo canceled recording and playback
stream to the source/sink to be echo canceled.

# Examples

The following example snippets need to be placed in `pipewire.conf` or `pipewire-pulse.conf` in the `context.modules` section or can be added to the `pipewire.conf.d` or `pipewire-pulse.conf.d` directories.

## Virtual Mono Source

This is an example of how to make a new source that makes a mono channel from the front left channel:

```
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            node.description = "C920 Front Left"
            capture.props = {
                node.name = capture.C920_Front_Left"
                audio.position = [ FL ]
                stream.dont-remix = true
                target.object = "alsa_input.usb-046d_HD_Pro_Webcam_C920_09D53E1F-02.analog-stereo"
                node.passive = true
            }
            playback.props = {
                node.name = "C920_Front_Left"
                media.class = "Audio/Source"
                audio.position = [ MONO ]
            }
        }
    }
]
```

The `stream.dont-remix` property makes sure that only the FL port of the capture device is linked (and not all ports mixed into FL).

The `node.passive` property instructs the session manager to create passive links between the stream and the device node so that, when nothing is connected to the source, both streams will idle (and suspend if the session manager has this configured).

The `node.target` makes the stream connect to the named device automatically by the session manager.
Note that if the device is removed, the stream will move to some other device until the named device
reappears. If you want the virtual device to appear and disappear together with the real device, you
will need to use a WirePlumber script.

## Virtual Sinks

Some professional cards have many independent inputs but you would only like to use a certain pair for media playback. This snippet will set up 2 stereo sinks on the first and second pair of channels of a 7.1 surround device.

```
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            node.description = "CM106 Stereo Pair 1"
            capture.props = {
                node.name = "CM106_stereo_pair_1"
                media.class = "Audio/Sink"
                audio.position = [ FL FR ]
            }
            playback.props = {
                node.name = "playback.CM106_stereo_pair_1"
                audio.position = [ FL FR ]
                target.object = "alsa_output.usb-0d8c_USB_Sound_Device-00.analog-surround-71"
                stream.dont-remix = true
                node.passive = true
            }
        }
    }
    {   name = libpipewire-module-loopback
        args = {
            node.description = "CM106 Stereo Pair 2"
            capture.props = {
                node.name = "CM106_stereo_pair_2"
                media.class = "Audio/Sink"
                audio.position = [ FL FR ]
            }
            playback.props = {
                node.name = "playback.CM106_stereo_pair_2"
                audio.position = [ RL RR ]
                target.object = "alsa_output.usb-0d8c_USB_Sound_Device-00.analog-surround-71"
                stream.dont-remix = true
                node.passive = true
            }
        }
    }
]
```

## Behringer UMC404HD Speakers/Headphones Virtual Sinks

The card is placed in the "Pro Audio" profile and 2 sinks are created, one for each stereo pair. The Behringer has an option to route the second stereo pair to the headphones.

```
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            node.description = "UMC Speakers"
            capture.props = {
                node.name = "UMC_Speakers"
                media.class = "Audio/Sink"
                audio.position = [ FL FR ]
            }
            playback.props = {
                node.name = "playback.UMC_Speakers"
                audio.position = [ AUX0 AUX1 ]
                target.object = "alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0"
                stream.dont-remix = true
                node.passive = true
            }
        }
    }
    {   name = libpipewire-module-loopback
        args = {
            node.description = "UMC Headphones"
            capture.props = {
                node.name = "UMC_Headphones"
                media.class = "Audio/Sink"
                audio.position = [ FL FR ]
            }
            playback.props = {
                node.name = "playback.UMC_Headphones"
                audio.position = [ AUX2 AUX3 ]
                target.object = "alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0"
                stream.dont-remix = true
                node.passive = true
            }
        }
    }
]
```

## Behringer UMC404HD Microphone/Guitar Virtual Sources

A condenser microphone is plugged into the first input port, the output of a guitar amplifier is plugged into the second input.

The card is placed in the "Pro Audio" profile and 2 sources are created. One for the microphone and another for the guitar signal.

```
context.modules = [
    {   name = libpipewire-module-loopback
        args = {
            node.description = "UMC Microphone"
            capture.props = {
                node.name = "capture.UMC_Mic"
                audio.position = [ AUX0 ]
                stream.dont-remix = true
                target.object = "alsa_input.usb-BEHRINGER_UMC404HD_192k-00.pro-input-0"
                node.passive = true
            }
            playback.props = {
                node.name = "UMC_Mic"
                media.class = "Audio/Source"
                audio.position = [ MONO ]
            }
        }
    }
    {   name = libpipewire-module-loopback
        args = {
            node.description = "UMC Guitar"
            capture.props = {
                node.name = "capture.UMC_Guitar"
                audio.position = [ AUX1 ]
                stream.dont-remix = true
                target.object = "alsa_input.usb-BEHRINGER_UMC404HD_192k-00.pro-input-0"
                node.passive = true
            }
            playback.props = {
                node.name = "UMC_Guitar"
                media.class = "Audio/Source"
                audio.position = [ MONO ]
            }
        }
    }
]
```

# Pipe devices

These are virtual devices that can be used to bring data into the graph (pipe source) or
read data from a graph (pipe sink).

You can use pactl to load the equivalent PulseAudio module.

You can also use the native pipe tunnel module. For this the following example snippets need to be placed in `pipewire.conf` or `pipewire-pulse.conf` in the `context.modules` section or can be copy and pasted into a new file in the `pipewire.conf.d` or `pipewire-pulse.conf.d` directories.

Check out [module-pipe-tunnel](https://docs.pipewire.org/page_module_pipe_tunnel.html) for more info.

## Pipe source

You can use the PulseAudio module to load a pipe source like this:

```
pactl load-module module-pipe-source source_name=VirtualMic file=/tmp/virtualdevice format=s16le rate=44100 channels=2
```

When you write samples to `/tmp/virtualdevice` they will appear as samples on the source in the
PipeWire graph.

This snippet is the equivalent of the above:

```json
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = source
          pipe.filename = "/tmp/virtualdevice"
          audio.format = S16LE
          audio.rate = 44100
          audio.channels = 2
          audio.position = [ FL FR ]
          stream.props = {
	      node.name = VirtualMic
          }
      }
  }
]
```

## Pipe sink

You can use the PulseAudio module to load a pipe sink like this:

```
pactl load-module module-pipe-sink source_name=VirtualSpeaker file=/tmp/virtualdevice format=s16le rate=44100 channels=2
```

Samples sent to the PipeWire virtual sink will be available on `/tmp/virtualdevice` for reading.

This snippet is the equivalent of the above:

```json
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = sink
          pipe.filename = "/tmp/virtualdevice"
          audio.format = S16LE
          audio.rate = 44100
          audio.channels = 2
          audio.position = [ FL FR ]
          stream.props = {
	      node.name = VirtualSpeaker
          }
      }
  }
]
```

## Pipe playback

This will create a playback stream linked to the configured sink.

When you write samples to `/tmp/virtualdevice` they will be played on the sink.

```json
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = playback
          pipe.filename = "/tmp/virtualdevice"
          audio.format = S16LE
          audio.rate = 44100
          audio.channels = 2
          audio.position = [ FL FR ]
          stream.props = {
	      node.name = VirtualOut
          }
      }
  }
]
```


## Pipe capture

This will create a capture stream linked to the configured source.

Samples captured by the source will appear on `/tmp/virtualdevice` for reading by the
application.

```json
context.modules = [
  {   name = libpipewire-module-pipe-tunnel
      args = {
          tunnel.mode = capture
          pipe.filename = "/tmp/virtualdevice"
          audio.format = S16LE
          audio.rate = 44100
          audio.channels = 2
          audio.position = [ FL FR ]
          stream.props = {
	      node.name = VirtualIn
          }
      }
  }
]
```

# Combine streams

These are virtual devices that can combine multiple sinks or sources into one new virtual sink or source.

The streams to merge into one are selected with [match rules](Config-PipeWire#rules).

The features are implemented with the `libpipewire-module-combine-stream` module. Please refer to the
[documentation](https://docs.pipewire.org/page_module_combine_stream.html) For more information.

## Combine sink (all sinks)

The following example loads a new virtual sink that sends the output to all other sinks.

This is also the default behaviour when stream.rules is not defined.

```json
context.modules = [
{   name = libpipewire-module-combine-stream
    args = {
        combine.mode = sink
        node.name = "combine_sink"
        node.description = "My Combine Sink"
        combine.latency-compensate = false   # if true, match latencies by adding delays
        combine.props = {
            audio.position = [ FL FR ]
        }
        stream.props = {
        }
        stream.rules = [
            {
                matches = [ { media.class = "Audio/Sink" } ]
                actions = { create-stream = { } }
            }
        ]
    }
}
]
```

## Combine sink (selected channels and sinks)

The following example merges 3 stereo sinks into a 5.1 virtual device:

```json
context.modules = [
{   name = libpipewire-module-combine-stream
    args = {
        combine.mode = sink
        node.name = "combine_sink_5_1"
        node.description = "My 5.1 Combine Sink"
        combine.latency-compensate = false
        combine.props = {
            audio.position = [ FL FR FC LFE SL SR ]
        }
        stream.props = {
                stream.dont-remix = true      # link matching channels without remixing
        }
        stream.rules = [
            {   matches = [
                    {   media.class = "Audio/Sink"
                        node.name = "alsa_output.usb-Topping_E30-00.analog-stereo"
                    } ]
                actions = { create-stream = {
                        combine.audio.position = [ FL FR ]
                        audio.position = [ FL FR ]
                } } }
            {   matches = [
                    {   media.class = "Audio/Sink"
                        node.name = "alsa_output.usb-BEHRINGER_UMC404HD_192k-00.pro-output-0"
                    } ]
                actions = { create-stream = {
                        combine.audio.position = [ FC LFE ]
                        audio.position = [ AUX0 AUX1 ]
                } } }
            {   matches = [
                    {   media.class = "Audio/Sink"
                        node.name = "alsa_output.pci-0000_00_1b.0.analog-stereo"
                    } ]
                actions = { create-stream = {
                        combine.audio.position = [ SL SR ]
                        audio.position = [ FL FR ]
                } } }
        ]
    }
}
]
```

## Combine source (selected channels and sources)

Below is an example configuration that makes a 4.0 virtual audio source
from 2 separate stereo sources.

```json
context.modules = [
{   name = libpipewire-module-combine-stream
    args = {
        combine.mode = source
        node.name = "combine_source_4_0"
        node.description = "My 4.0 Combine Source"
        combine.latency-compensate = false
        combine.props = {
            audio.position = [ FL FR SL SR ]
        }
        stream.props = {
                stream.dont-remix = true
        }
        stream.rules = [
            {   matches = [
                    {   media.class = "Audio/Source"
                        node.name = "alsa_input.usb-046d_HD_Pro_Webcam_C920_09D53E1F-02.analog-stereo"
                    } ]
                actions = { create-stream = {
                        audio.position = [ FL FR ]
                        combine.audio.position = [ FL FR ]
                } } }
            {   matches = [
                    {   media.class = "Audio/Source"
                        node.name = "alsa_input.usb-046d_0821_9534DE90-00.analog-stereo"
                    } ]
                actions = { create-stream = {
                        audio.position = [ FL FR ]
                        combine.audio.position = [ SL SR ]
                } } }
        ]
    }
}
]
```

# Other virtual devices

Other virtual devices can be created, depending on the source of the data:

* [module-protocol-simple](https://docs.pipewire.org/page_module_protocol_simple.html) for reading/writing
  from a TCP socket.
* [module-pulse-tunnel](https://docs.pipewire.org/page_module_pulse_tunnel.html) for reading/writing
  from/to a PulseAudio server.
* [module-roc-sink](https://docs.pipewire.org/page_module_roc_sink.html) for sending to a
  ROC receiver.
* [module-roc-source](https://docs.pipewire.org/page_module_roc_source.html) for receiving from a
  ROC sender.
* [module-raop-sink](https://docs.pipewire.org/page_module_raop_sink.html) for sending to an
  Airplay receiver.
* [module-rtp-source](https://docs.pipewire.org/page_module_rtp_source.html) for receiving from a
  SAP/RTP sender.
* [module-rtp-sink](https://docs.pipewire.org/page_module_rtp_sink.html) for announcing a stream
  using SAP/RTP.
* [module-rtp-session](https://docs.pipewire.org/page_module_rtp_session.html) for announcing
  and establishing a bidirectional session using avahi.
