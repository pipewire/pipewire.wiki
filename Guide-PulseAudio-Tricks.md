[[_TOC_]]

# Enable paprefs configuration

paprefs is an GUI application to configure the PulseAudio daemon. It writes the
configuration in GSettings.

To automatically load the configuration from GSettings at startup, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/20-gsettings.conf` with the content:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-gsettings" flags = [ nofail ] }
]
```

And restart pipewire-pulse.


# Setup TCP network streaming between machines

There are a couple of [options](Network) for doing network streaming with PipeWire,
each with its advantages and drawbacks.

The easiest and most featureful option is to use the PulseAudio protocol to send and receive
audio to and from sink and sources.

## Receiver

The receiver is going to announce its devices (sinks and sources) on the network so that the senders can send their data to the sinks.

On the receiver side (publisher), make a file 
`~/.config/pipewire/pipewire-pulse.conf.d/30-network-publish.conf` with the content:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-native-protocol-tcp" }
  { cmd = "load-module" args = "module-zeroconf-publish" }
]
```
And restart the pipewire-pulse server.

## Sender

The sender uses the discover module to find all devices on the network. It can then send or receiver to and from those sinks and sources.

On the sender side,  make a file 
`~/.config/pipewire/pipewire-pulse.conf.d/30-network-discover.conf` with the content:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-zeroconf-discover" }
]

```
And restart the pipewire-pulse server.

# Setup RTP network streaming between machines

Another option for doing network audio is to use the RTP protocol. This can potentially give
a lower-latency audio at the expense of potential dropouts, especially on WIFI networks.

## Sender

On the sender side, make a file 
`~/.config/pipewire/pipewire-pulse.conf.d/35-rtp-send.conf` with the content:


```
pulse.cmd = [
  { cmd = "load-module" args = "module-rtp-send" }
]
```

The sender will use SAP messages to broadcast the availability of a stream.

You can also send the SAP and RTP directly to a specific machine (192.168.1.45) with:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-rtp-send destination_ip=192.168.1.45" }
]
```

This will capture data from the default source. You can also make a sink and then stream the output of the sink to rtp like this:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-null-sink sink_name=rtp-sink" }
  { cmd = "load-module" args = "module-rtp-send source=rtp-sink.monitor" }
]
```


## Receiver

On the receiver side,  make a file 
`~/.config/pipewire/pipewire-pulse.conf.d/35-rtp-recv.conf` with the content:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-rtp-recv" }
]
```
And restart the pipewire-pulse server.

The receiver will receive SAP messages from the sender and automatically create a
stream.

# Enable upmixing for PulseAudio clients

If you have a 5.1 or 7.1 surrounf sound system and you play a stereo file, the sound will not
automatically come out of all your speakers. To make this happen you have to enable upmixing.

To enable upmixing for PulseAudio clients, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/40-upmix.conf` with the content:

```
# Enables upmixing
stream.properties = {
    channelmix.upmix      = true
    channelmix.upmix-method = psd
    channelmix.lfe-cutoff = 150
    channelmix.fc-cutoff  = 12000
    channelmix.rear-delay = 12.0
}
```

And restart the pipewire-pulse server.

More info [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/Config-client#channel-mixer-properties).

# Send sound to multiple devices

Normally, applications can send their output to only one output device.

To send audio to multiple sinks, make a file
`~/.config/pipewire/pipewire-pulse.conf.d/50-combine-sink.conf` with the content:

```
pulse.cmd = [
  { cmd = "load-module" args = "module-combine-sink" }
]
```

And restart the pipewire-pulse server.
