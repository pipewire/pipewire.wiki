[[_TOC_]]

# General

To avoid xruns and other hiccups while processing audio, there are some configuration options that can be done.

# Kernel

On recent kernels configured with PREEMPT_DYNAMIC, you can boot the kernel with and extra
`preempt=full` option.

You could also try to install a realtime kernel for your distribution.

Kernel 5.16 brings important latency improvements for USB devices. It also includes some important samplerate fixes https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/1547 .

# Realtime Scheduling

Realtime priorities are given to the data processing threads in both the clients and the server.

Since 0.3.44 there is a single module-rt that can use Portal/RTKit and fall back to native thread implementation automatically.

module-rt first tries to use the org.freedesktop.portal.Realtime DBus service to request realtime privileges to the processing thread. This Portal service knows about flatpak namespaces and can map flatpak pids to pids used in org.freedesktop.RealtimeKit1.

org.freedesktop.RealtimeKit1 is used as a fallback when the Realtime portal doesn't exist.

The Portal/RTKit implementation is potentially better because it can implement a global policy and does not require extra permissions from the client. It however needs DBus and is currently not configured optimally in many distributions.

# Configuration

The configuration of the realtime priorities and the implementation can be done in the different server and client config files:

- `/usr/share/pipewire/pipewire.conf` for the PipeWire daemon settings
- `/usr/share/pipewire/pipewire-pulse.conf` for the PipeWire pulseaudio server
- `/usr/share/pipewire/client-rt.conf` for the PipeWire native client settings
- `/usr/share/pipewire/jack.conf` for the PipeWire JACK clients

The relevant module is enabled in the `context.modules` section, see
[module-rt](https://docs.pipewire.org/page_module_rt.html) documentation.

# RLIMITs

Real-time priority limits are usually stored in `/etc/security/limits.conf` and `/etc/security/limits.d/`. The best option is to add a new file `95-pipewire.conf` in `/etc/security/limits.d/` with this content:

```ini
# Default limits for users of pipewire
@pipewire - rtprio 95
@pipewire - nice -19
@pipewire - memlock 4194304
```
Then add your user to the PipeWire group so that you can use these priorities.

# RTkit

## For Red Hat/Arch Based Distributions

RTKit can be configured by passing extra arguments to the `/usr/libexec/rtkit-daemon` process. You can try `/usr/libexec/rtkit-daemon --help` to see the options.

Depending on the distribution, you would do something like this:

* Make `/usr/lib/systemd/system/rtkit-daemon.service.d/limits.conf` with:

```systemd
[Service]
EnvironmentFile=/etc/sysconfig/rtkit
ExecStart=
ExecStart=/usr/libexec/rtkit-daemon $RTKIT_ARGS
```

* Make `/etc/sysconfig/rtkit` with:

```ini
RTKIT_ARGS="--scheduling-policy=FIFO
--our-realtime-priority=89
--max-realtime-priority=88
--min-nice-level=-19
--rttime-usec-max=2000000
--users-max=100
--processes-per-user-max=1000
--threads-per-user-max=10000
--actions-burst-sec=10
--actions-per-burst-max=1000
--canary-cheep-msec=30000
--canary-watchdog-msec=60000
"
```

## For Debian/Ubuntu Based Distributions

* Edit `/lib/systemd/system/rtkit-daemon.service` with `systemctl edit rtkit-daemon.service` and add changes like so:

```systemd
# This file is part of RealtimeKit.
#
# Copyright 2010 Lennart Poettering
#
# RealtimeKit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RealtimeKit is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with RealtimeKit. If not, see <http://www.gnu.org/licenses/>.

[Unit]
Description=RealtimeKit Scheduling Policy Service

[Service]
EnvironmentFile=/etc/dbus-1/system.d/rtkit
ExecStart=/usr/libexec/rtkit-daemon $RTKIT_ARGS
Type=dbus
BusName=org.freedesktop.RealtimeKit1
NotifyAccess=main
CapabilityBoundingSet=CAP_SYS_NICE CAP_DAC_READ_SEARCH CAP_SYS_CHROOT CAP_SETGID CAP_SETUID
PrivateNetwork=yes

[Install]
WantedBy=graphical.target
```

* Make `/etc/dbus-1/system.d/rtkit` with:

```ini
RTKIT_ARGS="--scheduling-policy=FIFO
--our-realtime-priority=89
--max-realtime-priority=88
--min-nice-level=-19
--rttime-usec-max=2000000
--users-max=100
--processes-per-user-max=1000
--threads-per-user-max=10000
--actions-burst-sec=10
--actions-per-burst-max=1000
--canary-cheep-msec=30000
--canary-watchdog-msec=60000
"
```

Reboot.

# ALSA

ALSA USB devices can be tuned for latency with the `api.alsa.period-size` property.

Since 0.3.43 this tuning is done automatically when the device is opened and based on the graph quantum.

See [here](https://gitlab.freedesktop.org/pipewire/media-session/-/wikis/Config-ALSA#alsa-buffer-properties) for tuning the period-size.

See [here](Performance#pipewire-0331-tuned) for some measurements with a tuned ALSA USB device.

# Applications

## Firefox

Firefox ships with Reader enabled, which is enabled for speech synthesis (read text
aloud). Alas, with this merely enabled, Firefox will make pipewire hog CPU resources,
especially inside a VM.

You can notice this in pw-top when the processes `speech-dispatcher-dummy` and
`speech-dispatcher-espeak-ng` take up a lot of CPU time.

To compensate, turn off relevant settings in Firefox by opening the about:config URL in Firefox,
configure the settings as found below, then restart Firefox.

```
reader.parse-on-load.enabled false
media.webspeech.synth.enabled false
```

Since 0.3.44 the pulse-server has a quirk to force a higher quantum on speech dispatcher, which should alleviate the problem.

Since 0.3.61, the pulse-server will suspend speech-dispatcher when it is inactive for 5 minutes, which should
eliminate the problem.

# Profiling

`pw-top` can be used to get a realtime overview of the various nodes in the graph.

The `pw-profiler` tool can be used to collect statistics about PipeWire and turn them into images that can be displayed in the browser. Run `pw-profiler` in a terminal, let it collect information and then stop it. It will output a script and an html file in the current directory with instructions on how to display the generated graphs.

# Further Reading

Check [here](Performance) for some performance measurements.

See also [here](https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/698) for more information.
